<?xml version="1.0" encoding="UTF-8"?>
<solution name="PlanningLanguage.sandbox" uuid="76feab90-0c10-4cdc-96a2-84885d0f3447" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
    <modelRoot contentPath="/Users/tanboonwan/projectmanagement-2/projectmanagement/JPlanner.jar!/" type="java_classes">
      <sourceRoot location="." />
    </modelRoot>
  </models>
  <stubModelEntries>
    <stubModelEntry path="/Users/tanboonwan/projectmanagement-2/projectmanagement/JPlanner.jar" />
  </stubModelEntries>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)</dependency>
  </dependencies>
  <languageVersions>
    <language slang="l:48455107-003a-4d59-8376-b8d95ea4f6ed:PlanningLanguage" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="76feab90-0c10-4cdc-96a2-84885d0f3447(PlanningLanguage.sandbox)" version="0" />
  </dependencyVersions>
</solution>

