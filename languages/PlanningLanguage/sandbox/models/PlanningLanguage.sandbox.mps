<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:d5acd212-4203-4672-a6ac-c355162f2ede(PlanningLanguage.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="48455107-003a-4d59-8376-b8d95ea4f6ed" name="PlanningLanguage" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="48455107-003a-4d59-8376-b8d95ea4f6ed" name="PlanningLanguage">
      <concept id="89425540936782164" name="PlanningLanguage.structure.ActivityTime" flags="ng" index="10nYlq">
        <property id="7319139016311737619" name="endHour" index="1dp4cV" />
        <property id="7319139016311710393" name="startHour" index="1dpeMh" />
        <property id="7319139016311710397" name="startMin" index="1dpeMl" />
        <child id="7319139016311737868" name="dayOfWeek" index="1dp40$" />
      </concept>
      <concept id="7319139016311737793" name="PlanningLanguage.structure.DayOfWeek" flags="ng" index="1dp4fD">
        <property id="7319139016311737832" name="Saturday" index="1dp4f0" />
        <property id="7319139016311737845" name="Sunday" index="1dp4ft" />
        <property id="7319139016311737800" name="Tuesday" index="1dp4fw" />
        <property id="7319139016311737805" name="Wednesday" index="1dp4f_" />
        <property id="7319139016311737797" name="Monday" index="1dp4fH" />
        <property id="7319139016311737821" name="Friday" index="1dp4fP" />
        <property id="7319139016311737812" name="Thursday" index="1dp4fW" />
      </concept>
      <concept id="2829670469730820874" name="PlanningLanguage.structure.Course" flags="ng" index="3GWI56">
        <property id="7319139016311713497" name="difficulty" index="1dpe3L" />
        <child id="7319139016311737888" name="classes" index="1dp408" />
      </concept>
      <concept id="2829670469730822382" name="PlanningLanguage.structure.Schedule" flags="ng" index="3GWIEy">
        <child id="7319139016311737891" name="RestTime" index="1dp40b" />
        <child id="7319139016311737883" name="MealTime" index="1dp40N" />
        <child id="2829670469730822725" name="courses" index="3GWIw9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3GWIEy" id="6miNywCY73O">
    <property role="TrG5h" value="University Schedule" />
    <node concept="10nYlq" id="6miNywCZsoS" role="1dp40b">
      <property role="TrG5h" value="Sleeping" />
      <property role="1dpeMh" value="22" />
      <property role="1dp4cV" value="-6" />
      <node concept="1dp4fD" id="6miNywCZsoT" role="1dp40$">
        <property role="1dp4fH" value="true" />
        <property role="1dp4fw" value="true" />
        <property role="1dp4f_" value="true" />
        <property role="1dp4fW" value="true" />
        <property role="1dp4fP" value="true" />
        <property role="1dp4f0" value="true" />
        <property role="1dp4ft" value="true" />
      </node>
    </node>
    <node concept="10nYlq" id="6miNywCYaAH" role="1dp40N">
      <property role="TrG5h" value="Breakfast" />
      <property role="1dp4cV" value="9" />
      <node concept="1dp4fD" id="6miNywCYaAI" role="1dp40$">
        <property role="1dp4fH" value="true" />
        <property role="1dp4f_" value="true" />
        <property role="1dp4fw" value="true" />
        <property role="1dp4fW" value="true" />
        <property role="1dp4f0" value="true" />
        <property role="1dp4ft" value="true" />
        <property role="1dp4fP" value="true" />
      </node>
    </node>
    <node concept="10nYlq" id="6miNywCZsow" role="1dp40N">
      <property role="TrG5h" value="Lunch" />
      <property role="1dpeMh" value="13" />
      <property role="1dp4cV" value="14" />
      <node concept="1dp4fD" id="6miNywCZsox" role="1dp40$">
        <property role="1dp4fH" value="true" />
        <property role="1dp4f_" value="true" />
        <property role="1dp4fW" value="true" />
        <property role="1dp4fP" value="true" />
        <property role="1dp4f0" value="true" />
        <property role="1dp4ft" value="true" />
        <property role="1dp4fw" value="true" />
      </node>
    </node>
    <node concept="10nYlq" id="6miNywCZsoE" role="1dp40N">
      <property role="TrG5h" value="Dinner" />
      <property role="1dpeMh" value="18" />
      <property role="1dp4cV" value="19" />
      <node concept="1dp4fD" id="6miNywCZsoF" role="1dp40$">
        <property role="1dp4fH" value="true" />
        <property role="1dp4fw" value="true" />
        <property role="1dp4f_" value="true" />
        <property role="1dp4fW" value="true" />
        <property role="1dp4fP" value="true" />
        <property role="1dp4f0" value="true" />
        <property role="1dp4ft" value="true" />
      </node>
    </node>
    <node concept="3GWI56" id="6miNywCY73P" role="3GWIw9">
      <property role="TrG5h" value="SWEN424" />
      <property role="1dpe3L" value="3" />
      <node concept="10nYlq" id="6miNywCY73S" role="1dp408">
        <property role="TrG5h" value="Lecture" />
        <property role="1dpeMh" value="14" />
        <property role="1dpeMl" value="10" />
        <property role="1dp4cV" value="15" />
        <node concept="1dp4fD" id="6miNywCY73T" role="1dp40$">
          <property role="1dp4fH" value="true" />
          <property role="1dp4f_" value="true" />
          <property role="1dp4fw" value="true" />
        </node>
      </node>
    </node>
    <node concept="3GWI56" id="6miNywCY73Y" role="3GWIw9">
      <property role="TrG5h" value="COMP307" />
      <node concept="10nYlq" id="6miNywCY747" role="1dp408">
        <property role="TrG5h" value="Lecture" />
        <property role="1dpeMh" value="12" />
        <property role="1dp4cV" value="13" />
        <node concept="1dp4fD" id="6miNywCY748" role="1dp40$">
          <property role="1dp4fw" value="true" />
          <property role="1dp4f_" value="true" />
        </node>
      </node>
      <node concept="10nYlq" id="6miNywCZsmJ" role="1dp408">
        <property role="TrG5h" value="Tutorial" />
        <property role="1dpeMh" value="12" />
        <property role="1dp4cV" value="13" />
        <node concept="1dp4fD" id="6miNywCZsmK" role="1dp40$">
          <property role="1dp4fP" value="true" />
        </node>
      </node>
    </node>
    <node concept="3GWI56" id="6miNywCZsmT" role="3GWIw9">
      <property role="TrG5h" value="SWEN432" />
      <node concept="10nYlq" id="6miNywCZsoq" role="1dp408">
        <property role="TrG5h" value="Lecture" />
        <property role="1dpeMh" value="11" />
        <property role="1dp4cV" value="12" />
        <node concept="1dp4fD" id="6miNywCZsor" role="1dp40$">
          <property role="1dp4fH" value="true" />
          <property role="1dp4f_" value="true" />
          <property role="1dp4fP" value="true" />
        </node>
      </node>
    </node>
  </node>
</model>

