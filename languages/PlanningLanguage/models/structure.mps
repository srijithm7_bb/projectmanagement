<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:59173ef2-9d94-4aad-b93e-31f525951c59(PlanningLanguage.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="7" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="4fqr" ref="r:fa713d69-08ea-4732-b1f2-cb07f9e103ef(jetbrains.mps.execution.util.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1082978164219" name="jetbrains.mps.lang.structure.structure.EnumerationDataTypeDeclaration" flags="ng" index="AxPO7">
        <property id="1197591154882" name="memberIdentifierPolicy" index="3lZH7k" />
        <reference id="1083171729157" name="memberDataType" index="M4eZT" />
        <reference id="1083241965437" name="defaultMember" index="Qgau1" />
        <child id="1083172003582" name="member" index="M5hS2" />
      </concept>
      <concept id="1083171877298" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ig" index="M4N5e">
        <property id="1083923523172" name="externalValue" index="1uS6qo" />
        <property id="1083923523171" name="internalValue" index="1uS6qv" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="2t50$NbHYca">
    <property role="EcuMT" value="2829670469730820874" />
    <property role="TrG5h" value="Course" />
    <property role="34LRSv" value="Course" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="6miNywCXyVp" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311713497" />
      <property role="TrG5h" value="difficulty" />
      <ref role="AX2Wp" node="6miNywCXGsU" resolve="LevelOfDifficulty" />
    </node>
    <node concept="PrWs8" id="2t50$NbHYfn" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="6miNywCXCSw" role="1TKVEi">
      <property role="IQ2ns" value="7319139016311737888" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="classes" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="4XH0T1o_5k" resolve="ActivityTime" />
    </node>
  </node>
  <node concept="1TIwiD" id="2t50$NbHYzI">
    <property role="EcuMT" value="2829670469730822382" />
    <property role="TrG5h" value="Schedule" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Schedule Name" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2t50$NbHYD5" role="1TKVEi">
      <property role="IQ2ns" value="2829670469730822725" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="courses" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="2t50$NbHYca" resolve="Course" />
    </node>
    <node concept="1TJgyj" id="6miNywCXCSr" role="1TKVEi">
      <property role="IQ2ns" value="7319139016311737883" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="MealTime" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="4XH0T1o_5k" resolve="ActivityTime" />
    </node>
    <node concept="1TJgyj" id="6miNywCXCSz" role="1TKVEi">
      <property role="IQ2ns" value="7319139016311737891" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="RestTime" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="4XH0T1o_5k" resolve="ActivityTime" />
    </node>
    <node concept="PrWs8" id="2t50$NbHYMF" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="2lFfnZ$$Oim" role="PzmwI">
      <ref role="PrY4T" to="4fqr:431DWIovi3l" resolve="IMainClass" />
    </node>
  </node>
  <node concept="1TIwiD" id="4XH0T1o_5k">
    <property role="EcuMT" value="89425540936782164" />
    <property role="TrG5h" value="ActivityTime" />
    <property role="34LRSv" value="Activity Time" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="6miNywCXyaT" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311710393" />
      <property role="TrG5h" value="startHour" />
      <ref role="AX2Wp" node="6miNywCXCNI" resolve="Hours" />
    </node>
    <node concept="1TJgyi" id="6miNywCXyaX" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311710397" />
      <property role="TrG5h" value="startMin" />
      <ref role="AX2Wp" node="6miNywCXCO5" resolve="Minutes" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCOj" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737619" />
      <property role="TrG5h" value="endHour" />
      <ref role="AX2Wp" node="6miNywCXCNI" resolve="Hours" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCOq" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737626" />
      <property role="TrG5h" value="endMin" />
      <ref role="AX2Wp" node="6miNywCXCO5" resolve="Minutes" />
    </node>
    <node concept="1TJgyj" id="6miNywCXCSc" role="1TKVEi">
      <property role="IQ2ns" value="7319139016311737868" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="dayOfWeek" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6miNywCXCR1" resolve="DayOfWeek" />
    </node>
    <node concept="PrWs8" id="6miNywCXWHn" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="AxPO7" id="6miNywCXCNI">
    <property role="TrG5h" value="Hours" />
    <property role="3lZH7k" value="derive_from_presentation" />
    <ref role="M4eZT" to="tpck:fKAQMTA" resolve="integer" />
    <ref role="Qgau1" node="6miNywCYxda" />
    <node concept="M4N5e" id="6miNywCXCNJ" role="M5hS2">
      <property role="1uS6qv" value="0" />
      <property role="1uS6qo" value="12am" />
    </node>
    <node concept="M4N5e" id="6miNywCXCNK" role="M5hS2">
      <property role="1uS6qo" value="1am" />
      <property role="1uS6qv" value="1" />
    </node>
    <node concept="M4N5e" id="6miNywCXCNP" role="M5hS2">
      <property role="1uS6qo" value="2am" />
      <property role="1uS6qv" value="2" />
    </node>
    <node concept="M4N5e" id="6miNywCYxc9" role="M5hS2">
      <property role="1uS6qv" value="3" />
      <property role="1uS6qo" value="3am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxci" role="M5hS2">
      <property role="1uS6qv" value="4" />
      <property role="1uS6qo" value="4am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxct" role="M5hS2">
      <property role="1uS6qv" value="5" />
      <property role="1uS6qo" value="5am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxcE" role="M5hS2">
      <property role="1uS6qv" value="6" />
      <property role="1uS6qo" value="6am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxcT" role="M5hS2">
      <property role="1uS6qv" value="7" />
      <property role="1uS6qo" value="7am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxda" role="M5hS2">
      <property role="1uS6qv" value="8" />
      <property role="1uS6qo" value="8am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxdt" role="M5hS2">
      <property role="1uS6qv" value="9" />
      <property role="1uS6qo" value="9am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxdM" role="M5hS2">
      <property role="1uS6qv" value="10" />
      <property role="1uS6qo" value="10am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxe9" role="M5hS2">
      <property role="1uS6qv" value="11" />
      <property role="1uS6qo" value="11am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxey" role="M5hS2">
      <property role="1uS6qv" value="12" />
      <property role="1uS6qo" value="12pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxeX" role="M5hS2">
      <property role="1uS6qv" value="13" />
      <property role="1uS6qo" value="1pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxfq" role="M5hS2">
      <property role="1uS6qv" value="14" />
      <property role="1uS6qo" value="2pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxfT" role="M5hS2">
      <property role="1uS6qv" value="15" />
      <property role="1uS6qo" value="3pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxgq" role="M5hS2">
      <property role="1uS6qv" value="16" />
      <property role="1uS6qo" value="4pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxgX" role="M5hS2">
      <property role="1uS6qv" value="17" />
      <property role="1uS6qo" value="5pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxhy" role="M5hS2">
      <property role="1uS6qv" value="18" />
      <property role="1uS6qo" value="6pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxi9" role="M5hS2">
      <property role="1uS6qv" value="19" />
      <property role="1uS6qo" value="7pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxiM" role="M5hS2">
      <property role="1uS6qv" value="20" />
      <property role="1uS6qo" value="8pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxjt" role="M5hS2">
      <property role="1uS6qv" value="21" />
      <property role="1uS6qo" value="9pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxka" role="M5hS2">
      <property role="1uS6qv" value="22" />
      <property role="1uS6qo" value="10pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxkT" role="M5hS2">
      <property role="1uS6qv" value="23" />
      <property role="1uS6qo" value="11pm" />
    </node>
    <node concept="M4N5e" id="6miNywCYxmt" role="M5hS2">
      <property role="1uS6qo" value="next day 1am" />
      <property role="1uS6qv" value="-1" />
    </node>
    <node concept="M4N5e" id="6miNywCYxni" role="M5hS2">
      <property role="1uS6qo" value="next day 2am" />
      <property role="1uS6qv" value="-2" />
    </node>
    <node concept="M4N5e" id="6miNywCYxo9" role="M5hS2">
      <property role="1uS6qo" value="next day 3am" />
      <property role="1uS6qv" value="-3" />
    </node>
    <node concept="M4N5e" id="6miNywCYxpS" role="M5hS2">
      <property role="1uS6qv" value="-4" />
      <property role="1uS6qo" value="next day 4am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxqL" role="M5hS2">
      <property role="1uS6qv" value="-5" />
      <property role="1uS6qo" value="next day 5am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxrG" role="M5hS2">
      <property role="1uS6qv" value="-6" />
      <property role="1uS6qo" value="next day 6am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxsD" role="M5hS2">
      <property role="1uS6qv" value="-7" />
      <property role="1uS6qo" value="next day 7am" />
    </node>
    <node concept="M4N5e" id="6miNywCYxtC" role="M5hS2">
      <property role="1uS6qv" value="-8" />
      <property role="1uS6qo" value="next day 8am" />
    </node>
  </node>
  <node concept="AxPO7" id="6miNywCXCO5">
    <property role="TrG5h" value="Minutes" />
    <ref role="M4eZT" to="tpck:fKAQMTA" resolve="integer" />
    <node concept="M4N5e" id="6miNywCXCO6" role="M5hS2">
      <property role="1uS6qv" value="0" />
      <property role="1uS6qo" value="00min" />
    </node>
    <node concept="M4N5e" id="2lFfnZ$ytan" role="M5hS2">
      <property role="1uS6qo" value="5min" />
      <property role="1uS6qv" value="5" />
    </node>
    <node concept="M4N5e" id="6miNywCXCO7" role="M5hS2">
      <property role="1uS6qo" value="10min" />
      <property role="1uS6qv" value="10" />
    </node>
    <node concept="M4N5e" id="2lFfnZ$ytaA" role="M5hS2">
      <property role="1uS6qo" value="15min" />
      <property role="1uS6qv" value="15" />
    </node>
    <node concept="M4N5e" id="6miNywCXCOc" role="M5hS2">
      <property role="1uS6qo" value="20min" />
      <property role="1uS6qv" value="20" />
    </node>
    <node concept="M4N5e" id="2lFfnZ$ytaR" role="M5hS2">
      <property role="1uS6qo" value="25min" />
      <property role="1uS6qv" value="25" />
    </node>
    <node concept="M4N5e" id="6miNywCYxuD" role="M5hS2">
      <property role="1uS6qo" value="30min" />
      <property role="1uS6qv" value="30" />
    </node>
    <node concept="M4N5e" id="2lFfnZ$ytba" role="M5hS2">
      <property role="1uS6qo" value="35min" />
      <property role="1uS6qv" value="35" />
    </node>
    <node concept="M4N5e" id="6miNywCZnL5" role="M5hS2">
      <property role="1uS6qo" value="40min" />
      <property role="1uS6qv" value="40" />
    </node>
    <node concept="M4N5e" id="2lFfnZ$ytbv" role="M5hS2">
      <property role="1uS6qo" value="45min" />
      <property role="1uS6qv" value="45" />
    </node>
    <node concept="M4N5e" id="6miNywCZnLg" role="M5hS2">
      <property role="1uS6qo" value="50min" />
      <property role="1uS6qv" value="50" />
    </node>
    <node concept="M4N5e" id="2lFfnZ$ytbQ" role="M5hS2">
      <property role="1uS6qo" value="55min" />
      <property role="1uS6qv" value="55" />
    </node>
  </node>
  <node concept="1TIwiD" id="6miNywCXCR1">
    <property role="EcuMT" value="7319139016311737793" />
    <property role="TrG5h" value="DayOfWeek" />
    <property role="34LRSv" value="Day of Week" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6miNywCXCR2" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCR5" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737797" />
      <property role="TrG5h" value="Monday" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCR8" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737800" />
      <property role="TrG5h" value="Tuesday" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCRd" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737805" />
      <property role="TrG5h" value="Wednesday" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCRk" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737812" />
      <property role="TrG5h" value="Thursday" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCRt" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737821" />
      <property role="TrG5h" value="Friday" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCRC" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737832" />
      <property role="TrG5h" value="Saturday" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="6miNywCXCRP" role="1TKVEl">
      <property role="IQ2nx" value="7319139016311737845" />
      <property role="TrG5h" value="Sunday" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="AxPO7" id="6miNywCXGsU">
    <property role="TrG5h" value="LevelOfDifficulty" />
    <property role="3lZH7k" value="derive_from_presentation" />
    <ref role="M4eZT" to="tpck:fKAQMTA" resolve="integer" />
    <ref role="Qgau1" node="6miNywCXGsW" />
    <node concept="M4N5e" id="6miNywCXGsV" role="M5hS2">
      <property role="1uS6qo" value="High" />
      <property role="1uS6qv" value="3" />
    </node>
    <node concept="M4N5e" id="6miNywCXGsW" role="M5hS2">
      <property role="1uS6qv" value="2" />
      <property role="1uS6qo" value="Medium" />
    </node>
    <node concept="M4N5e" id="6miNywCXGt1" role="M5hS2">
      <property role="1uS6qo" value="Low" />
      <property role="1uS6qv" value="1" />
    </node>
  </node>
</model>

