<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0d718529-a4b7-41d3-bbfb-a61df392bdc1(PlanningLanguage.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="12" />
    <use id="602c36ad-cc55-47ff-8c40-73d7f12f035c" name="jetbrains.mps.lang.editor.forms" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="embf" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util.jar(JDK/)" />
    <import index="lui2" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.module(MPS.OpenAPI/)" />
    <import index="nlpl" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/java:jetbrains.mps.editor.runtime.commands(MPS.Editor/)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="ky5v" ref="r:59173ef2-9d94-4aad-b93e-31f525951c59(PlanningLanguage.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
      </concept>
      <concept id="1186403751766" name="jetbrains.mps.lang.editor.structure.FontStyleStyleClassItem" flags="ln" index="Vb9p2">
        <property id="1186403771423" name="style" index="Vbekb" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414860679" name="jetbrains.mps.lang.editor.structure.EditableStyleClassItem" flags="ln" index="VPxyj" />
      <concept id="1186414999511" name="jetbrains.mps.lang.editor.structure.UnderlinedStyleClassItem" flags="ln" index="VQ3r3">
        <property id="1214316229833" name="underlined" index="2USNnj" />
      </concept>
      <concept id="1186415722038" name="jetbrains.mps.lang.editor.structure.FontSizeStyleClassItem" flags="ln" index="VSNWy">
        <property id="1221209241505" name="value" index="1lJzqX" />
      </concept>
      <concept id="1214406466686" name="jetbrains.mps.lang.editor.structure.TextBackgroundColorSelectedStyleClassItem" flags="ln" index="30h1P$" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="602c36ad-cc55-47ff-8c40-73d7f12f035c" name="jetbrains.mps.lang.editor.forms">
      <concept id="312429380032619384" name="jetbrains.mps.lang.editor.forms.structure.CellModel_Checkbox" flags="ng" index="2yq9I_">
        <reference id="3696012239575138271" name="propertyDeclaration" index="225u1j" />
        <child id="8215612579904156902" name="label" index="2fqkNU" />
        <child id="1340057216891284122" name="ui" index="1563LE" />
      </concept>
      <concept id="1340057216891283515" name="jetbrains.mps.lang.editor.forms.structure.CheckboxUI_Text" flags="ng" index="1563Vb">
        <property id="1340057216891283520" name="falseText" index="1563UK" />
        <property id="1340057216891283518" name="trueText" index="1563Ve" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="6miNywCXDmA">
    <ref role="1XX52x" to="ky5v:6miNywCXCR1" resolve="DayOfWeek" />
    <node concept="3EZMnI" id="6miNywCXDmC" role="2wV5jI">
      <node concept="2yq9I_" id="6miNywCXDmM" role="3EZMnx">
        <ref role="225u1j" to="ky5v:6miNywCXCR5" resolve="Monday" />
        <node concept="1563Vb" id="6miNywCXDmO" role="1563LE">
          <property role="1563UK" value="[ ]" />
          <property role="1563Ve" value="[x]" />
          <node concept="30h1P$" id="6miNywD31kE" role="3F10Kt">
            <property role="Vb096" value="blue" />
          </node>
        </node>
        <node concept="3F0ifn" id="6miNywCXDmT" role="2fqkNU">
          <property role="3F0ifm" value="Mon" />
          <node concept="Vb9p2" id="6miNywD1NJ1" role="3F10Kt" />
        </node>
      </node>
      <node concept="2yq9I_" id="6miNywCXDn5" role="3EZMnx">
        <ref role="225u1j" to="ky5v:6miNywCXCR8" resolve="Tuesday" />
        <node concept="1563Vb" id="6miNywCXDn7" role="1563LE">
          <property role="1563UK" value="[ ]" />
          <property role="1563Ve" value="[x]" />
        </node>
        <node concept="3F0ifn" id="6miNywCXDni" role="2fqkNU">
          <property role="3F0ifm" value="Tue" />
          <node concept="Vb9p2" id="6miNywD1NJa" role="3F10Kt" />
        </node>
      </node>
      <node concept="2yq9I_" id="6miNywCXDn$" role="3EZMnx">
        <ref role="225u1j" to="ky5v:6miNywCXCRd" resolve="Wednesday" />
        <node concept="1563Vb" id="6miNywCXDnA" role="1563LE">
          <property role="1563UK" value="[ ]" />
          <property role="1563Ve" value="[x]" />
        </node>
        <node concept="3F0ifn" id="6miNywCXDnR" role="2fqkNU">
          <property role="3F0ifm" value="Wed" />
          <node concept="Vb9p2" id="6miNywD1NJd" role="3F10Kt" />
        </node>
      </node>
      <node concept="2yq9I_" id="6miNywCXDof" role="3EZMnx">
        <ref role="225u1j" to="ky5v:6miNywCXCRk" resolve="Thursday" />
        <node concept="1563Vb" id="6miNywCXDoh" role="1563LE">
          <property role="1563UK" value="[ ]" />
          <property role="1563Ve" value="[x]" />
        </node>
        <node concept="3F0ifn" id="6miNywCXDoC" role="2fqkNU">
          <property role="3F0ifm" value="Thu" />
          <node concept="Vb9p2" id="6miNywD1NJ4" role="3F10Kt" />
        </node>
      </node>
      <node concept="2yq9I_" id="6miNywCXDp6" role="3EZMnx">
        <ref role="225u1j" to="ky5v:6miNywCXCRt" resolve="Friday" />
        <node concept="1563Vb" id="6miNywCXDp8" role="1563LE">
          <property role="1563UK" value="[ ]" />
          <property role="1563Ve" value="[x]" />
        </node>
        <node concept="3F0ifn" id="6miNywCXDp_" role="2fqkNU">
          <property role="3F0ifm" value="Fri" />
          <node concept="Vb9p2" id="6miNywD1NJg" role="3F10Kt" />
        </node>
      </node>
      <node concept="2yq9I_" id="6miNywCXDq9" role="3EZMnx">
        <ref role="225u1j" to="ky5v:6miNywCXCRC" resolve="Saturday" />
        <node concept="1563Vb" id="6miNywCXDqb" role="1563LE">
          <property role="1563UK" value="[ ]" />
          <property role="1563Ve" value="[x]" />
        </node>
        <node concept="3F0ifn" id="6miNywCXDqI" role="2fqkNU">
          <property role="3F0ifm" value="Sat" />
          <node concept="Vb9p2" id="6miNywD1NJj" role="3F10Kt" />
        </node>
      </node>
      <node concept="2yq9I_" id="6miNywCXDs0" role="3EZMnx">
        <ref role="225u1j" to="ky5v:6miNywCXCRP" resolve="Sunday" />
        <node concept="1563Vb" id="6miNywCXDs2" role="1563LE">
          <property role="1563UK" value="[ ]" />
          <property role="1563Ve" value="[x]" />
        </node>
        <node concept="3F0ifn" id="6miNywCXDsF" role="2fqkNU">
          <property role="3F0ifm" value="Sun" />
          <node concept="Vb9p2" id="6miNywD1NJ7" role="3F10Kt" />
        </node>
      </node>
      <node concept="l2Vlx" id="6miNywCXDmF" role="2iSdaV" />
      <node concept="Vb9p2" id="6miNywD1ISN" role="3F10Kt" />
    </node>
  </node>
  <node concept="24kQdi" id="6miNywCXK2m">
    <ref role="1XX52x" to="ky5v:4XH0T1o_5k" resolve="ActivityTime" />
    <node concept="3EZMnI" id="6miNywCXK2o" role="2wV5jI">
      <node concept="l2Vlx" id="6miNywCXK2r" role="2iSdaV" />
      <node concept="3F0ifn" id="6miNywCY3xh" role="3EZMnx">
        <property role="3F0ifm" value="Name:" />
        <node concept="VQ3r3" id="6miNywD2$HC" role="3F10Kt">
          <property role="2USNnj" value="0" />
        </node>
        <node concept="lj46D" id="6miNywD3DuJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6miNywCXZYP" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        <node concept="ljvvj" id="6miNywCXZZd" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VechU" id="6miNywD2Nqh" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
      </node>
      <node concept="3F0ifn" id="6miNywCXK3e" role="3EZMnx">
        <property role="3F0ifm" value="Start Time:" />
        <node concept="lj46D" id="6miNywCZAXr" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="Vb9p2" id="6miNywD1wno" role="3F10Kt">
          <property role="Vbekb" value="BOLD" />
        </node>
        <node concept="VQ3r3" id="6miNywD2NqJ" role="3F10Kt">
          <property role="2USNnj" value="0" />
        </node>
      </node>
      <node concept="3F0A7n" id="6miNywCXK3z" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXyaT" resolve="startHour" />
        <node concept="VechU" id="6miNywD2Nqn" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
        <node concept="VPxyj" id="2lFfnZ$y9I7" role="3F10Kt" />
      </node>
      <node concept="3F0ifn" id="6miNywCXK3O" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="3F0A7n" id="6miNywCXK4l" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXyaX" resolve="startMin" />
        <node concept="VechU" id="6miNywD2Nqq" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
        <node concept="VPxyj" id="2lFfnZ$ynfE" role="3F10Kt" />
      </node>
      <node concept="3F0ifn" id="6miNywCXK4I" role="3EZMnx">
        <property role="3F0ifm" value="End Time:" />
        <node concept="Vb9p2" id="6miNywD1_cW" role="3F10Kt">
          <property role="Vbekb" value="BOLD" />
        </node>
        <node concept="VQ3r3" id="6miNywD2Nqy" role="3F10Kt">
          <property role="2USNnj" value="0" />
        </node>
      </node>
      <node concept="3F0A7n" id="6miNywCXK5b" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXCOj" resolve="endHour" />
        <node concept="VechU" id="6miNywD2NqR" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
        <node concept="VPxyj" id="2lFfnZ$ynfP" role="3F10Kt" />
      </node>
      <node concept="3F0ifn" id="6miNywCXK5G" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="3F0A7n" id="6miNywCXK6h" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXCOq" resolve="endMin" />
        <node concept="ljvvj" id="6miNywCXTrz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VechU" id="6miNywD2NqZ" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
        <node concept="VPxyj" id="2lFfnZ$yng2" role="3F10Kt" />
      </node>
      <node concept="3F1sOY" id="6miNywCXTrV" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
        <node concept="lj46D" id="6miNywCZFLt" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="Vb9p2" id="6miNywD1E2P" role="3F10Kt">
          <property role="Vbekb" value="PLAIN" />
        </node>
        <node concept="VechU" id="6miNywD36gF" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
      </node>
      <node concept="3F0ifn" id="1Ghyh6eMkL7" role="3EZMnx" />
    </node>
  </node>
  <node concept="24kQdi" id="6miNywCYfmE">
    <ref role="1XX52x" to="ky5v:2t50$NbHYca" resolve="Course" />
    <node concept="3EZMnI" id="6miNywCYlbm" role="2wV5jI">
      <node concept="l2Vlx" id="6miNywCYlbn" role="2iSdaV" />
      <node concept="3F0ifn" id="6miNywCYlbs" role="3EZMnx">
        <property role="3F0ifm" value="Course Name:" />
        <node concept="VQ3r3" id="6miNywD27mE" role="3F10Kt">
          <property role="2USNnj" value="0" />
        </node>
      </node>
      <node concept="3F0A7n" id="6miNywCYlb$" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        <node concept="VechU" id="6miNywD2Iwd" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
      </node>
      <node concept="3F0ifn" id="6miNywCYp0G" role="3EZMnx">
        <property role="3F0ifm" value="Level of Difficulty:" />
        <node concept="VQ3r3" id="6miNywD2hap" role="3F10Kt">
          <property role="2USNnj" value="0" />
        </node>
      </node>
      <node concept="3F0A7n" id="6miNywCYp0X" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXyVp" resolve="difficulty" />
        <node concept="ljvvj" id="6miNywCZUfe" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="VechU" id="6miNywD2Iwl" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
      </node>
      <node concept="3F0ifn" id="6miNywD13hg" role="3EZMnx">
        <property role="3F0ifm" value="Class Schedule:" />
        <node concept="VQ3r3" id="6miNywD2qW5" role="3F10Kt">
          <property role="2USNnj" value="0" />
        </node>
      </node>
      <node concept="3F0ifn" id="6miNywD22sd" role="3EZMnx">
        <node concept="ljvvj" id="6miNywD22sN" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6miNywD1cZS" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXCSw" resolve="classes" />
        <node concept="l2Vlx" id="6miNywD1cZU" role="2czzBx" />
        <node concept="pj6Ft" id="6miNywD1d0l" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6miNywD1hPS" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6miNywCZtjf">
    <ref role="1XX52x" to="ky5v:2t50$NbHYzI" resolve="Schedule" />
    <node concept="3EZMnI" id="6miNywD3$co" role="2wV5jI">
      <node concept="l2Vlx" id="6miNywD3$cp" role="2iSdaV" />
      <node concept="3F0ifn" id="6miNywD3$cu" role="3EZMnx">
        <property role="3F0ifm" value="Schedule Name:" />
        <node concept="VSNWy" id="6miNywD3T7f" role="3F10Kt">
          <property role="1lJzqX" value="15" />
        </node>
      </node>
      <node concept="3F0A7n" id="6miNywD3$cA" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        <node concept="ljvvj" id="6miNywD3$cG" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="Vb9p2" id="6miNywD3IFt" role="3F10Kt">
          <property role="Vbekb" value="BOLD" />
        </node>
        <node concept="VechU" id="6miNywD43yg" role="3F10Kt">
          <property role="Vb096" value="blue" />
        </node>
        <node concept="VSNWy" id="6miNywD3T6F" role="3F10Kt">
          <property role="1lJzqX" value="15" />
        </node>
      </node>
      <node concept="3F0ifn" id="1Ghyh6eMkM0" role="3EZMnx">
        <node concept="ljvvj" id="1Ghyh6eMkMr" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6miNywD4jcO" role="3EZMnx">
        <property role="3F0ifm" value="Courses:" />
        <node concept="ljvvj" id="6miNywD4jd$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6miNywD3$cS" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:2t50$NbHYD5" resolve="courses" />
        <node concept="l2Vlx" id="6miNywD3$cU" role="2czzBx" />
        <node concept="pj6Ft" id="6miNywD3$d5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6miNywD3$d8" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6miNywD3$du" role="3EZMnx">
        <property role="3F0ifm" value="Meal Time:" />
        <node concept="ljvvj" id="6miNywD3$f1" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6miNywD3$fp" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXCSr" resolve="MealTime" />
        <node concept="l2Vlx" id="6miNywD3$fr" role="2czzBx" />
        <node concept="ljvvj" id="6miNywD3$fM" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="6miNywD4dWW" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6miNywD3$gg" role="3EZMnx">
        <property role="3F0ifm" value="Rest Time:" />
        <node concept="ljvvj" id="6miNywD3$gG" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6miNywD3$hI" role="3EZMnx">
        <ref role="1NtTu8" to="ky5v:6miNywCXCSz" resolve="RestTime" />
        <node concept="l2Vlx" id="6miNywD3$hK" role="2czzBx" />
        <node concept="pj6Ft" id="6miNywD4dX1" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
</model>

