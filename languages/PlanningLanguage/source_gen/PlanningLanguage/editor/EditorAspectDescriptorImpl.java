package PlanningLanguage.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.EditorAspectDescriptorBase;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import jetbrains.mps.openapi.editor.descriptor.ConceptEditor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Collections;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public class EditorAspectDescriptorImpl extends EditorAspectDescriptorBase {
  @NotNull
  public Collection<ConceptEditor> getDeclaredEditors(SAbstractConcept concept) {
    SAbstractConcept cncpt = ((SAbstractConcept) concept);
    switch (conceptIndex.index(cncpt)) {
      case 0:
        return Collections.<ConceptEditor>singletonList(new ActivityTime_Editor());
      case 1:
        return Collections.<ConceptEditor>singletonList(new Course_Editor());
      case 2:
        return Collections.<ConceptEditor>singletonList(new DayOfWeek_Editor());
      case 3:
        return Collections.<ConceptEditor>singletonList(new Schedule_Editor());
      default:
    }
    return Collections.<ConceptEditor>emptyList();
  }



  private static final ConceptSwitchIndex conceptIndex = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x48455107003a4d59L, 0x8376b8d95ea4f6edL, 0x13db40e41625154L), MetaIdFactory.conceptId(0x48455107003a4d59L, 0x8376b8d95ea4f6edL, 0x2745024ccbb7e30aL), MetaIdFactory.conceptId(0x48455107003a4d59L, 0x8376b8d95ea4f6edL, 0x6592ce2828f68dc1L), MetaIdFactory.conceptId(0x48455107003a4d59L, 0x8376b8d95ea4f6edL, 0x2745024ccbb7e8eeL)).seal();
}
