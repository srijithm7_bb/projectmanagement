<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7a477b4a-a107-423c-b9af-c8e93e4e89f4(main@generator)">
  <persistence version="9" />
  <languages>
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="2" />
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="ky5v" ref="r:59173ef2-9d94-4aad-b93e-31f525951c59(PlanningLanguage.structure)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="j4ye" ref="76feab90-0c10-4cdc-96a2-84885d0f3447/java:com.mindfusion.common(PlanningLanguage.sandbox/)" />
    <import index="tr4j" ref="76feab90-0c10-4cdc-96a2-84885d0f3447/java:com.mindfusion.drawing(PlanningLanguage.sandbox/)" />
    <import index="spdm" ref="76feab90-0c10-4cdc-96a2-84885d0f3447/java:com.mindfusion.scheduling.model(PlanningLanguage.sandbox/)" />
    <import index="1gjj" ref="76feab90-0c10-4cdc-96a2-84885d0f3447/java:com.mindfusion.scheduling(PlanningLanguage.sandbox/)" />
    <import index="25x5" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.text(JDK/)" />
    <import index="hyam" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.event(JDK/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="r791" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.text(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1215695189714" name="jetbrains.mps.baseLanguage.structure.PlusAssignmentExpression" flags="nn" index="d57v9" />
      <concept id="1153422105332" name="jetbrains.mps.baseLanguage.structure.RemExpression" flags="nn" index="2dk9JS" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1173175405605" name="jetbrains.mps.baseLanguage.structure.ArrayAccessExpression" flags="nn" index="AH0OO">
        <child id="1173175577737" name="index" index="AHEQo" />
        <child id="1173175590490" name="array" index="AHHXb" />
      </concept>
      <concept id="1188220165133" name="jetbrains.mps.baseLanguage.structure.ArrayLiteral" flags="nn" index="2BsdOp">
        <child id="1188220173759" name="item" index="2BsfMF" />
      </concept>
      <concept id="1095950406618" name="jetbrains.mps.baseLanguage.structure.DivExpression" flags="nn" index="FJ1c_" />
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1083260308424" name="jetbrains.mps.baseLanguage.structure.EnumConstantReference" flags="nn" index="Rm8GO">
        <reference id="1083260308426" name="enumConstantDeclaration" index="Rm8GQ" />
        <reference id="1144432896254" name="enumClass" index="1Px2BO" />
      </concept>
      <concept id="1164879751025" name="jetbrains.mps.baseLanguage.structure.TryCatchStatement" flags="nn" index="SfApY">
        <child id="1164879758292" name="body" index="SfCbr" />
        <child id="1164903496223" name="catchClause" index="TEbGg" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1164903280175" name="jetbrains.mps.baseLanguage.structure.CatchClause" flags="nn" index="TDmWw">
        <child id="1164903359218" name="catchBody" index="TDEfX" />
        <child id="1164903359217" name="throwable" index="TDEfY" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA" />
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P" />
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534513062" name="jetbrains.mps.baseLanguage.structure.DoubleType" flags="in" index="10P55v" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1164879685961" name="throwsItem" index="Sfmx6" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_" />
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
        <child id="1206060520071" name="elsifClauses" index="3eNLev" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1206060495898" name="jetbrains.mps.baseLanguage.structure.ElsifClause" flags="ng" index="3eNFk2">
        <child id="1206060619838" name="condition" index="3eO9$A" />
        <child id="1206060644605" name="statementList" index="3eOfB_" />
      </concept>
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1081855346303" name="jetbrains.mps.baseLanguage.structure.BreakStatement" flags="nn" index="3zACq4" />
      <concept id="1184950988562" name="jetbrains.mps.baseLanguage.structure.ArrayCreator" flags="nn" index="3$_iS1">
        <child id="1184951007469" name="componentType" index="3$_nBY" />
        <child id="1184952969026" name="dimensionExpression" index="3$GQph" />
      </concept>
      <concept id="1184952934362" name="jetbrains.mps.baseLanguage.structure.DimensionExpression" flags="nn" index="3$GHV9">
        <child id="1184953288404" name="expression" index="3$I4v7" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144226303539" name="jetbrains.mps.baseLanguage.structure.ForeachStatement" flags="nn" index="1DcWWT">
        <child id="1144226360166" name="iterable" index="1DdaDG" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1082113931046" name="jetbrains.mps.baseLanguage.structure.ContinueStatement" flags="nn" index="3N13vt" />
      <concept id="1208890769693" name="jetbrains.mps.baseLanguage.structure.ArrayLengthOperation" flags="nn" index="1Rwk04" />
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1200397529627" name="jetbrains.mps.baseLanguage.structure.CharConstant" flags="nn" index="1Xhbcc">
        <property id="1200397540847" name="charConstant" index="1XhdNS" />
      </concept>
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167514678247" name="rootMappingRule" index="3lj3bC" />
      </concept>
      <concept id="1168619357332" name="jetbrains.mps.lang.generator.structure.RootTemplateAnnotation" flags="lg" index="n94m4">
        <reference id="1168619429071" name="applicableConcept" index="n9lRv" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1167514355419" name="jetbrains.mps.lang.generator.structure.Root_MappingRule" flags="lg" index="3lhOvk">
        <reference id="1167514355421" name="template" index="3lhOvi" />
      </concept>
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
      <concept id="1118786554307" name="jetbrains.mps.lang.generator.structure.LoopMacro" flags="ln" index="1WS0z7">
        <child id="1167952069335" name="sourceNodesQuery" index="3Jn$fo" />
      </concept>
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1218047638031" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_CreateUniqueName" flags="nn" index="2piZGk">
        <child id="1218047638032" name="baseName" index="2piZGb" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="name_DebugInfo" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
      <concept id="779128492853369165" name="jetbrains.mps.lang.core.structure.SideTransformInfo" flags="ng" index="1KehLL">
        <property id="779128492853934523" name="cellId" index="1K8rM7" />
        <property id="779128492853699361" name="side" index="1Kfyot" />
      </concept>
      <concept id="709746936026466394" name="jetbrains.mps.lang.core.structure.ChildAttribute" flags="ng" index="3VBwX9">
        <property id="709746936026609031" name="linkId" index="3V$3ak" />
        <property id="709746936026609029" name="role_DebugInfo" index="3V$3am" />
      </concept>
      <concept id="4452961908202556907" name="jetbrains.mps.lang.core.structure.BaseCommentAttribute" flags="ng" index="1X3_iC">
        <child id="3078666699043039389" name="commentedNode" index="8Wnug" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="4Q4iDQr7JUx">
    <property role="TrG5h" value="main" />
    <node concept="3lhOvk" id="2lFfnZ$$V1y" role="3lj3bC">
      <ref role="30HIoZ" to="ky5v:2t50$NbHYzI" resolve="Schedule" />
      <ref role="3lhOvi" node="2lFfnZ$$V1C" resolve="Schedule" />
    </node>
  </node>
  <node concept="312cEu" id="2lFfnZ$$V1C">
    <property role="TrG5h" value="Schedule" />
    <node concept="312cEg" id="3rIOsIrxzmG" role="jymVt">
      <property role="TrG5h" value="activities" />
      <node concept="3uibUv" id="3rIOsIrxr87" role="1tU5fm">
        <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
        <node concept="3uibUv" id="3rIOsIrxzi2" role="11_B2D">
          <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
        </node>
      </node>
      <node concept="2ShNRf" id="3rIOsIrxFpg" role="33vP2m">
        <node concept="1pGfFk" id="3rIOsIrxG0$" role="2ShVmc">
          <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
          <node concept="3uibUv" id="3rIOsIrxHEb" role="1pMfVU">
            <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="3rIOsIry918" role="jymVt">
      <property role="TrG5h" value="courses" />
      <node concept="3uibUv" id="3rIOsIry234" role="1tU5fm">
        <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
        <node concept="3uibUv" id="3rIOsIry8WN" role="11_B2D">
          <ref role="3uigEE" node="2lFfnZ$MNHC" resolve="Schedule.Course" />
        </node>
      </node>
      <node concept="2ShNRf" id="3rIOsIryfYz" role="33vP2m">
        <node concept="1pGfFk" id="3rIOsIryg_R" role="2ShVmc">
          <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
          <node concept="3uibUv" id="3rIOsIryi6z" role="1pMfVU">
            <ref role="3uigEE" node="2lFfnZ$MNHC" resolve="Schedule.Course" />
          </node>
        </node>
      </node>
    </node>
    <node concept="312cEg" id="2lFfnZ_klFn" role="jymVt">
      <property role="TrG5h" value="scheduleArray" />
      <node concept="10Q1$e" id="2lFfnZ_kl4R" role="1tU5fm">
        <node concept="17QB3L" id="2lFfnZ_kcT$" role="10Q1$1" />
      </node>
    </node>
    <node concept="Wx3nA" id="2OB0AerrXhs" role="jymVt">
      <property role="TrG5h" value="calendar" />
      <node concept="3uibUv" id="2OB0AerrVpx" role="1tU5fm">
        <ref role="3uigEE" to="1gjj:~Calendar" resolve="Calendar" />
      </node>
      <node concept="2ShNRf" id="2OB0Aers8HO" role="33vP2m">
        <node concept="HV5vD" id="2OB0AersbiD" role="2ShVmc">
          <ref role="HV5vE" to="1gjj:~Calendar" resolve="Calendar" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="2OB0Aerpgi1" role="jymVt" />
    <node concept="3clFbW" id="2lFfnZ_gNmR" role="jymVt">
      <node concept="3cqZAl" id="2lFfnZ_gNmS" role="3clF45" />
      <node concept="3clFbS" id="2lFfnZ_gNmU" role="3clF47">
        <node concept="3clFbF" id="2lFfnZ$N9Bj" role="3cqZAp">
          <node concept="2OqwBi" id="2lFfnZ$NaXp" role="3clFbG">
            <node concept="Xjq3P" id="2lFfnZ_h9F6" role="2Oq$k0" />
            <node concept="liA8E" id="2lFfnZ$Nd1m" role="2OqNvi">
              <ref role="37wK5l" node="2lFfnZ$N66X" resolve="addCourses" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2lFfnZ$AIYf" role="3cqZAp">
          <node concept="2OqwBi" id="2lFfnZ$AK8Z" role="3clFbG">
            <node concept="Xjq3P" id="2lFfnZ_hajs" role="2Oq$k0" />
            <node concept="liA8E" id="2lFfnZ$ALML" role="2OqNvi">
              <ref role="37wK5l" node="2lFfnZ$AHa$" resolve="addMealTime" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2lFfnZ$LKcS" role="3cqZAp">
          <node concept="2OqwBi" id="2lFfnZ$LLuH" role="3clFbG">
            <node concept="Xjq3P" id="2lFfnZ_haVC" role="2Oq$k0" />
            <node concept="liA8E" id="2lFfnZ$LNv_" role="2OqNvi">
              <ref role="37wK5l" node="2lFfnZ$LCHI" resolve="addRestTime" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2lFfnZ_q9l8" role="3cqZAp">
          <node concept="2OqwBi" id="2lFfnZ_q9XZ" role="3clFbG">
            <node concept="Xjq3P" id="2lFfnZ_q9l6" role="2Oq$k0" />
            <node concept="liA8E" id="2lFfnZ_qnYK" role="2OqNvi">
              <ref role="37wK5l" node="2lFfnZ_qj4z" resolve="prepareSchedule" />
            </node>
          </node>
        </node>
        <node concept="SfApY" id="7bbHeTWxNO3" role="3cqZAp">
          <node concept="3clFbS" id="7bbHeTWxNO5" role="SfCbr">
            <node concept="3clFbF" id="3rIOsIrtMJV" role="3cqZAp">
              <node concept="2OqwBi" id="3rIOsIrtNnX" role="3clFbG">
                <node concept="Xjq3P" id="3rIOsIrtMJT" role="2Oq$k0" />
                <node concept="liA8E" id="3rIOsIrupp3" role="2OqNvi">
                  <ref role="37wK5l" node="3rIOsIru98Q" resolve="loadSchedule" />
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="7bbHeTWxNO4" role="3cqZAp" />
          </node>
          <node concept="TDmWw" id="7bbHeTWxNO6" role="TEbGg">
            <node concept="3cpWsn" id="7bbHeTWxNO8" role="TDEfY">
              <property role="TrG5h" value="e" />
              <node concept="3uibUv" id="7bbHeTWxOwK" role="1tU5fm">
                <ref role="3uigEE" to="25x5:~ParseException" resolve="ParseException" />
              </node>
            </node>
            <node concept="3clFbS" id="7bbHeTWxNOc" role="TDEfX">
              <node concept="3clFbH" id="7bbHeTWxOEf" role="3cqZAp" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2lFfnZ_gKzJ" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="7bbHeTWkxvV" role="jymVt" />
    <node concept="3clFb_" id="3rIOsIru98Q" role="jymVt">
      <property role="TrG5h" value="loadSchedule" />
      <node concept="3clFbS" id="3rIOsIru98T" role="3clF47">
        <node concept="3clFbF" id="5t_d$dJGUVD" role="3cqZAp">
          <node concept="2OqwBi" id="5t_d$dJGUVA" role="3clFbG">
            <node concept="10M0yZ" id="5t_d$dJGUVB" role="2Oq$k0">
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
            </node>
            <node concept="liA8E" id="5t_d$dJGUVC" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="Xl_RD" id="5t_d$dJGYtP" role="37wK5m">
                <property role="Xl_RC" value="Load Schedule called" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWgFj7" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWgFja" role="3cpWs9">
            <property role="TrG5h" value="numberOfDays" />
            <node concept="10Oyi0" id="7bbHeTWgFj5" role="1tU5fm" />
            <node concept="3cmrfG" id="7bbHeTWgFZP" role="33vP2m">
              <property role="3cmrfH" value="50" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2OB0Aerv9DD" role="3cqZAp">
          <node concept="1rXfSq" id="2OB0Aerv9DB" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JFrame.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
            <node concept="2ShNRf" id="2OB0Aervd3B" role="37wK5m">
              <node concept="1pGfFk" id="2OB0Aervfml" role="2ShVmc">
                <ref role="37wK5l" to="z60i:~BorderLayout.&lt;init&gt;()" resolve="BorderLayout" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AerF6Wp" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AerF6Wq" role="3cpWs9">
            <property role="TrG5h" value="task" />
            <node concept="3uibUv" id="2OB0AerF6Wr" role="1tU5fm">
              <ref role="3uigEE" node="2OB0AerCHzQ" resolve="Schedule.TaskCreatePanel" />
            </node>
            <node concept="2ShNRf" id="2OB0AerFaLX" role="33vP2m">
              <node concept="1pGfFk" id="2OB0AerFcLg" role="2ShVmc">
                <ref role="37wK5l" node="2OB0AerD3_v" resolve="Schedule.TaskCreatePanel" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWgLWX" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWgN4C" role="3clFbG">
            <node concept="1rXfSq" id="7bbHeTWgLWV" role="2Oq$k0">
              <ref role="37wK5l" to="dxuu:~JFrame.getContentPane():java.awt.Container" resolve="getContentPane" />
            </node>
            <node concept="liA8E" id="7bbHeTWgOfw" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="2OB0Aerscvu" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
              </node>
              <node concept="10M0yZ" id="2OB0AerkAkW" role="37wK5m">
                <ref role="3cqZAo" to="z60i:~BorderLayout.CENTER" resolve="CENTER" />
                <ref role="1PxDUh" to="z60i:~BorderLayout" resolve="BorderLayout" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2OB0AerFcUW" role="3cqZAp">
          <node concept="2OqwBi" id="2OB0AerFcUX" role="3clFbG">
            <node concept="1rXfSq" id="2OB0AerFcUY" role="2Oq$k0">
              <ref role="37wK5l" to="dxuu:~JFrame.getContentPane():java.awt.Container" resolve="getContentPane" />
            </node>
            <node concept="liA8E" id="2OB0AerFcUZ" role="2OqNvi">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="2OB0AerFigI" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerF6Wq" resolve="task" />
              </node>
              <node concept="10M0yZ" id="2OB0AerFiJI" role="37wK5m">
                <ref role="3cqZAo" to="z60i:~BorderLayout.WEST" resolve="WEST" />
                <ref role="1PxDUh" to="z60i:~BorderLayout" resolve="BorderLayout" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2OB0Aervr57" role="3cqZAp" />
        <node concept="3clFbF" id="7bbHeTWgUut" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWgW3z" role="3clFbG">
            <node concept="37vLTw" id="2OB0AersdWS" role="2Oq$k0">
              <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
            </node>
            <node concept="liA8E" id="7bbHeTWgZ7r" role="2OqNvi">
              <ref role="37wK5l" to="1gjj:~Calendar.setCurrentView(com.mindfusion.scheduling.CalendarView):void" resolve="setCurrentView" />
              <node concept="Rm8GO" id="2OB0Aer4pWZ" role="37wK5m">
                <ref role="Rm8GQ" to="1gjj:~CalendarView.Timetable" resolve="Timetable" />
                <ref role="1Px2BO" to="1gjj:~CalendarView" resolve="CalendarView" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWh31y" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWh5Gu" role="3clFbG">
            <node concept="37vLTw" id="2OB0Aersflo" role="2Oq$k0">
              <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
            </node>
            <node concept="liA8E" id="7bbHeTWh8T2" role="2OqNvi">
              <ref role="37wK5l" to="1gjj:~Calendar.setTheme(com.mindfusion.scheduling.ThemeType):void" resolve="setTheme" />
              <node concept="Rm8GO" id="5t_d$dJrFer" role="37wK5m">
                <ref role="Rm8GQ" to="1gjj:~ThemeType.Windows2003" resolve="Windows2003" />
                <ref role="1Px2BO" to="1gjj:~ThemeType" resolve="ThemeType" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWhdA2" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWhdA3" role="3cpWs9">
            <property role="TrG5h" value="today" />
            <node concept="3uibUv" id="7bbHeTWhdA4" role="1tU5fm">
              <ref role="3uigEE" to="j4ye:~DateTime" resolve="DateTime" />
            </node>
            <node concept="2YIFZM" id="7bbHeTWhi2x" role="33vP2m">
              <ref role="37wK5l" to="j4ye:~DateTime.today():com.mindfusion.common.DateTime" resolve="today" />
              <ref role="1Pybhc" to="j4ye:~DateTime" resolve="DateTime" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWhjqN" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWhw$8" role="3clFbG">
            <node concept="2OqwBi" id="7bbHeTWhtyd" role="2Oq$k0">
              <node concept="2OqwBi" id="7bbHeTWhpuz" role="2Oq$k0">
                <node concept="37vLTw" id="2OB0AersgHV" role="2Oq$k0">
                  <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
                </node>
                <node concept="liA8E" id="7bbHeTWhsOg" role="2OqNvi">
                  <ref role="37wK5l" to="1gjj:~Calendar.getTimetableSettings():com.mindfusion.scheduling.TimetableSettings" resolve="getTimetableSettings" />
                </node>
              </node>
              <node concept="liA8E" id="7bbHeTWhvrh" role="2OqNvi">
                <ref role="37wK5l" to="1gjj:~TimetableSettings.getDates():com.mindfusion.scheduling.DateList" resolve="getDates" />
              </node>
            </node>
            <node concept="liA8E" id="7bbHeTWh_cj" role="2OqNvi">
              <ref role="37wK5l" to="j4ye:~BaseList.clear():void" resolve="clear" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2OB0Aer7W$y" role="3cqZAp" />
        <node concept="1Dw8fO" id="7bbHeTWhBfQ" role="3cqZAp">
          <node concept="3clFbS" id="7bbHeTWhBfS" role="2LFqv$">
            <node concept="3clFbF" id="7bbHeTWhKd3" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWhSUH" role="3clFbG">
                <node concept="2OqwBi" id="7bbHeTWhPVQ" role="2Oq$k0">
                  <node concept="2OqwBi" id="7bbHeTWhMeT" role="2Oq$k0">
                    <node concept="37vLTw" id="2OB0Aersi1x" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
                    </node>
                    <node concept="liA8E" id="7bbHeTWhP74" role="2OqNvi">
                      <ref role="37wK5l" to="1gjj:~Calendar.getTimetableSettings():com.mindfusion.scheduling.TimetableSettings" resolve="getTimetableSettings" />
                    </node>
                  </node>
                  <node concept="liA8E" id="7bbHeTWhRLC" role="2OqNvi">
                    <ref role="37wK5l" to="1gjj:~TimetableSettings.getDates():com.mindfusion.scheduling.DateList" resolve="getDates" />
                  </node>
                </node>
                <node concept="liA8E" id="7bbHeTWhWlH" role="2OqNvi">
                  <ref role="37wK5l" to="j4ye:~BaseList.add(java.lang.Object):boolean" resolve="add" />
                  <node concept="2OqwBi" id="7bbHeTWhZ2C" role="37wK5m">
                    <node concept="37vLTw" id="7bbHeTWhY0T" role="2Oq$k0">
                      <ref role="3cqZAo" node="7bbHeTWhdA3" resolve="today" />
                    </node>
                    <node concept="liA8E" id="7bbHeTWi03t" role="2OqNvi">
                      <ref role="37wK5l" to="j4ye:~DateTime.addDays(double):com.mindfusion.common.DateTime" resolve="addDays" />
                      <node concept="37vLTw" id="7bbHeTWi0Gh" role="37wK5m">
                        <ref role="3cqZAo" node="7bbHeTWhBfT" resolve="i" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="7bbHeTWhBfT" role="1Duv9x">
            <property role="TrG5h" value="i" />
            <node concept="10Oyi0" id="7bbHeTWhCau" role="1tU5fm" />
            <node concept="3cmrfG" id="7bbHeTWhCJr" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3eOVzh" id="7bbHeTWhFnO" role="1Dwp0S">
            <node concept="37vLTw" id="7bbHeTWhHcb" role="3uHU7w">
              <ref role="3cqZAo" node="7bbHeTWgFja" resolve="numberOfDays" />
            </node>
            <node concept="37vLTw" id="7bbHeTWhDj9" role="3uHU7B">
              <ref role="3cqZAo" node="7bbHeTWhBfT" resolve="i" />
            </node>
          </node>
          <node concept="3uNrnE" id="7bbHeTWhIP3" role="1Dwrff">
            <node concept="37vLTw" id="7bbHeTWhIP5" role="2$L3a6">
              <ref role="3cqZAo" node="7bbHeTWhBfT" resolve="i" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2OB0Aer7ZdR" role="3cqZAp" />
        <node concept="3clFbF" id="7bbHeTWi2xK" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWi9B5" role="3clFbG">
            <node concept="2OqwBi" id="7bbHeTWi4R7" role="2Oq$k0">
              <node concept="37vLTw" id="2OB0AersjyV" role="2Oq$k0">
                <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
              </node>
              <node concept="liA8E" id="7bbHeTWi8Dz" role="2OqNvi">
                <ref role="37wK5l" to="1gjj:~Calendar.getTimetableSettings():com.mindfusion.scheduling.TimetableSettings" resolve="getTimetableSettings" />
              </node>
            </node>
            <node concept="liA8E" id="7bbHeTWibWS" role="2OqNvi">
              <ref role="37wK5l" to="1gjj:~TimetableSettings.setVisibleColumns(java.lang.Integer):void" resolve="setVisibleColumns" />
              <node concept="3cmrfG" id="7bbHeTWicEK" role="37wK5m">
                <property role="3cmrfH" value="7" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5t_d$dJyJ47" role="3cqZAp">
          <node concept="2OqwBi" id="5t_d$dJyJ44" role="3clFbG">
            <node concept="10M0yZ" id="5t_d$dJyJ45" role="2Oq$k0">
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
            </node>
            <node concept="liA8E" id="5t_d$dJyJ46" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="Xl_RD" id="5t_d$dJyMMl" role="37wK5m">
                <property role="Xl_RC" value="" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1Dw8fO" id="7bbHeTWif6$" role="3cqZAp">
          <node concept="3clFbS" id="7bbHeTWif6A" role="2LFqv$">
            <node concept="3clFbJ" id="5t_d$dJIcxb" role="3cqZAp">
              <node concept="3clFbS" id="5t_d$dJIcxd" role="3clFbx">
                <node concept="3N13vt" id="5t_d$dJIMTE" role="3cqZAp" />
              </node>
              <node concept="3clFbC" id="5t_d$dJILac" role="3clFbw">
                <node concept="2OqwBi" id="5t_d$dJIrH0" role="3uHU7B">
                  <node concept="2OqwBi" id="5t_d$dJIeRb" role="2Oq$k0">
                    <node concept="AH0OO" id="5t_d$dJIdHs" role="2Oq$k0">
                      <node concept="37vLTw" id="5t_d$dJIdHt" role="AHEQo">
                        <ref role="3cqZAo" node="7bbHeTWif6B" resolve="j" />
                      </node>
                      <node concept="37vLTw" id="5t_d$dJIdHu" role="AHHXb">
                        <ref role="3cqZAo" node="2lFfnZ_klFn" resolve="scheduleArray" />
                      </node>
                    </node>
                    <node concept="liA8E" id="5t_d$dJIrem" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.trim():java.lang.String" resolve="trim" />
                    </node>
                  </node>
                  <node concept="liA8E" id="5t_d$dJIC74" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.length():int" resolve="length" />
                  </node>
                </node>
                <node concept="3cmrfG" id="5t_d$dJIKgv" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWiH5Z" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWiH5W" role="3clFbG">
                <node concept="10M0yZ" id="7bbHeTWiH5X" role="2Oq$k0">
                  <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                  <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                </node>
                <node concept="liA8E" id="7bbHeTWiH5Y" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                  <node concept="AH0OO" id="7bbHeTWiLxZ" role="37wK5m">
                    <node concept="37vLTw" id="7bbHeTWiMEr" role="AHEQo">
                      <ref role="3cqZAo" node="7bbHeTWif6B" resolve="j" />
                    </node>
                    <node concept="37vLTw" id="7bbHeTWiIAy" role="AHHXb">
                      <ref role="3cqZAo" node="2lFfnZ_klFn" resolve="scheduleArray" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="7bbHeTWjnJo" role="3cqZAp">
              <node concept="3cpWsn" id="7bbHeTWjnJr" role="3cpWs9">
                <property role="TrG5h" value="splitStr" />
                <node concept="10Q1$e" id="7bbHeTWjnZ7" role="1tU5fm">
                  <node concept="17QB3L" id="7bbHeTWjnJm" role="10Q1$1" />
                </node>
                <node concept="2OqwBi" id="7bbHeTWjshh" role="33vP2m">
                  <node concept="AH0OO" id="7bbHeTWjqty" role="2Oq$k0">
                    <node concept="37vLTw" id="7bbHeTWjrLs" role="AHEQo">
                      <ref role="3cqZAo" node="7bbHeTWif6B" resolve="j" />
                    </node>
                    <node concept="37vLTw" id="7bbHeTWjp7f" role="AHHXb">
                      <ref role="3cqZAo" node="2lFfnZ_klFn" resolve="scheduleArray" />
                    </node>
                  </node>
                  <node concept="liA8E" id="7bbHeTWjCwT" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.split(java.lang.String):java.lang.String[]" resolve="split" />
                    <node concept="Xl_RD" id="7bbHeTWjDkG" role="37wK5m">
                      <property role="Xl_RC" value="," />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="7bbHeTWjhYV" role="3cqZAp">
              <node concept="3cpWsn" id="7bbHeTWjhYY" role="3cpWs9">
                <property role="TrG5h" value="num1" />
                <node concept="10Oyi0" id="7bbHeTWjhYT" role="1tU5fm" />
                <node concept="2YIFZM" id="7bbHeTWjjWX" role="33vP2m">
                  <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                  <ref role="37wK5l" to="wyt6:~Integer.parseInt(java.lang.String):int" resolve="parseInt" />
                  <node concept="AH0OO" id="7bbHeTWjEWZ" role="37wK5m">
                    <node concept="3cmrfG" id="7bbHeTWjFyT" role="AHEQo">
                      <property role="3cmrfH" value="5" />
                    </node>
                    <node concept="37vLTw" id="7bbHeTWjE9U" role="AHHXb">
                      <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="7bbHeTWjF_R" role="3cqZAp">
              <node concept="3cpWsn" id="7bbHeTWjF_S" role="3cpWs9">
                <property role="TrG5h" value="num2" />
                <node concept="10Oyi0" id="7bbHeTWjF_T" role="1tU5fm" />
                <node concept="2YIFZM" id="7bbHeTWjF_U" role="33vP2m">
                  <ref role="37wK5l" to="wyt6:~Integer.parseInt(java.lang.String):int" resolve="parseInt" />
                  <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
                  <node concept="AH0OO" id="7bbHeTWjHDD" role="37wK5m">
                    <node concept="3cmrfG" id="7bbHeTWjIf_" role="AHEQo">
                      <property role="3cmrfH" value="6" />
                    </node>
                    <node concept="37vLTw" id="7bbHeTWjF_X" role="AHHXb">
                      <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="7bbHeTWk7dw" role="3cqZAp">
              <node concept="3clFbS" id="7bbHeTWk7dy" role="3clFbx">
                <node concept="3cpWs8" id="7bbHeTWkhtT" role="3cqZAp">
                  <node concept="3cpWsn" id="7bbHeTWkhtW" role="3cpWs9">
                    <property role="TrG5h" value="nightTime" />
                    <node concept="17QB3L" id="7bbHeTWkhtR" role="1tU5fm" />
                    <node concept="AH0OO" id="7bbHeTWkjpr" role="33vP2m">
                      <node concept="3cmrfG" id="7bbHeTWkjXU" role="AHEQo">
                        <property role="3cmrfH" value="5" />
                      </node>
                      <node concept="37vLTw" id="7bbHeTWkiBO" role="AHHXb">
                        <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="7bbHeTWkkyI" role="3cqZAp">
                  <node concept="3cpWsn" id="7bbHeTWkkyL" role="3cpWs9">
                    <property role="TrG5h" value="mrngTime" />
                    <node concept="17QB3L" id="7bbHeTWkkyG" role="1tU5fm" />
                    <node concept="AH0OO" id="7bbHeTWknlJ" role="33vP2m">
                      <node concept="3cmrfG" id="7bbHeTWknUg" role="AHEQo">
                        <property role="3cmrfH" value="6" />
                      </node>
                      <node concept="37vLTw" id="7bbHeTWkm$6" role="AHHXb">
                        <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="7bbHeTWkp2c" role="3cqZAp">
                  <node concept="37vLTI" id="7bbHeTWkwNb" role="3clFbG">
                    <node concept="Xl_RD" id="7bbHeTWkxoe" role="37vLTx">
                      <property role="Xl_RC" value="2400" />
                    </node>
                    <node concept="AH0OO" id="7bbHeTWkpHf" role="37vLTJ">
                      <node concept="3cmrfG" id="7bbHeTWkqhG" role="AHEQo">
                        <property role="3cmrfH" value="6" />
                      </node>
                      <node concept="37vLTw" id="7bbHeTWkp2a" role="AHHXb">
                        <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="7bbHeTWtZgU" role="3cqZAp">
                  <node concept="3cpWsn" id="7bbHeTWtZgV" role="3cpWs9">
                    <property role="TrG5h" value="app" />
                    <node concept="3uibUv" id="7bbHeTWtZgW" role="1tU5fm">
                      <ref role="3uigEE" to="spdm:~Appointment" resolve="Appointment" />
                    </node>
                    <node concept="1rXfSq" id="7bbHeTWu0tN" role="33vP2m">
                      <ref role="37wK5l" node="7bbHeTWkPTp" resolve="appointmentMake" />
                      <node concept="37vLTw" id="7bbHeTWu2k1" role="37wK5m">
                        <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="7bbHeTWu3tS" role="3cqZAp">
                  <node concept="2OqwBi" id="7bbHeTWucHC" role="3clFbG">
                    <node concept="2OqwBi" id="7bbHeTWu8Ye" role="2Oq$k0">
                      <node concept="2OqwBi" id="7bbHeTWu4Th" role="2Oq$k0">
                        <node concept="37vLTw" id="5t_d$dJxhx5" role="2Oq$k0">
                          <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
                        </node>
                        <node concept="liA8E" id="7bbHeTWu7LO" role="2OqNvi">
                          <ref role="37wK5l" to="1gjj:~Calendar.getSchedule():com.mindfusion.scheduling.model.Schedule" resolve="getSchedule" />
                        </node>
                      </node>
                      <node concept="liA8E" id="7bbHeTWubiV" role="2OqNvi">
                        <ref role="37wK5l" to="spdm:~Schedule.getAllItems():com.mindfusion.scheduling.model.ItemList" resolve="getAllItems" />
                      </node>
                    </node>
                    <node concept="liA8E" id="7bbHeTWugzZ" role="2OqNvi">
                      <ref role="37wK5l" to="j4ye:~BaseList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="7bbHeTWuhXX" role="37wK5m">
                        <ref role="3cqZAo" node="7bbHeTWtZgV" resolve="app" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="7bbHeTWuBmD" role="3cqZAp">
                  <node concept="37vLTI" id="7bbHeTWuJrs" role="3clFbG">
                    <node concept="Xl_RD" id="7bbHeTWuK1$" role="37vLTx">
                      <property role="Xl_RC" value="0000" />
                    </node>
                    <node concept="AH0OO" id="7bbHeTWuCif" role="37vLTJ">
                      <node concept="3cmrfG" id="7bbHeTWuCRL" role="AHEQo">
                        <property role="3cmrfH" value="5" />
                      </node>
                      <node concept="37vLTw" id="7bbHeTWuBmB" role="AHHXb">
                        <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="7bbHeTWuLuM" role="3cqZAp">
                  <node concept="37vLTI" id="7bbHeTWuTzW" role="3clFbG">
                    <node concept="37vLTw" id="7bbHeTWuUHz" role="37vLTx">
                      <ref role="3cqZAo" node="7bbHeTWkkyL" resolve="mrngTime" />
                    </node>
                    <node concept="AH0OO" id="7bbHeTWuMqu" role="37vLTJ">
                      <node concept="3cmrfG" id="7bbHeTWuN00" role="AHEQo">
                        <property role="3cmrfH" value="6" />
                      </node>
                      <node concept="37vLTw" id="7bbHeTWuLuK" role="AHHXb">
                        <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3KaCP$" id="7bbHeTWuWbN" role="3cqZAp">
                  <node concept="AH0OO" id="7bbHeTWuYfL" role="3KbGdf">
                    <node concept="3cmrfG" id="7bbHeTWuYMV" role="AHEQo">
                      <property role="3cmrfH" value="4" />
                    </node>
                    <node concept="37vLTw" id="7bbHeTWuXzr" role="AHHXb">
                      <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                    </node>
                  </node>
                  <node concept="3KbdKl" id="7bbHeTWuYQb" role="3KbHQx">
                    <node concept="Xl_RD" id="7bbHeTWuZrs" role="3Kbmr1">
                      <property role="Xl_RC" value="Monday" />
                    </node>
                    <node concept="3clFbS" id="7bbHeTWuYQd" role="3Kbo56">
                      <node concept="3clFbF" id="7bbHeTWv0AG" role="3cqZAp">
                        <node concept="37vLTI" id="7bbHeTWv8Hn" role="3clFbG">
                          <node concept="Xl_RD" id="7bbHeTWv9jv" role="37vLTx">
                            <property role="Xl_RC" value="Tuesday" />
                          </node>
                          <node concept="AH0OO" id="7bbHeTWv1j3" role="37vLTJ">
                            <node concept="3cmrfG" id="7bbHeTWv1S_" role="AHEQo">
                              <property role="3cmrfH" value="4" />
                            </node>
                            <node concept="37vLTw" id="7bbHeTWv0AF" role="AHHXb">
                              <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3zACq4" id="7bbHeTWva3x" role="3cqZAp" />
                    </node>
                  </node>
                  <node concept="3KbdKl" id="7bbHeTWvaC_" role="3KbHQx">
                    <node concept="Xl_RD" id="7bbHeTWvbey" role="3Kbmr1">
                      <property role="Xl_RC" value="Tuesday" />
                    </node>
                    <node concept="3clFbS" id="7bbHeTWvaCB" role="3Kbo56">
                      <node concept="3clFbF" id="7bbHeTWvbpg" role="3cqZAp">
                        <node concept="37vLTI" id="7bbHeTWvbph" role="3clFbG">
                          <node concept="Xl_RD" id="7bbHeTWvbpi" role="37vLTx">
                            <property role="Xl_RC" value="Wednesday" />
                          </node>
                          <node concept="AH0OO" id="7bbHeTWvbpj" role="37vLTJ">
                            <node concept="3cmrfG" id="7bbHeTWvbpk" role="AHEQo">
                              <property role="3cmrfH" value="4" />
                            </node>
                            <node concept="37vLTw" id="7bbHeTWvbpl" role="AHHXb">
                              <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3zACq4" id="7bbHeTWvbpm" role="3cqZAp" />
                    </node>
                  </node>
                  <node concept="3KbdKl" id="7bbHeTWvc8w" role="3KbHQx">
                    <node concept="Xl_RD" id="7bbHeTWvcIJ" role="3Kbmr1">
                      <property role="Xl_RC" value="Wednesday" />
                    </node>
                    <node concept="3clFbS" id="7bbHeTWvc8y" role="3Kbo56">
                      <node concept="3clFbF" id="7bbHeTWvcUi" role="3cqZAp">
                        <node concept="37vLTI" id="7bbHeTWvcUj" role="3clFbG">
                          <node concept="Xl_RD" id="7bbHeTWvcUk" role="37vLTx">
                            <property role="Xl_RC" value="Thursday" />
                          </node>
                          <node concept="AH0OO" id="7bbHeTWvcUl" role="37vLTJ">
                            <node concept="3cmrfG" id="7bbHeTWvcUm" role="AHEQo">
                              <property role="3cmrfH" value="4" />
                            </node>
                            <node concept="37vLTw" id="7bbHeTWvcUn" role="AHHXb">
                              <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3zACq4" id="7bbHeTWvcUo" role="3cqZAp" />
                    </node>
                  </node>
                  <node concept="3KbdKl" id="7bbHeTWvdaO" role="3KbHQx">
                    <node concept="Xl_RD" id="7bbHeTWvdLl" role="3Kbmr1">
                      <property role="Xl_RC" value="Thursday" />
                    </node>
                    <node concept="3clFbS" id="7bbHeTWvdaQ" role="3Kbo56">
                      <node concept="3clFbF" id="7bbHeTWvdVY" role="3cqZAp">
                        <node concept="37vLTI" id="7bbHeTWvdVZ" role="3clFbG">
                          <node concept="Xl_RD" id="7bbHeTWvdW0" role="37vLTx">
                            <property role="Xl_RC" value="Friday" />
                          </node>
                          <node concept="AH0OO" id="7bbHeTWvdW1" role="37vLTJ">
                            <node concept="3cmrfG" id="7bbHeTWvdW2" role="AHEQo">
                              <property role="3cmrfH" value="4" />
                            </node>
                            <node concept="37vLTw" id="7bbHeTWvdW3" role="AHHXb">
                              <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3zACq4" id="7bbHeTWvdW4" role="3cqZAp" />
                    </node>
                  </node>
                  <node concept="3KbdKl" id="7bbHeTWve2p" role="3KbHQx">
                    <node concept="Xl_RD" id="7bbHeTWveDc" role="3Kbmr1">
                      <property role="Xl_RC" value="Friday" />
                    </node>
                    <node concept="3clFbS" id="7bbHeTWve2r" role="3Kbo56">
                      <node concept="3clFbF" id="7bbHeTWveGM" role="3cqZAp">
                        <node concept="37vLTI" id="7bbHeTWveGN" role="3clFbG">
                          <node concept="Xl_RD" id="7bbHeTWveGO" role="37vLTx">
                            <property role="Xl_RC" value="Saturday" />
                          </node>
                          <node concept="AH0OO" id="7bbHeTWveGP" role="37vLTJ">
                            <node concept="3cmrfG" id="7bbHeTWveGQ" role="AHEQo">
                              <property role="3cmrfH" value="4" />
                            </node>
                            <node concept="37vLTw" id="7bbHeTWveGR" role="AHHXb">
                              <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3zACq4" id="7bbHeTWveGS" role="3cqZAp" />
                    </node>
                  </node>
                  <node concept="3KbdKl" id="7bbHeTWveRV" role="3KbHQx">
                    <node concept="Xl_RD" id="7bbHeTWvfsh" role="3Kbmr1">
                      <property role="Xl_RC" value="Saturday" />
                    </node>
                    <node concept="3clFbS" id="7bbHeTWveRX" role="3Kbo56">
                      <node concept="3clFbF" id="7bbHeTWvfyK" role="3cqZAp">
                        <node concept="37vLTI" id="7bbHeTWvfyL" role="3clFbG">
                          <node concept="Xl_RD" id="7bbHeTWvfyM" role="37vLTx">
                            <property role="Xl_RC" value="Sunday" />
                          </node>
                          <node concept="AH0OO" id="7bbHeTWvfyN" role="37vLTJ">
                            <node concept="3cmrfG" id="7bbHeTWvfyO" role="AHEQo">
                              <property role="3cmrfH" value="4" />
                            </node>
                            <node concept="37vLTw" id="7bbHeTWvfyP" role="AHHXb">
                              <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3zACq4" id="7bbHeTWvfyQ" role="3cqZAp" />
                    </node>
                  </node>
                  <node concept="3KbdKl" id="7bbHeTWvgfZ" role="3KbHQx">
                    <node concept="Xl_RD" id="7bbHeTWvgRm" role="3Kbmr1">
                      <property role="Xl_RC" value="Sunday" />
                    </node>
                    <node concept="3clFbS" id="7bbHeTWvgg1" role="3Kbo56">
                      <node concept="3clFbF" id="7bbHeTWvgWI" role="3cqZAp">
                        <node concept="37vLTI" id="7bbHeTWvgWJ" role="3clFbG">
                          <node concept="Xl_RD" id="7bbHeTWvgWK" role="37vLTx">
                            <property role="Xl_RC" value="Monday" />
                          </node>
                          <node concept="AH0OO" id="7bbHeTWvgWL" role="37vLTJ">
                            <node concept="3cmrfG" id="7bbHeTWvgWM" role="AHEQo">
                              <property role="3cmrfH" value="4" />
                            </node>
                            <node concept="37vLTw" id="7bbHeTWvgWN" role="AHHXb">
                              <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3zACq4" id="7bbHeTWvgWO" role="3cqZAp" />
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="7bbHeTWviUk" role="3cqZAp">
                  <node concept="3cpWsn" id="7bbHeTWviUl" role="3cpWs9">
                    <property role="TrG5h" value="app1" />
                    <node concept="3uibUv" id="7bbHeTWviUm" role="1tU5fm">
                      <ref role="3uigEE" to="spdm:~Appointment" resolve="Appointment" />
                    </node>
                    <node concept="1rXfSq" id="7bbHeTWvkqC" role="33vP2m">
                      <ref role="37wK5l" node="7bbHeTWkPTp" resolve="appointmentMake" />
                      <node concept="37vLTw" id="7bbHeTWvmir" role="37wK5m">
                        <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="7bbHeTWvnJl" role="3cqZAp">
                  <node concept="2OqwBi" id="7bbHeTWvwtg" role="3clFbG">
                    <node concept="2OqwBi" id="7bbHeTWvsIS" role="2Oq$k0">
                      <node concept="2OqwBi" id="7bbHeTWvpsp" role="2Oq$k0">
                        <node concept="37vLTw" id="5t_d$dJxhxa" role="2Oq$k0">
                          <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
                        </node>
                        <node concept="liA8E" id="7bbHeTWvsmk" role="2OqNvi">
                          <ref role="37wK5l" to="1gjj:~Calendar.getSchedule():com.mindfusion.scheduling.model.Schedule" resolve="getSchedule" />
                        </node>
                      </node>
                      <node concept="liA8E" id="7bbHeTWvve8" role="2OqNvi">
                        <ref role="37wK5l" to="spdm:~Schedule.getItems():com.mindfusion.scheduling.model.ItemList" resolve="getItems" />
                      </node>
                    </node>
                    <node concept="liA8E" id="7bbHeTWv$vA" role="2OqNvi">
                      <ref role="37wK5l" to="j4ye:~BaseList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="7bbHeTWvAyh" role="37wK5m">
                        <ref role="3cqZAo" node="7bbHeTWviUl" resolve="app1" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOSWO" id="7bbHeTWkgfH" role="3clFbw">
                <node concept="37vLTw" id="7bbHeTWkgNB" role="3uHU7w">
                  <ref role="3cqZAo" node="7bbHeTWjF_S" resolve="num2" />
                </node>
                <node concept="37vLTw" id="7bbHeTWk843" role="3uHU7B">
                  <ref role="3cqZAo" node="7bbHeTWjhYY" resolve="num1" />
                </node>
              </node>
              <node concept="9aQIb" id="7bbHeTWvIQd" role="9aQIa">
                <node concept="3clFbS" id="7bbHeTWvIQe" role="9aQI4">
                  <node concept="3cpWs8" id="7bbHeTWvKBp" role="3cqZAp">
                    <node concept="3cpWsn" id="7bbHeTWvKBq" role="3cpWs9">
                      <property role="TrG5h" value="app" />
                      <node concept="3uibUv" id="7bbHeTWvKBr" role="1tU5fm">
                        <ref role="3uigEE" to="spdm:~Appointment" resolve="Appointment" />
                      </node>
                      <node concept="1rXfSq" id="7bbHeTWvLSx" role="33vP2m">
                        <ref role="37wK5l" node="7bbHeTWkPTp" resolve="appointmentMake" />
                        <node concept="37vLTw" id="7bbHeTWvNK$" role="37wK5m">
                          <ref role="3cqZAo" node="7bbHeTWjnJr" resolve="splitStr" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="7bbHeTWvOwI" role="3cqZAp">
                    <node concept="2OqwBi" id="7bbHeTWvOwJ" role="3clFbG">
                      <node concept="2OqwBi" id="7bbHeTWvOwK" role="2Oq$k0">
                        <node concept="2OqwBi" id="7bbHeTWvOwL" role="2Oq$k0">
                          <node concept="37vLTw" id="5t_d$dJxhxf" role="2Oq$k0">
                            <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
                          </node>
                          <node concept="liA8E" id="7bbHeTWvOwN" role="2OqNvi">
                            <ref role="37wK5l" to="1gjj:~Calendar.getSchedule():com.mindfusion.scheduling.model.Schedule" resolve="getSchedule" />
                          </node>
                        </node>
                        <node concept="liA8E" id="7bbHeTWvOwO" role="2OqNvi">
                          <ref role="37wK5l" to="spdm:~Schedule.getItems():com.mindfusion.scheduling.model.ItemList" resolve="getItems" />
                        </node>
                      </node>
                      <node concept="liA8E" id="7bbHeTWvOwP" role="2OqNvi">
                        <ref role="37wK5l" to="j4ye:~BaseList.add(java.lang.Object):boolean" resolve="add" />
                        <node concept="37vLTw" id="7bbHeTWvQWa" role="37wK5m">
                          <ref role="3cqZAo" node="7bbHeTWvKBq" resolve="app" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbH" id="7bbHeTWvNO2" role="3cqZAp" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="7bbHeTWif6B" role="1Duv9x">
            <property role="TrG5h" value="j" />
            <node concept="10Oyi0" id="7bbHeTWigs3" role="1tU5fm" />
            <node concept="3cmrfG" id="7bbHeTWihcC" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3eOVzh" id="7bbHeTWikfQ" role="1Dwp0S">
            <node concept="2OqwBi" id="5t_d$dJCrNc" role="3uHU7w">
              <node concept="2OqwBi" id="7bbHeTWioaq" role="2Oq$k0">
                <node concept="Xjq3P" id="7bbHeTWimuB" role="2Oq$k0" />
                <node concept="2OwXpG" id="7bbHeTWipJn" role="2OqNvi">
                  <ref role="2Oxat5" node="2lFfnZ_klFn" resolve="scheduleArray" />
                </node>
              </node>
              <node concept="1Rwk04" id="5t_d$dJCt6c" role="2OqNvi" />
            </node>
            <node concept="37vLTw" id="7bbHeTWihKm" role="3uHU7B">
              <ref role="3cqZAo" node="7bbHeTWif6B" resolve="j" />
            </node>
          </node>
          <node concept="3uNrnE" id="7bbHeTWiEF$" role="1Dwrff">
            <node concept="37vLTw" id="7bbHeTWiEFA" role="2$L3a6">
              <ref role="3cqZAo" node="7bbHeTWif6B" resolve="j" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2OB0Aer81T2" role="3cqZAp" />
        <node concept="3clFbF" id="7bbHeTWtl61" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWtmUh" role="3clFbG">
            <node concept="37vLTw" id="5t_d$dJw$jg" role="2Oq$k0">
              <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
            </node>
            <node concept="liA8E" id="7bbHeTWtpML" role="2OqNvi">
              <ref role="37wK5l" to="1gjj:~Calendar.endInit():void" resolve="endInit" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWvYIy" role="3cqZAp">
          <node concept="1rXfSq" id="7bbHeTWvYIw" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~JFrame.setDefaultCloseOperation(int):void" resolve="setDefaultCloseOperation" />
            <node concept="37vLTw" id="5t_d$dJw$jl" role="37wK5m">
              <ref role="3cqZAo" to="dxuu:~JFrame.EXIT_ON_CLOSE" resolve="EXIT_ON_CLOSE" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5t_d$dJswu8" role="3cqZAp">
          <node concept="1rXfSq" id="5t_d$dJswu6" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Frame.setExtendedState(int):void" resolve="setExtendedState" />
            <node concept="37vLTw" id="5t_d$dJw$jq" role="37wK5m">
              <ref role="3cqZAo" to="z60i:~Frame.MAXIMIZED_BOTH" resolve="MAXIMIZED_BOTH" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWtyi5" role="3cqZAp">
          <node concept="1rXfSq" id="7bbHeTWtyi3" role="3clFbG">
            <ref role="37wK5l" to="z60i:~Frame.setTitle(java.lang.String):void" resolve="setTitle" />
            <node concept="Xl_RD" id="5t_d$dJ_sBP" role="37wK5m">
              <property role="Xl_RC" value="Student Planner" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5t_d$dJwzl0" role="3cqZAp" />
      </node>
      <node concept="3Tm6S6" id="3rIOsIru2f6" role="1B3o_S" />
      <node concept="3cqZAl" id="3rIOsIru8W8" role="3clF45" />
      <node concept="3uibUv" id="7bbHeTWusiF" role="Sfmx6">
        <ref role="3uigEE" to="25x5:~ParseException" resolve="ParseException" />
      </node>
    </node>
    <node concept="2tJIrI" id="2OB0Aer9mbe" role="jymVt" />
    <node concept="3clFb_" id="7bbHeTWkPTp" role="jymVt">
      <property role="TrG5h" value="appointmentMake" />
      <node concept="3clFbS" id="7bbHeTWkPTs" role="3clF47">
        <node concept="3clFbF" id="7bbHeTWld9M" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWld9J" role="3clFbG">
            <node concept="10M0yZ" id="7bbHeTWld9K" role="2Oq$k0">
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
            </node>
            <node concept="liA8E" id="7bbHeTWld9L" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="AH0OO" id="7bbHeTWllYz" role="37wK5m">
                <node concept="3cmrfG" id="7bbHeTWlnaM" role="AHEQo">
                  <property role="3cmrfH" value="5" />
                </node>
                <node concept="37vLTw" id="7bbHeTWleOb" role="AHHXb">
                  <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWlo7u" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWlo7r" role="3clFbG">
            <node concept="10M0yZ" id="7bbHeTWlo7s" role="2Oq$k0">
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
            </node>
            <node concept="liA8E" id="7bbHeTWlo7t" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="AH0OO" id="7bbHeTWlsGA" role="37wK5m">
                <node concept="3cmrfG" id="7bbHeTWltKK" role="AHEQo">
                  <property role="3cmrfH" value="6" />
                </node>
                <node concept="37vLTw" id="7bbHeTWlp$j" role="AHHXb">
                  <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWlvvy" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWlvvz" role="3cpWs9">
            <property role="TrG5h" value="app" />
            <node concept="3uibUv" id="2OB0Aer4a0m" role="1tU5fm">
              <ref role="3uigEE" to="spdm:~Appointment" resolve="Appointment" />
            </node>
            <node concept="2ShNRf" id="7bbHeTWlwbG" role="33vP2m">
              <node concept="HV5vD" id="2OB0Aer4iFO" role="2ShVmc">
                <ref role="HV5vE" to="spdm:~Appointment" resolve="Appointment" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWlyMP" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWlyZ6" role="3clFbG">
            <node concept="37vLTw" id="7bbHeTWlyMN" role="2Oq$k0">
              <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
            </node>
            <node concept="liA8E" id="7bbHeTWlzR2" role="2OqNvi">
              <ref role="37wK5l" to="spdm:~Appointment.setHeaderText(java.lang.String):void" resolve="setHeaderText" />
              <node concept="AH0OO" id="7bbHeTWlAsW" role="37wK5m">
                <node concept="3cmrfG" id="7bbHeTWlB$d" role="AHEQo">
                  <property role="3cmrfH" value="1" />
                </node>
                <node concept="37vLTw" id="7bbHeTWl_0B" role="AHHXb">
                  <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="7bbHeTWlCrO" role="3cqZAp">
          <node concept="3clFbS" id="7bbHeTWlCrQ" role="3clFbx">
            <node concept="3cpWs8" id="7bbHeTWlKS_" role="3cqZAp">
              <node concept="3cpWsn" id="7bbHeTWlKSC" role="3cpWs9">
                <property role="TrG5h" value="desc" />
                <node concept="17QB3L" id="7bbHeTWlKSz" role="1tU5fm" />
                <node concept="3cpWs3" id="7bbHeTWlSGT" role="33vP2m">
                  <node concept="AH0OO" id="7bbHeTWlVQj" role="3uHU7w">
                    <node concept="3cmrfG" id="7bbHeTWlWA6" role="AHEQo">
                      <property role="3cmrfH" value="3" />
                    </node>
                    <node concept="37vLTw" id="7bbHeTWlUvJ" role="AHHXb">
                      <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                    </node>
                  </node>
                  <node concept="3cpWs3" id="7bbHeTWlQC3" role="3uHU7B">
                    <node concept="3cpWs3" id="7bbHeTWlM7b" role="3uHU7B">
                      <node concept="Xl_RD" id="7bbHeTWlLuD" role="3uHU7B">
                        <property role="Xl_RC" value="Difficulty:" />
                      </node>
                      <node concept="AH0OO" id="7bbHeTWlOzW" role="3uHU7w">
                        <node concept="3cmrfG" id="7bbHeTWlP73" role="AHEQo">
                          <property role="3cmrfH" value="2" />
                        </node>
                        <node concept="37vLTw" id="7bbHeTWlNe0" role="AHHXb">
                          <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                        </node>
                      </node>
                    </node>
                    <node concept="Xl_RD" id="7bbHeTWlRRH" role="3uHU7w">
                      <property role="Xl_RC" value="\n" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWml5D" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWmlNQ" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWml5B" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
                </node>
                <node concept="liA8E" id="7bbHeTWmmzN" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Appointment.setDescriptionText(java.lang.String):void" resolve="setDescriptionText" />
                  <node concept="37vLTw" id="7bbHeTWmn9J" role="37wK5m">
                    <ref role="3cqZAo" node="7bbHeTWlKSC" resolve="desc" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3fqX7Q" id="7bbHeTWlD7q" role="3clFbw">
            <node concept="2OqwBi" id="7bbHeTWlHjJ" role="3fr31v">
              <node concept="AH0OO" id="7bbHeTWlFHW" role="2Oq$k0">
                <node concept="3cmrfG" id="7bbHeTWlGQW" role="AHEQo">
                  <property role="3cmrfH" value="2" />
                </node>
                <node concept="37vLTw" id="7bbHeTWlElA" role="AHHXb">
                  <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                </node>
              </node>
              <node concept="liA8E" id="7bbHeTWlJxo" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                <node concept="Xl_RD" id="7bbHeTWlK7$" role="37wK5m">
                  <property role="Xl_RC" value="NA" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWnsPU" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWnsPV" role="3cpWs9">
            <property role="TrG5h" value="dateFormat" />
            <node concept="3uibUv" id="7bbHeTWnsPW" role="1tU5fm">
              <ref role="3uigEE" to="25x5:~DateFormat" resolve="DateFormat" />
            </node>
            <node concept="2ShNRf" id="7bbHeTWnAlS" role="33vP2m">
              <node concept="1pGfFk" id="7bbHeTWnCjh" role="2ShVmc">
                <ref role="37wK5l" to="25x5:~SimpleDateFormat.&lt;init&gt;(java.lang.String)" resolve="SimpleDateFormat" />
                <node concept="Xl_RD" id="7bbHeTWnCRI" role="37wK5m">
                  <property role="Xl_RC" value="hhmm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWnVR5" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWnVR6" role="3cpWs9">
            <property role="TrG5h" value="startTim" />
            <node concept="3uibUv" id="7bbHeTWnVR7" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Date" resolve="Date" />
            </node>
            <node concept="2OqwBi" id="7bbHeTWo$Ax" role="33vP2m">
              <node concept="37vLTw" id="7bbHeTWo6gS" role="2Oq$k0">
                <ref role="3cqZAo" node="7bbHeTWnsPV" resolve="dateFormat" />
              </node>
              <node concept="liA8E" id="7bbHeTWo$Pa" role="2OqNvi">
                <ref role="37wK5l" to="25x5:~DateFormat.parse(java.lang.String):java.util.Date" resolve="parse" />
                <node concept="AH0OO" id="7bbHeTWoBnM" role="37wK5m">
                  <node concept="3cmrfG" id="7bbHeTWoCq0" role="AHEQo">
                    <property role="3cmrfH" value="5" />
                  </node>
                  <node concept="37vLTw" id="7bbHeTWo_WE" role="AHHXb">
                    <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWom3U" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWom3V" role="3cpWs9">
            <property role="TrG5h" value="endTim" />
            <node concept="3uibUv" id="7bbHeTWom3W" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Date" resolve="Date" />
            </node>
            <node concept="2OqwBi" id="7bbHeTWom3X" role="33vP2m">
              <node concept="37vLTw" id="7bbHeTWom3Y" role="2Oq$k0">
                <ref role="3cqZAo" node="7bbHeTWnsPV" resolve="dateFormat" />
              </node>
              <node concept="liA8E" id="7bbHeTWom3Z" role="2OqNvi">
                <ref role="37wK5l" to="25x5:~DateFormat.parse(java.lang.String):java.util.Date" resolve="parse" />
                <node concept="AH0OO" id="7bbHeTWoxUQ" role="37wK5m">
                  <node concept="3cmrfG" id="7bbHeTWoyDU" role="AHEQo">
                    <property role="3cmrfH" value="6" />
                  </node>
                  <node concept="37vLTw" id="7bbHeTWom42" role="AHHXb">
                    <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWpc$d" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWpc$e" role="3cpWs9">
            <property role="TrG5h" value="sdt" />
            <node concept="3uibUv" id="7bbHeTWpc$f" role="1tU5fm">
              <ref role="3uigEE" to="j4ye:~DateTime" resolve="DateTime" />
            </node>
            <node concept="2ShNRf" id="7bbHeTWpmDx" role="33vP2m">
              <node concept="1pGfFk" id="7bbHeTWpoCg" role="2ShVmc">
                <ref role="37wK5l" to="j4ye:~DateTime.&lt;init&gt;(java.util.Date)" resolve="DateTime" />
                <node concept="37vLTw" id="7bbHeTWppKD" role="37wK5m">
                  <ref role="3cqZAo" node="7bbHeTWnVR6" resolve="startTim" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWpxeS" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWpxeT" role="3cpWs9">
            <property role="TrG5h" value="edt" />
            <node concept="3uibUv" id="7bbHeTWpxeU" role="1tU5fm">
              <ref role="3uigEE" to="j4ye:~DateTime" resolve="DateTime" />
            </node>
            <node concept="2ShNRf" id="7bbHeTWpxeV" role="33vP2m">
              <node concept="1pGfFk" id="7bbHeTWpxeW" role="2ShVmc">
                <ref role="37wK5l" to="j4ye:~DateTime.&lt;init&gt;(java.util.Date)" resolve="DateTime" />
                <node concept="37vLTw" id="7bbHeTWpFla" role="37wK5m">
                  <ref role="3cqZAo" node="7bbHeTWom3V" resolve="endTim" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2OB0Aer9K3V" role="3cqZAp" />
        <node concept="3clFbJ" id="2OB0Aeraej9" role="3cqZAp">
          <node concept="3clFbS" id="2OB0Aeraejb" role="3clFbx">
            <node concept="3clFbF" id="2OB0AerazJm" role="3cqZAp">
              <node concept="2OqwBi" id="2OB0AerazJj" role="3clFbG">
                <node concept="10M0yZ" id="2OB0AerazJk" role="2Oq$k0">
                  <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                  <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                </node>
                <node concept="liA8E" id="2OB0AerazJl" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                  <node concept="3cpWs3" id="2OB0AeraGJj" role="37wK5m">
                    <node concept="AH0OO" id="2OB0AeraJl5" role="3uHU7w">
                      <node concept="3cmrfG" id="2OB0AeraJSf" role="AHEQo">
                        <property role="3cmrfH" value="5" />
                      </node>
                      <node concept="37vLTw" id="2OB0AeraHTW" role="AHHXb">
                        <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="2OB0AeraFo0" role="3uHU7B">
                      <property role="Xl_RC" value="splitStr[5" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="2OB0AeraRDm" role="3cqZAp">
              <node concept="3cpWsn" id="2OB0AeraRDn" role="3cpWs9">
                <property role="TrG5h" value="spt5" />
                <node concept="3uibUv" id="2OB0AeraRDo" role="1tU5fm">
                  <ref role="3uigEE" to="wyt6:~StringBuilder" resolve="StringBuilder" />
                </node>
                <node concept="2ShNRf" id="2OB0AeraUk8" role="33vP2m">
                  <node concept="1pGfFk" id="2OB0AeraWrb" role="2ShVmc">
                    <ref role="37wK5l" to="wyt6:~StringBuilder.&lt;init&gt;(java.lang.String)" resolve="StringBuilder" />
                    <node concept="AH0OO" id="2OB0Aerb05f" role="37wK5m">
                      <node concept="3cmrfG" id="2OB0Aerb0Cp" role="AHEQo">
                        <property role="3cmrfH" value="5" />
                      </node>
                      <node concept="37vLTw" id="2OB0AeraXzr" role="AHHXb">
                        <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0Aerb2Rd" role="3cqZAp">
              <node concept="2OqwBi" id="2OB0Aerb3Ua" role="3clFbG">
                <node concept="37vLTw" id="2OB0Aerb2Rb" role="2Oq$k0">
                  <ref role="3cqZAo" node="2OB0AeraRDn" resolve="spt5" />
                </node>
                <node concept="liA8E" id="2OB0Aerb5Yt" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~AbstractStringBuilder.setCharAt(int,char):void" resolve="setCharAt" />
                  <node concept="3cmrfG" id="2OB0Aerb6$k" role="37wK5m">
                    <property role="3cmrfH" value="1" />
                  </node>
                  <node concept="1Xhbcc" id="2OB0Aerb9FJ" role="37wK5m">
                    <property role="1XhdNS" value="1" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0AerbakJ" role="3cqZAp">
              <node concept="2OqwBi" id="2OB0AerbakG" role="3clFbG">
                <node concept="10M0yZ" id="2OB0AerbakH" role="2Oq$k0">
                  <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                  <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                </node>
                <node concept="liA8E" id="2OB0AerbakI" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                  <node concept="3cpWs3" id="2OB0Aerbd64" role="37wK5m">
                    <node concept="2OqwBi" id="2OB0AerbfjI" role="3uHU7w">
                      <node concept="37vLTw" id="2OB0AerbegH" role="2Oq$k0">
                        <ref role="3cqZAo" node="2OB0AeraRDn" resolve="spt5" />
                      </node>
                      <node concept="liA8E" id="2OB0AerbhpG" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="2OB0Aerbbqc" role="3uHU7B">
                      <property role="Xl_RC" value="splitStr[5] changes: " />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0Aerbj83" role="3cqZAp">
              <node concept="37vLTI" id="2OB0AerbliZ" role="3clFbG">
                <node concept="2OqwBi" id="2OB0AerbnhO" role="37vLTx">
                  <node concept="37vLTw" id="2OB0AerbmsN" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWnsPV" resolve="dateFormat" />
                  </node>
                  <node concept="liA8E" id="2OB0AerbpGn" role="2OqNvi">
                    <ref role="37wK5l" to="25x5:~DateFormat.parse(java.lang.String):java.util.Date" resolve="parse" />
                    <node concept="2OqwBi" id="2OB0AerbrPP" role="37wK5m">
                      <node concept="37vLTw" id="2OB0AerbqPt" role="2Oq$k0">
                        <ref role="3cqZAo" node="2OB0AeraRDn" resolve="spt5" />
                      </node>
                      <node concept="liA8E" id="2OB0Aerbu3n" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="2OB0Aerbj81" role="37vLTJ">
                  <ref role="3cqZAo" node="7bbHeTWnVR6" resolve="startTim" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0Aerbvgs" role="3cqZAp">
              <node concept="37vLTI" id="2OB0Aerbxu8" role="3clFbG">
                <node concept="2ShNRf" id="2OB0AerbyBf" role="37vLTx">
                  <node concept="1pGfFk" id="2OB0Aerby4b" role="2ShVmc">
                    <ref role="37wK5l" to="j4ye:~DateTime.&lt;init&gt;(java.util.Date)" resolve="DateTime" />
                    <node concept="37vLTw" id="2OB0AerbzKm" role="37wK5m">
                      <ref role="3cqZAo" node="7bbHeTWnVR6" resolve="startTim" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="2OB0Aerbvgq" role="37vLTJ">
                  <ref role="3cqZAo" node="7bbHeTWpc$e" resolve="sdt" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0Aerb$LJ" role="3cqZAp">
              <node concept="2OqwBi" id="2OB0Aerb$LG" role="3clFbG">
                <node concept="10M0yZ" id="2OB0Aerb$LH" role="2Oq$k0">
                  <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                  <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                </node>
                <node concept="liA8E" id="2OB0Aerb$LI" role="2OqNvi">
                  <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                  <node concept="3cpWs3" id="2OB0AerbCfm" role="37wK5m">
                    <node concept="2OqwBi" id="2OB0AerbDF5" role="3uHU7w">
                      <node concept="37vLTw" id="2OB0AerbCQx" role="2Oq$k0">
                        <ref role="3cqZAo" node="7bbHeTWpc$e" resolve="sdt" />
                      </node>
                      <node concept="liA8E" id="2OB0AerbIev" role="2OqNvi">
                        <ref role="37wK5l" to="j4ye:~DateTime.toString():java.lang.String" resolve="toString" />
                      </node>
                    </node>
                    <node concept="Xl_RD" id="2OB0AerbA9R" role="3uHU7B">
                      <property role="Xl_RC" value="sdt changes1:" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0AerbK36" role="3cqZAp">
              <node concept="37vLTI" id="2OB0AerbNBf" role="3clFbG">
                <node concept="2OqwBi" id="2OB0AerbPet" role="37vLTx">
                  <node concept="37vLTw" id="2OB0AerbOlk" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWpc$e" resolve="sdt" />
                  </node>
                  <node concept="liA8E" id="2OB0AerbYt$" role="2OqNvi">
                    <ref role="37wK5l" to="j4ye:~DateTime.addHours(double):com.mindfusion.common.DateTime" resolve="addHours" />
                    <node concept="3cmrfG" id="2OB0AerbZ4m" role="37wK5m">
                      <property role="3cmrfH" value="1" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="2OB0AerbK34" role="37vLTJ">
                  <ref role="3cqZAo" node="7bbHeTWpc$e" resolve="sdt" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2OB0AeravRm" role="3clFbw">
            <node concept="AH0OO" id="5t_d$dJwcrI" role="2Oq$k0">
              <node concept="3cmrfG" id="5t_d$dJwd8L" role="AHEQo">
                <property role="3cmrfH" value="5" />
              </node>
              <node concept="37vLTw" id="2OB0AerarSD" role="AHHXb">
                <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
              </node>
            </node>
            <node concept="liA8E" id="2OB0AeraxHB" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.startsWith(java.lang.String):boolean" resolve="startsWith" />
              <node concept="Xl_RD" id="2OB0Aeraz74" role="37wK5m">
                <property role="Xl_RC" value="12" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2OB0Aerc0o6" role="3cqZAp" />
        <node concept="3clFbJ" id="2OB0AerclOF" role="3cqZAp">
          <node concept="3clFbS" id="2OB0AerclOH" role="3clFbx">
            <node concept="3cpWs8" id="2OB0AercHHd" role="3cqZAp">
              <node concept="3cpWsn" id="2OB0AercHHe" role="3cpWs9">
                <property role="TrG5h" value="spt6" />
                <node concept="3uibUv" id="2OB0AercHHf" role="1tU5fm">
                  <ref role="3uigEE" to="wyt6:~StringBuilder" resolve="StringBuilder" />
                </node>
                <node concept="2ShNRf" id="2OB0AercIxu" role="33vP2m">
                  <node concept="1pGfFk" id="2OB0AercKxk" role="2ShVmc">
                    <ref role="37wK5l" to="wyt6:~StringBuilder.&lt;init&gt;(java.lang.String)" resolve="StringBuilder" />
                    <node concept="AH0OO" id="2OB0AercOcd" role="37wK5m">
                      <node concept="3cmrfG" id="2OB0AercOJn" role="AHEQo">
                        <property role="3cmrfH" value="6" />
                      </node>
                      <node concept="37vLTw" id="2OB0AercLG7" role="AHHXb">
                        <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0AercQM9" role="3cqZAp">
              <node concept="2OqwBi" id="2OB0AercT7L" role="3clFbG">
                <node concept="37vLTw" id="2OB0AercSiF" role="2Oq$k0">
                  <ref role="3cqZAo" node="2OB0AercHHe" resolve="spt6" />
                </node>
                <node concept="liA8E" id="2OB0AercVc4" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~AbstractStringBuilder.setCharAt(int,char):void" resolve="setCharAt" />
                  <node concept="3cmrfG" id="2OB0AercVM_" role="37wK5m">
                    <property role="3cmrfH" value="1" />
                  </node>
                  <node concept="1Xhbcc" id="2OB0AercWu1" role="37wK5m">
                    <property role="1XhdNS" value="1" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0AercX6V" role="3cqZAp">
              <node concept="37vLTI" id="2OB0Aerd0$w" role="3clFbG">
                <node concept="2OqwBi" id="2OB0Aerd2zo" role="37vLTx">
                  <node concept="37vLTw" id="2OB0Aerd1Ik" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWnsPV" resolve="dateFormat" />
                  </node>
                  <node concept="liA8E" id="2OB0Aerd4XY" role="2OqNvi">
                    <ref role="37wK5l" to="25x5:~DateFormat.parse(java.lang.String):java.util.Date" resolve="parse" />
                    <node concept="2OqwBi" id="2OB0Aerd6qz" role="37wK5m">
                      <node concept="37vLTw" id="2OB0Aerd5$a" role="2Oq$k0">
                        <ref role="3cqZAo" node="2OB0AercHHe" resolve="spt6" />
                      </node>
                      <node concept="liA8E" id="2OB0Aerd8eX" role="2OqNvi">
                        <ref role="37wK5l" to="wyt6:~StringBuilder.toString():java.lang.String" resolve="toString" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="2OB0AercYy1" role="37vLTJ">
                  <ref role="3cqZAo" node="7bbHeTWom3V" resolve="endTim" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0Aerd94Y" role="3cqZAp">
              <node concept="37vLTI" id="2OB0Aerdbaf" role="3clFbG">
                <node concept="2ShNRf" id="2OB0Aerdcjm" role="37vLTx">
                  <node concept="1pGfFk" id="2OB0AerdbKi" role="2ShVmc">
                    <ref role="37wK5l" to="j4ye:~DateTime.&lt;init&gt;(java.util.Date)" resolve="DateTime" />
                    <node concept="37vLTw" id="2OB0AerddwV" role="37wK5m">
                      <ref role="3cqZAo" node="7bbHeTWom3V" resolve="endTim" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="2OB0Aerd94W" role="37vLTJ">
                  <ref role="3cqZAo" node="7bbHeTWpxeT" resolve="edt" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0AerdebB" role="3cqZAp">
              <node concept="37vLTI" id="2OB0Aerdg5z" role="3clFbG">
                <node concept="2OqwBi" id="2OB0Aerdhv0" role="37vLTx">
                  <node concept="37vLTw" id="2OB0AerdgFa" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWpxeT" resolve="edt" />
                  </node>
                  <node concept="liA8E" id="2OB0Aerdr85" role="2OqNvi">
                    <ref role="37wK5l" to="j4ye:~DateTime.addHours(double):com.mindfusion.common.DateTime" resolve="addHours" />
                    <node concept="3cmrfG" id="2OB0AerdrJ9" role="37wK5m">
                      <property role="3cmrfH" value="1" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="2OB0Aerdeb_" role="37vLTJ">
                  <ref role="3cqZAo" node="7bbHeTWpxeT" resolve="edt" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2OB0AercCFm" role="3clFbw">
            <node concept="AH0OO" id="2OB0AercA5K" role="2Oq$k0">
              <node concept="3cmrfG" id="2OB0AercBaQ" role="AHEQo">
                <property role="3cmrfH" value="6" />
              </node>
              <node concept="37vLTw" id="2OB0AerczDN" role="AHHXb">
                <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
              </node>
            </node>
            <node concept="liA8E" id="2OB0AercECy" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.startsWith(java.lang.String):boolean" resolve="startsWith" />
              <node concept="Xl_RD" id="2OB0AercGkQ" role="37wK5m">
                <property role="Xl_RC" value="12" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2OB0Aer9RdK" role="3cqZAp" />
        <node concept="3cpWs8" id="7bbHeTWpLVi" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWpLVj" role="3cpWs9">
            <property role="TrG5h" value="rec" />
            <node concept="3uibUv" id="7bbHeTWpLVk" role="1tU5fm">
              <ref role="3uigEE" to="spdm:~Recurrence" resolve="Recurrence" />
            </node>
            <node concept="2ShNRf" id="7bbHeTWpW1$" role="33vP2m">
              <node concept="1pGfFk" id="7bbHeTWpY0j" role="2ShVmc">
                <ref role="37wK5l" to="spdm:~Recurrence.&lt;init&gt;()" resolve="Recurrence" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWq4bQ" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWq9JV" role="3clFbG">
            <node concept="37vLTw" id="7bbHeTWq4bO" role="2Oq$k0">
              <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
            </node>
            <node concept="liA8E" id="7bbHeTWqeoF" role="2OqNvi">
              <ref role="37wK5l" to="spdm:~Item.setRecurrence(com.mindfusion.scheduling.model.Recurrence):void" resolve="setRecurrence" />
              <node concept="37vLTw" id="7bbHeTWqf0F" role="37wK5m">
                <ref role="3cqZAo" node="7bbHeTWpLVj" resolve="rec" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7bbHeTWqznH" role="3cqZAp">
          <node concept="3cpWsn" id="7bbHeTWqznI" role="3cpWs9">
            <property role="TrG5h" value="setEn" />
            <node concept="3uibUv" id="7bbHeTWqznF" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~EnumSet" resolve="EnumSet" />
              <node concept="3uibUv" id="7bbHeTWqH2e" role="11_B2D">
                <ref role="3uigEE" to="j4ye:~DayOfWeek" resolve="DayOfWeek" />
              </node>
            </node>
            <node concept="10Nm6u" id="7bbHeTWqHNh" role="33vP2m" />
          </node>
        </node>
        <node concept="3KaCP$" id="7bbHeTWqS2U" role="3cqZAp">
          <node concept="AH0OO" id="7bbHeTWr3Bm" role="3KbGdf">
            <node concept="3cmrfG" id="7bbHeTWr4GQ" role="AHEQo">
              <property role="3cmrfH" value="4" />
            </node>
            <node concept="37vLTw" id="7bbHeTWr2dL" role="AHHXb">
              <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
            </node>
          </node>
          <node concept="3KbdKl" id="7bbHeTWr50d" role="3KbHQx">
            <node concept="Xl_RD" id="7bbHeTWr5JQ" role="3Kbmr1">
              <property role="Xl_RC" value="Monday" />
            </node>
            <node concept="3clFbS" id="7bbHeTWr50f" role="3Kbo56">
              <node concept="3clFbF" id="7bbHeTWr6Va" role="3cqZAp">
                <node concept="37vLTI" id="7bbHeTWr8yZ" role="3clFbG">
                  <node concept="2YIFZM" id="7bbHeTWrc32" role="37vLTx">
                    <ref role="1Pybhc" to="33ny:~EnumSet" resolve="EnumSet" />
                    <ref role="37wK5l" to="33ny:~EnumSet.of(java.lang.Enum):java.util.EnumSet" resolve="of" />
                    <node concept="Rm8GO" id="7bbHeTWreT9" role="37wK5m">
                      <ref role="1Px2BO" to="j4ye:~DayOfWeek" resolve="DayOfWeek" />
                      <ref role="Rm8GQ" to="j4ye:~DayOfWeek.Monday" resolve="Monday" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="7bbHeTWr6V9" role="37vLTJ">
                    <ref role="3cqZAo" node="7bbHeTWqznI" resolve="setEn" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="7bbHeTWrgiX" role="3cqZAp" />
            </node>
          </node>
          <node concept="3KbdKl" id="7bbHeTWrgCw" role="3KbHQx">
            <node concept="Xl_RD" id="7bbHeTWrhGl" role="3Kbmr1">
              <property role="Xl_RC" value="Tuesday" />
            </node>
            <node concept="3clFbS" id="7bbHeTWrgCy" role="3Kbo56">
              <node concept="3clFbF" id="7bbHeTWrhM4" role="3cqZAp">
                <node concept="37vLTI" id="7bbHeTWrhM5" role="3clFbG">
                  <node concept="2YIFZM" id="7bbHeTWrhM6" role="37vLTx">
                    <ref role="1Pybhc" to="33ny:~EnumSet" resolve="EnumSet" />
                    <ref role="37wK5l" to="33ny:~EnumSet.of(java.lang.Enum):java.util.EnumSet" resolve="of" />
                    <node concept="Rm8GO" id="7bbHeTWriXx" role="37wK5m">
                      <ref role="1Px2BO" to="j4ye:~DayOfWeek" resolve="DayOfWeek" />
                      <ref role="Rm8GQ" to="j4ye:~DayOfWeek.Tuesday" resolve="Tuesday" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="7bbHeTWrhM8" role="37vLTJ">
                    <ref role="3cqZAo" node="7bbHeTWqznI" resolve="setEn" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="7bbHeTWrhM9" role="3cqZAp" />
            </node>
          </node>
          <node concept="3KbdKl" id="7bbHeTWrjpA" role="3KbHQx">
            <node concept="Xl_RD" id="7bbHeTWrkyz" role="3Kbmr1">
              <property role="Xl_RC" value="Wednesday" />
            </node>
            <node concept="3clFbS" id="7bbHeTWrjpC" role="3Kbo56">
              <node concept="3clFbF" id="7bbHeTWrkDV" role="3cqZAp">
                <node concept="37vLTI" id="7bbHeTWrkDW" role="3clFbG">
                  <node concept="2YIFZM" id="7bbHeTWrkDX" role="37vLTx">
                    <ref role="1Pybhc" to="33ny:~EnumSet" resolve="EnumSet" />
                    <ref role="37wK5l" to="33ny:~EnumSet.of(java.lang.Enum):java.util.EnumSet" resolve="of" />
                    <node concept="Rm8GO" id="7bbHeTWrlRi" role="37wK5m">
                      <ref role="1Px2BO" to="j4ye:~DayOfWeek" resolve="DayOfWeek" />
                      <ref role="Rm8GQ" to="j4ye:~DayOfWeek.Wednesday" resolve="Wednesday" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="7bbHeTWrkDZ" role="37vLTJ">
                    <ref role="3cqZAo" node="7bbHeTWqznI" resolve="setEn" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="7bbHeTWrkE0" role="3cqZAp" />
            </node>
          </node>
          <node concept="3KbdKl" id="7bbHeTWrmoe" role="3KbHQx">
            <node concept="Xl_RD" id="7bbHeTWrnAj" role="3Kbmr1">
              <property role="Xl_RC" value="Thursday" />
            </node>
            <node concept="3clFbS" id="7bbHeTWrmog" role="3Kbo56">
              <node concept="3clFbF" id="7bbHeTWrnIE" role="3cqZAp">
                <node concept="37vLTI" id="7bbHeTWrnIF" role="3clFbG">
                  <node concept="2YIFZM" id="7bbHeTWrnIG" role="37vLTx">
                    <ref role="1Pybhc" to="33ny:~EnumSet" resolve="EnumSet" />
                    <ref role="37wK5l" to="33ny:~EnumSet.of(java.lang.Enum):java.util.EnumSet" resolve="of" />
                    <node concept="Rm8GO" id="7bbHeTWroXV" role="37wK5m">
                      <ref role="1Px2BO" to="j4ye:~DayOfWeek" resolve="DayOfWeek" />
                      <ref role="Rm8GQ" to="j4ye:~DayOfWeek.Thursday" resolve="Thursday" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="7bbHeTWrnII" role="37vLTJ">
                    <ref role="3cqZAo" node="7bbHeTWqznI" resolve="setEn" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="7bbHeTWrnIJ" role="3cqZAp" />
            </node>
          </node>
          <node concept="3KbdKl" id="7bbHeTWrpzI" role="3KbHQx">
            <node concept="Xl_RD" id="7bbHeTWrqQV" role="3Kbmr1">
              <property role="Xl_RC" value="Friday" />
            </node>
            <node concept="3clFbS" id="7bbHeTWrpzK" role="3Kbo56">
              <node concept="3clFbF" id="7bbHeTWrqYa" role="3cqZAp">
                <node concept="37vLTI" id="7bbHeTWrqYb" role="3clFbG">
                  <node concept="2YIFZM" id="7bbHeTWrqYc" role="37vLTx">
                    <ref role="1Pybhc" to="33ny:~EnumSet" resolve="EnumSet" />
                    <ref role="37wK5l" to="33ny:~EnumSet.of(java.lang.Enum):java.util.EnumSet" resolve="of" />
                    <node concept="Rm8GO" id="7bbHeTWrsfl" role="37wK5m">
                      <ref role="Rm8GQ" to="j4ye:~DayOfWeek.Friday" resolve="Friday" />
                      <ref role="1Px2BO" to="j4ye:~DayOfWeek" resolve="DayOfWeek" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="7bbHeTWrqYe" role="37vLTJ">
                    <ref role="3cqZAo" node="7bbHeTWqznI" resolve="setEn" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="7bbHeTWrqYf" role="3cqZAp" />
            </node>
          </node>
          <node concept="3KbdKl" id="7bbHeTWrsTZ" role="3KbHQx">
            <node concept="Xl_RD" id="7bbHeTWruik" role="3Kbmr1">
              <property role="Xl_RC" value="Saturday" />
            </node>
            <node concept="3clFbS" id="7bbHeTWrsU1" role="3Kbo56">
              <node concept="3clFbF" id="7bbHeTWruov" role="3cqZAp">
                <node concept="37vLTI" id="7bbHeTWruow" role="3clFbG">
                  <node concept="2YIFZM" id="7bbHeTWruox" role="37vLTx">
                    <ref role="1Pybhc" to="33ny:~EnumSet" resolve="EnumSet" />
                    <ref role="37wK5l" to="33ny:~EnumSet.of(java.lang.Enum):java.util.EnumSet" resolve="of" />
                    <node concept="Rm8GO" id="7bbHeTWrvF$" role="37wK5m">
                      <ref role="1Px2BO" to="j4ye:~DayOfWeek" resolve="DayOfWeek" />
                      <ref role="Rm8GQ" to="j4ye:~DayOfWeek.Saturday" resolve="Saturday" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="7bbHeTWruoz" role="37vLTJ">
                    <ref role="3cqZAo" node="7bbHeTWqznI" resolve="setEn" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="7bbHeTWruo$" role="3cqZAp" />
            </node>
          </node>
          <node concept="3KbdKl" id="7bbHeTWrycF" role="3KbHQx">
            <node concept="Xl_RD" id="7bbHeTWrzE8" role="3Kbmr1">
              <property role="Xl_RC" value="Sunday" />
            </node>
            <node concept="3clFbS" id="7bbHeTWrycH" role="3Kbo56">
              <node concept="3clFbF" id="7bbHeTWrzLD" role="3cqZAp">
                <node concept="37vLTI" id="7bbHeTWrzLE" role="3clFbG">
                  <node concept="2YIFZM" id="7bbHeTWrzLF" role="37vLTx">
                    <ref role="37wK5l" to="33ny:~EnumSet.of(java.lang.Enum):java.util.EnumSet" resolve="of" />
                    <ref role="1Pybhc" to="33ny:~EnumSet" resolve="EnumSet" />
                    <node concept="Rm8GO" id="7bbHeTWr_6C" role="37wK5m">
                      <ref role="1Px2BO" to="j4ye:~DayOfWeek" resolve="DayOfWeek" />
                      <ref role="Rm8GQ" to="j4ye:~DayOfWeek.Sunday" resolve="Sunday" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="7bbHeTWrzLH" role="37vLTJ">
                    <ref role="3cqZAo" node="7bbHeTWqznI" resolve="setEn" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="7bbHeTWrzLI" role="3cqZAp" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWrMdG" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWrWjf" role="3clFbG">
            <node concept="37vLTw" id="7bbHeTWrMdE" role="2Oq$k0">
              <ref role="3cqZAo" node="7bbHeTWpLVj" resolve="rec" />
            </node>
            <node concept="liA8E" id="7bbHeTWrY1x" role="2OqNvi">
              <ref role="37wK5l" to="spdm:~Recurrence.setDaysOfWeek(java.util.EnumSet):void" resolve="setDaysOfWeek" />
              <node concept="37vLTw" id="7bbHeTWrZat" role="37wK5m">
                <ref role="3cqZAo" node="7bbHeTWqznI" resolve="setEn" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWs7A1" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWsdXi" role="3clFbG">
            <node concept="37vLTw" id="7bbHeTWs7_Z" role="2Oq$k0">
              <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
            </node>
            <node concept="liA8E" id="7bbHeTWsiKV" role="2OqNvi">
              <ref role="37wK5l" to="spdm:~Appointment.setStartTime(com.mindfusion.common.DateTime):void" resolve="setStartTime" />
              <node concept="37vLTw" id="7bbHeTWsjpz" role="37wK5m">
                <ref role="3cqZAo" node="7bbHeTWpc$e" resolve="sdt" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWsq1K" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWsvZd" role="3clFbG">
            <node concept="37vLTw" id="7bbHeTWsq1I" role="2Oq$k0">
              <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
            </node>
            <node concept="liA8E" id="7bbHeTWsxoz" role="2OqNvi">
              <ref role="37wK5l" to="spdm:~Appointment.setEndTime(com.mindfusion.common.DateTime):void" resolve="setEndTime" />
              <node concept="37vLTw" id="7bbHeTWsA7a" role="37wK5m">
                <ref role="3cqZAo" node="7bbHeTWpxeT" resolve="edt" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7bbHeTWsIgZ" role="3cqZAp">
          <node concept="2OqwBi" id="7bbHeTWsOeI" role="3clFbG">
            <node concept="37vLTw" id="7bbHeTWsIgX" role="2Oq$k0">
              <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
            </node>
            <node concept="liA8E" id="7bbHeTWsRjE" role="2OqNvi">
              <ref role="37wK5l" to="spdm:~Item.setRecurrence(com.mindfusion.scheduling.model.Recurrence):void" resolve="setRecurrence" />
              <node concept="37vLTw" id="7bbHeTWsU2u" role="37wK5m">
                <ref role="3cqZAo" node="7bbHeTWpLVj" resolve="rec" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="7bbHeTWw9Nr" role="3cqZAp">
          <node concept="3clFbS" id="7bbHeTWw9Nt" role="3clFbx">
            <node concept="3cpWs8" id="7bbHeTWwIwR" role="3cqZAp">
              <node concept="3cpWsn" id="7bbHeTWwIwS" role="3cpWs9">
                <property role="TrG5h" value="style" />
                <node concept="3uibUv" id="7bbHeTWwIwT" role="1tU5fm">
                  <ref role="3uigEE" to="spdm:~Style" resolve="Style" />
                </node>
                <node concept="2OqwBi" id="7bbHeTWwLOA" role="33vP2m">
                  <node concept="37vLTw" id="7bbHeTWwL4z" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWwMAk" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Appointment.getStyle():com.mindfusion.scheduling.model.Style" resolve="getStyle" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWwNR4" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWwOyx" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWwNR2" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWwPHx" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setBorderTopColor(java.awt.Color):void" resolve="setBorderTopColor" />
                  <node concept="10M0yZ" id="7bbHeTWwRwS" role="37wK5m">
                    <ref role="3cqZAo" to="tr4j:~Colors.DarkOrchid" resolve="DarkOrchid" />
                    <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWwSaF" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWwSQP" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWwSaD" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWwU1P" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setBorderLeftColor(java.awt.Color):void" resolve="setBorderLeftColor" />
                  <node concept="10M0yZ" id="7bbHeTWwUFx" role="37wK5m">
                    <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    <ref role="3cqZAo" to="tr4j:~Colors.DarkOrchid" resolve="DarkOrchid" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWwVlZ" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWwW48" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWwVlX" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWwXf8" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setBorderBottomColor(java.awt.Color):void" resolve="setBorderBottomColor" />
                  <node concept="10M0yZ" id="7bbHeTWwXT9" role="37wK5m">
                    <ref role="3cqZAo" to="tr4j:~Colors.DarkOrchid" resolve="DarkOrchid" />
                    <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWwY$k" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWwZhS" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWwY$i" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWx0sS" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setBorderRightColor(java.awt.Color):void" resolve="setBorderRightColor" />
                  <node concept="10M0yZ" id="7bbHeTWx17e" role="37wK5m">
                    <ref role="3cqZAo" to="tr4j:~Colors.DarkOrchid" resolve="DarkOrchid" />
                    <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWx5QY" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWx6_f" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWx5QW" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWx7Kf" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setLineColor(java.awt.Color):void" resolve="setLineColor" />
                  <node concept="10M0yZ" id="7bbHeTWx8qU" role="37wK5m">
                    <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    <ref role="3cqZAo" to="tr4j:~Colors.DarkOrchid" resolve="DarkOrchid" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWx97v" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWx9N$" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWx97t" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWxb0d" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setFillColor(java.awt.Color):void" resolve="setFillColor" />
                  <node concept="10M0yZ" id="7bbHeTWxbFd" role="37wK5m">
                    <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    <ref role="3cqZAo" to="tr4j:~Colors.MediumOrchid" resolve="MediumOrchid" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWxcV$" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWxdGz" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWxcVy" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWxeRz" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setBrush(com.mindfusion.drawing.Brush):void" resolve="setBrush" />
                  <node concept="2ShNRf" id="7bbHeTWxfvv" role="37wK5m">
                    <node concept="1pGfFk" id="7bbHeTWxhws" role="2ShVmc">
                      <ref role="37wK5l" to="tr4j:~GradientBrush.&lt;init&gt;(java.awt.Color,java.awt.Color,int)" resolve="GradientBrush" />
                      <node concept="10M0yZ" id="7bbHeTWxia$" role="37wK5m">
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                        <ref role="3cqZAo" to="tr4j:~Colors.White" resolve="White" />
                      </node>
                      <node concept="10M0yZ" id="7bbHeTWxiRA" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.Plum" resolve="Plum" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                      <node concept="3cmrfG" id="7bbHeTWxjyc" role="37wK5m">
                        <property role="3cmrfH" value="90" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWxkQP" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWxl$B" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWxkQN" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWxmMI" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setHeaderTextColor(java.awt.Color):void" resolve="setHeaderTextColor" />
                  <node concept="10M0yZ" id="7bbHeTWxnuo" role="37wK5m">
                    <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    <ref role="3cqZAo" to="tr4j:~Colors.DarkViolet" resolve="DarkViolet" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7bbHeTWxoes" role="3cqZAp">
              <node concept="2OqwBi" id="7bbHeTWxp0s" role="3clFbG">
                <node concept="37vLTw" id="7bbHeTWxoeq" role="2Oq$k0">
                  <ref role="3cqZAo" node="7bbHeTWwIwS" resolve="style" />
                </node>
                <node concept="liA8E" id="7bbHeTWxqbR" role="2OqNvi">
                  <ref role="37wK5l" to="spdm:~Style.setTextColor(java.awt.Color):void" resolve="setTextColor" />
                  <node concept="10M0yZ" id="7bbHeTWxr_d" role="37wK5m">
                    <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    <ref role="3cqZAo" to="tr4j:~Colors.DarkViolet" resolve="DarkViolet" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="7bbHeTWwpbC" role="3clFbw">
            <node concept="AH0OO" id="7bbHeTWwnnB" role="2Oq$k0">
              <node concept="3cmrfG" id="7bbHeTWwoNT" role="AHEQo">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="37vLTw" id="7bbHeTWwlMl" role="AHHXb">
                <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
              </node>
            </node>
            <node concept="liA8E" id="7bbHeTWwrkO" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="Xl_RD" id="7bbHeTWws87" role="37wK5m">
                <property role="Xl_RC" value="Course" />
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="7bbHeTWwt9n" role="3eNLev">
            <node concept="2OqwBi" id="7bbHeTWwxEr" role="3eO9$A">
              <node concept="AH0OO" id="7bbHeTWwvYz" role="2Oq$k0">
                <node concept="3cmrfG" id="7bbHeTWwwJL" role="AHEQo">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="37vLTw" id="7bbHeTWwuxq" role="AHHXb">
                  <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                </node>
              </node>
              <node concept="liA8E" id="7bbHeTWwzkJ" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                <node concept="Xl_RD" id="7bbHeTWw$uZ" role="37wK5m">
                  <property role="Xl_RC" value="Meal" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="7bbHeTWwt9p" role="3eOfB_">
              <node concept="3cpWs8" id="7bbHeTWxrCd" role="3cqZAp">
                <node concept="3cpWsn" id="7bbHeTWxrCe" role="3cpWs9">
                  <property role="TrG5h" value="style" />
                  <node concept="3uibUv" id="7bbHeTWxrCf" role="1tU5fm">
                    <ref role="3uigEE" to="spdm:~Style" resolve="Style" />
                  </node>
                  <node concept="2OqwBi" id="7bbHeTWxrCg" role="33vP2m">
                    <node concept="37vLTw" id="7bbHeTWxrCh" role="2Oq$k0">
                      <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
                    </node>
                    <node concept="liA8E" id="7bbHeTWxrCi" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Appointment.getStyle():com.mindfusion.scheduling.model.Style" resolve="getStyle" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrCj" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrCk" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrCl" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrCm" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBorderTopColor(java.awt.Color):void" resolve="setBorderTopColor" />
                    <node concept="10M0yZ" id="7bbHeTWylr0" role="37wK5m">
                      <ref role="3cqZAo" to="tr4j:~Colors.Firebrick" resolve="Firebrick" />
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrCr" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrCs" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrCt" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrCu" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBorderLeftColor(java.awt.Color):void" resolve="setBorderLeftColor" />
                    <node concept="10M0yZ" id="7bbHeTWylua" role="37wK5m">
                      <ref role="3cqZAo" to="tr4j:~Colors.Firebrick" resolve="Firebrick" />
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrCz" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrC$" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrC_" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrCA" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBorderBottomColor(java.awt.Color):void" resolve="setBorderBottomColor" />
                    <node concept="10M0yZ" id="7bbHeTWylxe" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.Firebrick" resolve="Firebrick" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrCF" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrCG" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrCH" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrCI" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBorderRightColor(java.awt.Color):void" resolve="setBorderRightColor" />
                    <node concept="10M0yZ" id="7bbHeTWyl$i" role="37wK5m">
                      <ref role="3cqZAo" to="tr4j:~Colors.Firebrick" resolve="Firebrick" />
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrCN" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrCO" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrCP" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrCQ" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setLineColor(java.awt.Color):void" resolve="setLineColor" />
                    <node concept="10M0yZ" id="7bbHeTWylBm" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.Firebrick" resolve="Firebrick" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrCV" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrCW" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrCX" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrCY" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setFillColor(java.awt.Color):void" resolve="setFillColor" />
                    <node concept="10M0yZ" id="7bbHeTWylEu" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.Brown" resolve="Brown" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrD3" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrD4" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrD5" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrD6" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBrush(com.mindfusion.drawing.Brush):void" resolve="setBrush" />
                    <node concept="2ShNRf" id="7bbHeTWxrD7" role="37wK5m">
                      <node concept="1pGfFk" id="7bbHeTWxrD8" role="2ShVmc">
                        <ref role="37wK5l" to="tr4j:~GradientBrush.&lt;init&gt;(java.awt.Color,java.awt.Color,int)" resolve="GradientBrush" />
                        <node concept="10M0yZ" id="7bbHeTWxrD9" role="37wK5m">
                          <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                          <ref role="3cqZAo" to="tr4j:~Colors.White" resolve="White" />
                        </node>
                        <node concept="10M0yZ" id="7bbHeTWylH$" role="37wK5m">
                          <ref role="3cqZAo" to="tr4j:~Colors.BurlyWood" resolve="BurlyWood" />
                          <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                        </node>
                        <node concept="3cmrfG" id="7bbHeTWxrDb" role="37wK5m">
                          <property role="3cmrfH" value="90" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrDi" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrDj" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrDk" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrDl" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setHeaderTextColor(java.awt.Color):void" resolve="setHeaderTextColor" />
                    <node concept="10M0yZ" id="7bbHeTWylLy" role="37wK5m">
                      <ref role="3cqZAo" to="tr4j:~Colors.Firebrick" resolve="Firebrick" />
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxrDq" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxrDr" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxrDs" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxrCe" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxrDt" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setTextColor(java.awt.Color):void" resolve="setTextColor" />
                    <node concept="10M0yZ" id="7bbHeTWylOA" role="37wK5m">
                      <ref role="3cqZAo" to="tr4j:~Colors.Firebrick" resolve="Firebrick" />
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3eNFk2" id="7bbHeTWw$AB" role="3eNLev">
            <node concept="2OqwBi" id="7bbHeTWwD8o" role="3eO9$A">
              <node concept="AH0OO" id="7bbHeTWwBsw" role="2Oq$k0">
                <node concept="3cmrfG" id="7bbHeTWwCBD" role="AHEQo">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="37vLTw" id="7bbHeTWw_Zn" role="AHHXb">
                  <ref role="3cqZAo" node="7bbHeTWkZt$" resolve="splitStr" />
                </node>
              </node>
              <node concept="liA8E" id="7bbHeTWwFkj" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                <node concept="Xl_RD" id="7bbHeTWwFXc" role="37wK5m">
                  <property role="Xl_RC" value="Rest" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="7bbHeTWw$AD" role="3eOfB_">
              <node concept="3cpWs8" id="7bbHeTWxth1" role="3cqZAp">
                <node concept="3cpWsn" id="7bbHeTWxth2" role="3cpWs9">
                  <property role="TrG5h" value="style" />
                  <node concept="3uibUv" id="7bbHeTWxth3" role="1tU5fm">
                    <ref role="3uigEE" to="spdm:~Style" resolve="Style" />
                  </node>
                  <node concept="2OqwBi" id="7bbHeTWxth4" role="33vP2m">
                    <node concept="37vLTw" id="7bbHeTWxth5" role="2Oq$k0">
                      <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
                    </node>
                    <node concept="liA8E" id="7bbHeTWxth6" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Appointment.getStyle():com.mindfusion.scheduling.model.Style" resolve="getStyle" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxth7" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxth8" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxth9" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxtha" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBorderTopColor(java.awt.Color):void" resolve="setBorderTopColor" />
                    <node concept="10M0yZ" id="7bbHeTWylRI" role="37wK5m">
                      <ref role="3cqZAo" to="tr4j:~Colors.DarkGreen" resolve="DarkGreen" />
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxthf" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxthg" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxthh" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxthi" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBorderLeftColor(java.awt.Color):void" resolve="setBorderLeftColor" />
                    <node concept="10M0yZ" id="7bbHeTWylUO" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.DarkGreen" resolve="DarkGreen" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxthn" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxtho" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxthp" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxthq" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBorderBottomColor(java.awt.Color):void" resolve="setBorderBottomColor" />
                    <node concept="10M0yZ" id="7bbHeTWylXS" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.DarkGreen" resolve="DarkGreen" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxthv" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxthw" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxthx" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxthy" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBorderRightColor(java.awt.Color):void" resolve="setBorderRightColor" />
                    <node concept="10M0yZ" id="7bbHeTWym0W" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.DarkGreen" resolve="DarkGreen" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxthB" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxthC" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxthD" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxthE" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setLineColor(java.awt.Color):void" resolve="setLineColor" />
                    <node concept="10M0yZ" id="7bbHeTWyma8" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.DarkGreen" resolve="DarkGreen" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxthJ" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxthK" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxthL" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxthM" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setFillColor(java.awt.Color):void" resolve="setFillColor" />
                    <node concept="10M0yZ" id="7bbHeTWymdg" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.DarkOliveGreen" resolve="DarkOliveGreen" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxthR" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxthS" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxthT" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxthU" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setBrush(com.mindfusion.drawing.Brush):void" resolve="setBrush" />
                    <node concept="2ShNRf" id="7bbHeTWxthV" role="37wK5m">
                      <node concept="1pGfFk" id="7bbHeTWxthW" role="2ShVmc">
                        <ref role="37wK5l" to="tr4j:~GradientBrush.&lt;init&gt;(java.awt.Color,java.awt.Color,int)" resolve="GradientBrush" />
                        <node concept="10M0yZ" id="7bbHeTWxthX" role="37wK5m">
                          <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                          <ref role="3cqZAo" to="tr4j:~Colors.White" resolve="White" />
                        </node>
                        <node concept="10M0yZ" id="7bbHeTWymgm" role="37wK5m">
                          <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                          <ref role="3cqZAo" to="tr4j:~Colors.LightYellow" resolve="LightYellow" />
                        </node>
                        <node concept="3cmrfG" id="7bbHeTWxthZ" role="37wK5m">
                          <property role="3cmrfH" value="90" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxti6" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxti7" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxti8" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxti9" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setHeaderTextColor(java.awt.Color):void" resolve="setHeaderTextColor" />
                    <node concept="10M0yZ" id="7bbHeTWym40" role="37wK5m">
                      <ref role="3cqZAo" to="tr4j:~Colors.DarkGreen" resolve="DarkGreen" />
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="7bbHeTWxtie" role="3cqZAp">
                <node concept="2OqwBi" id="7bbHeTWxtif" role="3clFbG">
                  <node concept="37vLTw" id="7bbHeTWxtig" role="2Oq$k0">
                    <ref role="3cqZAo" node="7bbHeTWxth2" resolve="style" />
                  </node>
                  <node concept="liA8E" id="7bbHeTWxtih" role="2OqNvi">
                    <ref role="37wK5l" to="spdm:~Style.setTextColor(java.awt.Color):void" resolve="setTextColor" />
                    <node concept="10M0yZ" id="7bbHeTWym74" role="37wK5m">
                      <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      <ref role="3cqZAo" to="tr4j:~Colors.DarkGreen" resolve="DarkGreen" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="7bbHeTWm3Hk" role="3cqZAp">
          <node concept="37vLTw" id="7bbHeTWmaTE" role="3cqZAk">
            <ref role="3cqZAo" node="7bbHeTWlvvz" resolve="app" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="7bbHeTWkFsL" role="1B3o_S" />
      <node concept="3uibUv" id="2OB0Aer44rf" role="3clF45">
        <ref role="3uigEE" to="spdm:~Appointment" resolve="Appointment" />
      </node>
      <node concept="37vLTG" id="7bbHeTWkZt$" role="3clF46">
        <property role="TrG5h" value="splitStr" />
        <node concept="10Q1$e" id="7bbHeTWl9O8" role="1tU5fm">
          <node concept="17QB3L" id="7bbHeTWoCL9" role="10Q1$1" />
        </node>
      </node>
      <node concept="3uibUv" id="7bbHeTWoWHA" role="Sfmx6">
        <ref role="3uigEE" to="25x5:~ParseException" resolve="ParseException" />
      </node>
    </node>
    <node concept="2tJIrI" id="2OB0Aer9mE4" role="jymVt" />
    <node concept="2tJIrI" id="2OB0Aer84zB" role="jymVt" />
    <node concept="2YIFZL" id="2OB0Aer8wzk" role="jymVt">
      <property role="TrG5h" value="scheduleApp" />
      <node concept="3clFbS" id="2OB0Aer8wzn" role="3clF47">
        <node concept="3clFbF" id="2OB0Aer8Pz5" role="3cqZAp">
          <node concept="2OqwBi" id="2OB0Aer8Pz2" role="3clFbG">
            <node concept="10M0yZ" id="2OB0Aer8Pz3" role="2Oq$k0">
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
            </node>
            <node concept="liA8E" id="2OB0Aer8Pz4" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="Xl_RD" id="2OB0Aer8Qv3" role="37wK5m">
                <property role="Xl_RC" value="action performed" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0Aer8T4_" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0Aer8T4A" role="3cpWs9">
            <property role="TrG5h" value="s" />
            <node concept="3uibUv" id="2OB0Aer8T4B" role="1tU5fm">
              <ref role="3uigEE" to="wyt6:~String" resolve="String" />
            </node>
            <node concept="Xl_RD" id="2OB0Aer8WPy" role="33vP2m">
              <property role="Xl_RC" value="SWEN24, 24/5/2019,Assignment" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0Aer8XSp" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0Aer8XSv" role="3cpWs9">
            <property role="TrG5h" value="spl" />
            <node concept="10Q1$e" id="2OB0Aer8XSx" role="1tU5fm">
              <node concept="3uibUv" id="2OB0Aer8XSz" role="10Q1$1">
                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
              </node>
            </node>
            <node concept="2OqwBi" id="2OB0AerduaK" role="33vP2m">
              <node concept="37vLTw" id="2OB0Aerdt46" role="2Oq$k0">
                <ref role="3cqZAo" node="2OB0Aer8DZK" resolve="details" />
              </node>
              <node concept="liA8E" id="2OB0AerdvnE" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.split(java.lang.String):java.lang.String[]" resolve="split" />
                <node concept="Xl_RD" id="2OB0Aerdw0r" role="37wK5m">
                  <property role="Xl_RC" value="," />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5t_d$dJqtWg" role="3cqZAp">
          <node concept="2OqwBi" id="5t_d$dJqtWd" role="3clFbG">
            <node concept="10M0yZ" id="5t_d$dJqtWe" role="2Oq$k0">
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
            </node>
            <node concept="liA8E" id="5t_d$dJqtWf" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="3cpWs3" id="5t_d$dJq$a3" role="37wK5m">
                <node concept="Xl_RD" id="5t_d$dJqwI2" role="3uHU7B">
                  <property role="Xl_RC" value="split2:" />
                </node>
                <node concept="37vLTw" id="5t_d$dJqWZa" role="3uHU7w">
                  <ref role="3cqZAo" node="2OB0Aer8DZK" resolve="details" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0Aerdx$6" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0Aerdx$9" role="3cpWs9">
            <property role="TrG5h" value="spendHours" />
            <node concept="10Oyi0" id="2OB0Aerdx$4" role="1tU5fm" />
            <node concept="2YIFZM" id="2OB0Aerd$Lb" role="33vP2m">
              <ref role="37wK5l" to="wyt6:~Integer.parseInt(java.lang.String):int" resolve="parseInt" />
              <ref role="1Pybhc" to="wyt6:~Integer" resolve="Integer" />
              <node concept="AH0OO" id="2OB0AerdB3x" role="37wK5m">
                <node concept="3cmrfG" id="2OB0AerdBEN" role="AHEQo">
                  <property role="3cmrfH" value="2" />
                </node>
                <node concept="37vLTw" id="2OB0AerdAf3" role="AHHXb">
                  <ref role="3cqZAo" node="2OB0Aer8XSv" resolve="spl" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AerdDre" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AerdDrk" role="3cpWs9">
            <property role="TrG5h" value="arr" />
            <node concept="10Q1$e" id="2OB0AerdDrm" role="1tU5fm">
              <node concept="3uibUv" id="2OB0AerdDro" role="10Q1$1">
                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
              </node>
            </node>
            <node concept="2OqwBi" id="2OB0AerdIdA" role="33vP2m">
              <node concept="37vLTw" id="2OB0AerdHU4" role="2Oq$k0">
                <ref role="3cqZAo" node="2OB0Aer8T4A" resolve="s" />
              </node>
              <node concept="liA8E" id="2OB0AerdIQu" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.split(java.lang.String):java.lang.String[]" resolve="split" />
                <node concept="Xl_RD" id="2OB0AerdJvl" role="37wK5m">
                  <property role="Xl_RC" value="," />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AerdOHm" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AerdOHn" role="3cpWs9">
            <property role="TrG5h" value="dateStr" />
            <node concept="3uibUv" id="2OB0AerdOHo" role="1tU5fm">
              <ref role="3uigEE" to="wyt6:~String" resolve="String" />
            </node>
            <node concept="2OqwBi" id="2OB0AerdVA_" role="33vP2m">
              <node concept="AH0OO" id="2OB0AerdUKo" role="2Oq$k0">
                <node concept="3cmrfG" id="2OB0AerdVlR" role="AHEQo">
                  <property role="3cmrfH" value="3" />
                </node>
                <node concept="37vLTw" id="2OB0AerdTXl" role="AHHXb">
                  <ref role="3cqZAo" node="2OB0Aer8XSv" resolve="spl" />
                </node>
              </node>
              <node concept="liA8E" id="2OB0AerdYPh" role="2OqNvi">
                <ref role="37wK5l" to="wyt6:~String.replaceAll(java.lang.String,java.lang.String):java.lang.String" resolve="replaceAll" />
                <node concept="Xl_RD" id="2OB0AerdZud" role="37wK5m">
                  <property role="Xl_RC" value="/" />
                </node>
                <node concept="Xl_RD" id="2OB0Aere0eI" role="37wK5m" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0Aere3LD" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0Aere3LE" role="3cpWs9">
            <property role="TrG5h" value="dateFormat" />
            <node concept="3uibUv" id="2OB0Aere3LF" role="1tU5fm">
              <ref role="3uigEE" to="25x5:~DateFormat" resolve="DateFormat" />
            </node>
            <node concept="2ShNRf" id="2OB0Aere6fe" role="33vP2m">
              <node concept="1pGfFk" id="2OB0Aere8eB" role="2ShVmc">
                <ref role="37wK5l" to="25x5:~SimpleDateFormat.&lt;init&gt;(java.lang.String)" resolve="SimpleDateFormat" />
                <node concept="Xl_RD" id="2OB0Aere8Or" role="37wK5m">
                  <property role="Xl_RC" value="ddMMyyyy" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AerebT3" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AerebT4" role="3cpWs9">
            <property role="TrG5h" value="dueDate" />
            <node concept="3uibUv" id="2OB0AerebT5" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~Date" resolve="Date" />
            </node>
            <node concept="2OqwBi" id="2OB0AerefYU" role="33vP2m">
              <node concept="37vLTw" id="2OB0Aerefca" role="2Oq$k0">
                <ref role="3cqZAo" node="2OB0Aere3LE" resolve="dateFormat" />
              </node>
              <node concept="liA8E" id="2OB0AereioW" role="2OqNvi">
                <ref role="37wK5l" to="25x5:~DateFormat.parse(java.lang.String):java.util.Date" resolve="parse" />
                <node concept="37vLTw" id="2OB0AerekaQ" role="37wK5m">
                  <ref role="3cqZAo" node="2OB0AerdOHn" resolve="dateStr" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AereqSD" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AereqSE" role="3cpWs9">
            <property role="TrG5h" value="ddt" />
            <node concept="3uibUv" id="2OB0AereqSF" role="1tU5fm">
              <ref role="3uigEE" to="j4ye:~DateTime" resolve="DateTime" />
            </node>
            <node concept="2ShNRf" id="2OB0Aereuas" role="33vP2m">
              <node concept="1pGfFk" id="2OB0Aerewag" role="2ShVmc">
                <ref role="37wK5l" to="j4ye:~DateTime.&lt;init&gt;(java.util.Date)" resolve="DateTime" />
                <node concept="37vLTw" id="2OB0AerewL5" role="37wK5m">
                  <ref role="3cqZAo" node="2OB0AerebT4" resolve="dueDate" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AereyvB" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AereyvE" role="3cpWs9">
            <property role="TrG5h" value="remainingDays" />
            <node concept="10Oyi0" id="2OB0Aereyv_" role="1tU5fm" />
            <node concept="2OqwBi" id="2OB0AereRgj" role="33vP2m">
              <node concept="2YIFZM" id="2OB0AereCfN" role="2Oq$k0">
                <ref role="37wK5l" to="j4ye:~DateTime.op_Subtraction(com.mindfusion.common.DateTime,com.mindfusion.common.DateTime):com.mindfusion.common.Duration" resolve="op_Subtraction" />
                <ref role="1Pybhc" to="j4ye:~DateTime" resolve="DateTime" />
                <node concept="37vLTw" id="2OB0AereExq" role="37wK5m">
                  <ref role="3cqZAo" node="2OB0AereqSE" resolve="ddt" />
                </node>
                <node concept="2YIFZM" id="2OB0AereL3P" role="37wK5m">
                  <ref role="37wK5l" to="j4ye:~DateTime.now():com.mindfusion.common.DateTime" resolve="now" />
                  <ref role="1Pybhc" to="j4ye:~DateTime" resolve="DateTime" />
                </node>
              </node>
              <node concept="liA8E" id="2OB0Aerf0EP" role="2OqNvi">
                <ref role="37wK5l" to="j4ye:~Duration.getDays():int" resolve="getDays" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0Aerf2s6" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0Aerf2s9" role="3cpWs9">
            <property role="TrG5h" value="perdayWork" />
            <node concept="10P55v" id="2OB0Aerf2s4" role="1tU5fm" />
            <node concept="FJ1c_" id="2OB0Aerf67d" role="33vP2m">
              <node concept="37vLTw" id="2OB0Aerf7gd" role="3uHU7w">
                <ref role="3cqZAo" node="2OB0AereyvE" resolve="remainingDays" />
              </node>
              <node concept="37vLTw" id="2OB0Aerf4Lc" role="3uHU7B">
                <ref role="3cqZAo" node="2OB0Aerdx$9" resolve="spendHours" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0Aerjhw3" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0Aerjhw4" role="3cpWs9">
            <property role="TrG5h" value="superList" />
            <node concept="3uibUv" id="2OB0Aerjhw5" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
              <node concept="3uibUv" id="2OB0AerydaM" role="11_B2D">
                <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
              </node>
            </node>
            <node concept="1rXfSq" id="2OB0AerjkP7" role="33vP2m">
              <ref role="37wK5l" node="2OB0AerfGcM" resolve="computeRepartitionNumber" />
              <node concept="37vLTw" id="2OB0AerjmK4" role="37wK5m">
                <ref role="3cqZAo" node="2OB0Aerdx$9" resolve="spendHours" />
              </node>
              <node concept="37vLTw" id="2OB0Aerjo91" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AereyvE" resolve="remainingDays" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0Aerzh1P" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0Aerzh1Q" role="3cpWs9">
            <property role="TrG5h" value="sch" />
            <node concept="3uibUv" id="2OB0Aerzh1R" role="1tU5fm">
              <ref role="3uigEE" to="spdm:~Schedule" resolve="Schedule" />
            </node>
            <node concept="2OqwBi" id="2OB0AerzmIq" role="33vP2m">
              <node concept="37vLTw" id="2OB0AerzleT" role="2Oq$k0">
                <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
              </node>
              <node concept="liA8E" id="2OB0AerzpXm" role="2OqNvi">
                <ref role="37wK5l" to="1gjj:~Calendar.getSchedule():com.mindfusion.scheduling.model.Schedule" resolve="getSchedule" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AersA$W" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AersA$Z" role="3cpWs9">
            <property role="TrG5h" value="dur" />
            <node concept="10P55v" id="2OB0AersA$U" role="1tU5fm" />
            <node concept="3cmrfG" id="2OB0AersCCi" role="33vP2m">
              <property role="3cmrfH" value="2" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AersHZ8" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AersHZ9" role="3cpWs9">
            <property role="TrG5h" value="dura" />
            <node concept="3uibUv" id="2OB0AersHZa" role="1tU5fm">
              <ref role="3uigEE" to="j4ye:~Duration" resolve="Duration" />
            </node>
            <node concept="2YIFZM" id="2OB0AersP8X" role="33vP2m">
              <ref role="37wK5l" to="j4ye:~Duration.fromHours(double):com.mindfusion.common.Duration" resolve="fromHours" />
              <ref role="1Pybhc" to="j4ye:~Duration" resolve="Duration" />
              <node concept="3cmrfG" id="2OB0AersRgB" role="37wK5m">
                <property role="3cmrfH" value="2" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AersVd5" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AersVd6" role="3cpWs9">
            <property role="TrG5h" value="dateVal" />
            <node concept="3uibUv" id="2OB0AersVd7" role="1tU5fm">
              <ref role="3uigEE" to="j4ye:~DateTime" resolve="DateTime" />
            </node>
            <node concept="2YIFZM" id="2OB0Aert3Vp" role="33vP2m">
              <ref role="37wK5l" to="j4ye:~DateTime.today():com.mindfusion.common.DateTime" resolve="today" />
              <ref role="1Pybhc" to="j4ye:~DateTime" resolve="DateTime" />
            </node>
          </node>
        </node>
        <node concept="1Dw8fO" id="2OB0Aert8xV" role="3cqZAp">
          <node concept="3clFbS" id="2OB0Aert8xX" role="2LFqv$">
            <node concept="3cpWs8" id="2OB0Aer$K$u" role="3cqZAp">
              <node concept="3cpWsn" id="2OB0Aer$K$v" role="3cpWs9">
                <property role="TrG5h" value="duran" />
                <node concept="3uibUv" id="2OB0Aer$K$w" role="1tU5fm">
                  <ref role="3uigEE" to="j4ye:~Duration" resolve="Duration" />
                </node>
                <node concept="2YIFZM" id="2OB0Aer$Rpy" role="33vP2m">
                  <ref role="37wK5l" to="j4ye:~Duration.fromHours(double):com.mindfusion.common.Duration" resolve="fromHours" />
                  <ref role="1Pybhc" to="j4ye:~Duration" resolve="Duration" />
                  <node concept="2OqwBi" id="2OB0Aer$VTW" role="37wK5m">
                    <node concept="37vLTw" id="2OB0Aer$U6A" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aerjhw4" resolve="superList" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer_80s" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.get(int):java.lang.Object" resolve="get" />
                      <node concept="37vLTw" id="2OB0Aer_9Cz" role="37wK5m">
                        <ref role="3cqZAo" node="2OB0Aert8xY" resolve="i" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2OB0AertU18" role="3cqZAp">
              <node concept="37vLTI" id="2OB0AertVGM" role="3clFbG">
                <node concept="2OqwBi" id="2OB0AertX_B" role="37vLTx">
                  <node concept="37vLTw" id="2OB0AertWPd" role="2Oq$k0">
                    <ref role="3cqZAo" node="2OB0AersVd6" resolve="dateVal" />
                  </node>
                  <node concept="liA8E" id="2OB0AertYpQ" role="2OqNvi">
                    <ref role="37wK5l" to="j4ye:~DateTime.addHours(double):com.mindfusion.common.DateTime" resolve="addHours" />
                    <node concept="3cmrfG" id="2OB0AertZ1E" role="37wK5m">
                      <property role="3cmrfH" value="24" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="2OB0AertU16" role="37vLTJ">
                  <ref role="3cqZAo" node="2OB0AersVd6" resolve="dateVal" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="2OB0Aerye6x" role="3cqZAp">
              <node concept="3clFbS" id="2OB0Aerye6z" role="3clFbx">
                <node concept="3cpWs8" id="2OB0AeryDCU" role="3cqZAp">
                  <node concept="3cpWsn" id="2OB0AeryDCV" role="3cpWs9">
                    <property role="TrG5h" value="newDateVal" />
                    <node concept="3uibUv" id="2OB0AeryDCW" role="1tU5fm">
                      <ref role="3uigEE" to="j4ye:~DateTime" resolve="DateTime" />
                    </node>
                    <node concept="2OqwBi" id="2OB0AeryHl9" role="33vP2m">
                      <node concept="37vLTw" id="2OB0AeryG$I" role="2Oq$k0">
                        <ref role="3cqZAo" node="2OB0AersVd6" resolve="dateVal" />
                      </node>
                      <node concept="liA8E" id="2OB0AeryI9n" role="2OqNvi">
                        <ref role="37wK5l" to="j4ye:~DateTime.addHours(double):com.mindfusion.common.DateTime" resolve="addHours" />
                        <node concept="3cmrfG" id="2OB0AeryILm" role="37wK5m">
                          <property role="3cmrfH" value="24" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="2OB0Aerz_y4" role="3cqZAp">
                  <node concept="3cpWsn" id="2OB0Aerz_y5" role="3cpWs9">
                    <property role="TrG5h" value="start" />
                    <node concept="3uibUv" id="2OB0Aerz_y6" role="1tU5fm">
                      <ref role="3uigEE" to="j4ye:~DateTime" resolve="DateTime" />
                    </node>
                    <node concept="2OqwBi" id="2OB0AerzCbQ" role="33vP2m">
                      <node concept="37vLTw" id="2OB0AerzC52" role="2Oq$k0">
                        <ref role="3cqZAo" node="2OB0Aerzh1Q" resolve="sch" />
                      </node>
                      <node concept="liA8E" id="2OB0AerzDF3" role="2OqNvi">
                        <ref role="37wK5l" to="spdm:~Schedule.getFreePeriod(com.mindfusion.common.DateTime,com.mindfusion.common.DateTime,com.mindfusion.common.Duration):com.mindfusion.common.DateTime" resolve="getFreePeriod" />
                        <node concept="37vLTw" id="2OB0AerzEWN" role="37wK5m">
                          <ref role="3cqZAo" node="2OB0AersVd6" resolve="dateVal" />
                        </node>
                        <node concept="37vLTw" id="2OB0AerzHfw" role="37wK5m">
                          <ref role="3cqZAo" node="2OB0AeryDCV" resolve="newDateVal" />
                        </node>
                        <node concept="37vLTw" id="5t_d$dJJNBL" role="37wK5m">
                          <ref role="3cqZAo" node="2OB0Aer$K$v" resolve="duran" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="2OB0AerzLwk" role="3cqZAp">
                  <node concept="3cpWsn" id="2OB0AerzLwl" role="3cpWs9">
                    <property role="TrG5h" value="end" />
                    <node concept="3uibUv" id="2OB0AerzLwm" role="1tU5fm">
                      <ref role="3uigEE" to="j4ye:~DateTime" resolve="DateTime" />
                    </node>
                    <node concept="2OqwBi" id="2OB0Aer$6MR" role="33vP2m">
                      <node concept="37vLTw" id="2OB0Aer$60h" role="2Oq$k0">
                        <ref role="3cqZAo" node="2OB0Aerz_y5" resolve="start" />
                      </node>
                      <node concept="liA8E" id="2OB0Aer$mAr" role="2OqNvi">
                        <ref role="37wK5l" to="j4ye:~DateTime.addHours(double):com.mindfusion.common.DateTime" resolve="addHours" />
                        <node concept="2OqwBi" id="2OB0Aer$pNP" role="37wK5m">
                          <node concept="37vLTw" id="2OB0Aer$nL$" role="2Oq$k0">
                            <ref role="3cqZAo" node="2OB0Aerjhw4" resolve="superList" />
                          </node>
                          <node concept="liA8E" id="2OB0Aer$B8y" role="2OqNvi">
                            <ref role="37wK5l" to="33ny:~ArrayList.get(int):java.lang.Object" resolve="get" />
                            <node concept="37vLTw" id="2OB0Aer$CJ0" role="37wK5m">
                              <ref role="3cqZAo" node="2OB0Aert8xY" resolve="i" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="2OB0Aer_bEI" role="3cqZAp">
                  <node concept="3cpWsn" id="2OB0Aer_bEJ" role="3cpWs9">
                    <property role="TrG5h" value="app" />
                    <node concept="3uibUv" id="2OB0Aer_bEK" role="1tU5fm">
                      <ref role="3uigEE" to="spdm:~Appointment" resolve="Appointment" />
                    </node>
                    <node concept="2ShNRf" id="2OB0Aer_c_8" role="33vP2m">
                      <node concept="1pGfFk" id="2OB0Aer_e$v" role="2ShVmc">
                        <ref role="37wK5l" to="spdm:~Appointment.&lt;init&gt;()" resolve="Appointment" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_fv9" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0Aer_gnT" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_fv7" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_bEJ" resolve="app" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer_hdi" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Appointment.setHeaderText(java.lang.String):void" resolve="setHeaderText" />
                      <node concept="3cpWs3" id="2OB0Aer_mPH" role="37wK5m">
                        <node concept="AH0OO" id="2OB0Aer_onZ" role="3uHU7w">
                          <node concept="3cmrfG" id="2OB0Aer_p5r" role="AHEQo">
                            <property role="3cmrfH" value="4" />
                          </node>
                          <node concept="37vLTw" id="2OB0Aer_nzD" role="AHHXb">
                            <ref role="3cqZAo" node="2OB0Aer8XSv" resolve="spl" />
                          </node>
                        </node>
                        <node concept="3cpWs3" id="2OB0Aer_ln7" role="3uHU7B">
                          <node concept="AH0OO" id="2OB0Aer_jj7" role="3uHU7B">
                            <node concept="3cmrfG" id="2OB0Aer_jVS" role="AHEQo">
                              <property role="3cmrfH" value="0" />
                            </node>
                            <node concept="37vLTw" id="2OB0Aer_isZ" role="AHHXb">
                              <ref role="3cqZAo" node="2OB0Aer8XSv" resolve="spl" />
                            </node>
                          </node>
                          <node concept="Xl_RD" id="2OB0Aer_lZX" role="3uHU7w">
                            <property role="Xl_RC" value=":" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_q7P" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0Aer_rbz" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_q7N" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_bEJ" resolve="app" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer_rWV" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Appointment.setDescriptionText(java.lang.String):void" resolve="setDescriptionText" />
                      <node concept="AH0OO" id="2OB0Aer_twB" role="37wK5m">
                        <node concept="3cmrfG" id="2OB0Aer_u8X" role="AHEQo">
                          <property role="3cmrfH" value="1" />
                        </node>
                        <node concept="37vLTw" id="2OB0Aer_sEf" role="AHHXb">
                          <ref role="3cqZAo" node="2OB0Aer8XSv" resolve="spl" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_v7D" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0Aer_wMq" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_w2m" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_bEJ" resolve="app" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer_xzn" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Appointment.setStartTime(com.mindfusion.common.DateTime):void" resolve="setStartTime" />
                      <node concept="37vLTw" id="2OB0Aer_yhl" role="37wK5m">
                        <ref role="3cqZAo" node="2OB0Aerz_y5" resolve="start" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_zjk" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0Aer_$pu" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_zji" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_bEJ" resolve="app" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer__aQ" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Appointment.setEndTime(com.mindfusion.common.DateTime):void" resolve="setEndTime" />
                      <node concept="37vLTw" id="2OB0Aer__Tu" role="37wK5m">
                        <ref role="3cqZAo" node="2OB0AerzLwl" resolve="end" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="2OB0Aer_Dwf" role="3cqZAp">
                  <node concept="3cpWsn" id="2OB0Aer_Dwg" role="3cpWs9">
                    <property role="TrG5h" value="style" />
                    <node concept="3uibUv" id="2OB0Aer_Dwh" role="1tU5fm">
                      <ref role="3uigEE" to="spdm:~Style" resolve="Style" />
                    </node>
                    <node concept="2OqwBi" id="2OB0Aer_GVB" role="33vP2m">
                      <node concept="37vLTw" id="2OB0Aer_GbY" role="2Oq$k0">
                        <ref role="3cqZAo" node="2OB0Aer_bEJ" resolve="app" />
                      </node>
                      <node concept="liA8E" id="2OB0Aer_HGV" role="2OqNvi">
                        <ref role="37wK5l" to="spdm:~Appointment.getStyle():com.mindfusion.scheduling.model.Style" resolve="getStyle" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_IR0" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0Aer_JTI" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_IQY" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer_L5j" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setBorderTopColor(java.awt.Color):void" resolve="setBorderTopColor" />
                      <node concept="10M0yZ" id="2OB0Aer_Mr1" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.Gold" resolve="Gold" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_Nsu" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0Aer_OnT" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_Nss" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer_PE2" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setBorderLeftColor(java.awt.Color):void" resolve="setBorderLeftColor" />
                      <node concept="10M0yZ" id="2OB0Aer_Qs_" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.Gold" resolve="Gold" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_RuH" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0Aer_Sqw" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_RuF" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer_TH2" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setBorderRightColor(java.awt.Color):void" resolve="setBorderRightColor" />
                      <node concept="10M0yZ" id="2OB0Aer_UvU" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.Gold" resolve="Gold" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_VyJ" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0Aer_WBt" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_VyH" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0Aer_XWR" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setBorderBottomColor(java.awt.Color):void" resolve="setBorderBottomColor" />
                      <node concept="10M0yZ" id="2OB0Aer_YK4" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.Gold" resolve="Gold" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0Aer_ZNA" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0AerA0K9" role="3clFbG">
                    <node concept="37vLTw" id="2OB0Aer_ZN$" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0AerA23h" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setLineColor(java.awt.Color):void" resolve="setLineColor" />
                      <node concept="10M0yZ" id="2OB0AerA2QN" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.Gold" resolve="Gold" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0AerA3V2" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0AerA4RX" role="3clFbG">
                    <node concept="37vLTw" id="2OB0AerA3V0" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0AerA6bP" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setFillColor(java.awt.Color):void" resolve="setFillColor" />
                      <node concept="10M0yZ" id="2OB0AerA6ZK" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.DarkRed" resolve="DarkRed" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0AerA84I" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0AerA921" role="3clFbG">
                    <node concept="37vLTw" id="2OB0AerA84G" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0AerAalN" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setBrush(com.mindfusion.drawing.Brush):void" resolve="setBrush" />
                      <node concept="2ShNRf" id="2OB0AerAb6G" role="37wK5m">
                        <node concept="1pGfFk" id="2OB0AerAd8I" role="2ShVmc">
                          <ref role="37wK5l" to="tr4j:~GradientBrush.&lt;init&gt;(java.awt.Color,java.awt.Color,int)" resolve="GradientBrush" />
                          <node concept="10M0yZ" id="2OB0AerAfkc" role="37wK5m">
                            <ref role="3cqZAo" to="tr4j:~Colors.White" resolve="White" />
                            <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                          </node>
                          <node concept="10M0yZ" id="2OB0AerAg1X" role="37wK5m">
                            <ref role="3cqZAo" to="tr4j:~Colors.LightGreen" resolve="LightGreen" />
                            <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                          </node>
                          <node concept="3cmrfG" id="2OB0AerAgHe" role="37wK5m">
                            <property role="3cmrfH" value="90" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0AerAioc" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0AerAk74" role="3clFbG">
                    <node concept="37vLTw" id="2OB0AerAjsf" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0AerAliD" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setHeaderTextColor(java.awt.Color):void" resolve="setHeaderTextColor" />
                      <node concept="10M0yZ" id="2OB0AerAm7a" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.DarkRed" resolve="DarkRed" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0AerAngb" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0AerAoqi" role="3clFbG">
                    <node concept="37vLTw" id="2OB0AerAng9" role="2Oq$k0">
                      <ref role="3cqZAo" node="2OB0Aer_Dwg" resolve="style" />
                    </node>
                    <node concept="liA8E" id="2OB0AerAp_s" role="2OqNvi">
                      <ref role="37wK5l" to="spdm:~Style.setTextColor(java.awt.Color):void" resolve="setTextColor" />
                      <node concept="10M0yZ" id="2OB0AerAqqi" role="37wK5m">
                        <ref role="3cqZAo" to="tr4j:~Colors.DarkRed" resolve="DarkRed" />
                        <ref role="1PxDUh" to="tr4j:~Colors" resolve="Colors" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2OB0AerAs73" role="3cqZAp">
                  <node concept="2OqwBi" id="2OB0AerA_0X" role="3clFbG">
                    <node concept="2OqwBi" id="2OB0AerAyaj" role="2Oq$k0">
                      <node concept="2OqwBi" id="2OB0AerAtVB" role="2Oq$k0">
                        <node concept="37vLTw" id="2OB0AerAs71" role="2Oq$k0">
                          <ref role="3cqZAo" node="2OB0AerrXhs" resolve="calendar" />
                        </node>
                        <node concept="liA8E" id="2OB0AerAxeB" role="2OqNvi">
                          <ref role="37wK5l" to="1gjj:~Calendar.getSchedule():com.mindfusion.scheduling.model.Schedule" resolve="getSchedule" />
                        </node>
                      </node>
                      <node concept="liA8E" id="2OB0AerAz$S" role="2OqNvi">
                        <ref role="37wK5l" to="spdm:~Schedule.getItems():com.mindfusion.scheduling.model.ItemList" resolve="getItems" />
                      </node>
                    </node>
                    <node concept="liA8E" id="2OB0AerAFqi" role="2OqNvi">
                      <ref role="37wK5l" to="j4ye:~BaseList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="2OB0AerAGoK" role="37wK5m">
                        <ref role="3cqZAo" node="2OB0Aer_bEJ" resolve="app" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3y3z36" id="2OB0Aery_g0" role="3clFbw">
                <node concept="3cmrfG" id="2OB0AeryAJy" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="2OB0Aeryh8y" role="3uHU7B">
                  <node concept="37vLTw" id="2OB0AeryflB" role="2Oq$k0">
                    <ref role="3cqZAo" node="2OB0Aerjhw4" resolve="superList" />
                  </node>
                  <node concept="liA8E" id="2OB0Aeryus1" role="2OqNvi">
                    <ref role="37wK5l" to="33ny:~ArrayList.get(int):java.lang.Object" resolve="get" />
                    <node concept="37vLTw" id="2OB0Aeryw2I" role="37wK5m">
                      <ref role="3cqZAo" node="2OB0Aert8xY" resolve="i" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="9aQIb" id="2OB0AerAHgV" role="9aQIa">
                <node concept="3clFbS" id="2OB0AerAHgW" role="9aQI4" />
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="2OB0Aert8xY" role="1Duv9x">
            <property role="TrG5h" value="i" />
            <node concept="10Oyi0" id="2OB0Aertagg" role="1tU5fm" />
            <node concept="3cmrfG" id="2OB0Aertb5m" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
          <node concept="3eOVzh" id="2OB0Aertd7D" role="1Dwp0S">
            <node concept="37vLTw" id="2OB0AertfDk" role="3uHU7w">
              <ref role="3cqZAo" node="2OB0AereyvE" resolve="remainingDays" />
            </node>
            <node concept="37vLTw" id="2OB0AertbEt" role="3uHU7B">
              <ref role="3cqZAo" node="2OB0Aert8xY" resolve="i" />
            </node>
          </node>
          <node concept="3uNrnE" id="2OB0Aerth_1" role="1Dwrff">
            <node concept="37vLTw" id="2OB0Aerth_3" role="2$L3a6">
              <ref role="3cqZAo" node="2OB0Aert8xY" resolve="i" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2OB0Aer8jjx" role="1B3o_S" />
      <node concept="3cqZAl" id="2OB0Aer8vOw" role="3clF45" />
      <node concept="37vLTG" id="2OB0Aer8DZK" role="3clF46">
        <property role="TrG5h" value="details" />
        <node concept="3uibUv" id="2OB0Aer8DZJ" role="1tU5fm">
          <ref role="3uigEE" to="wyt6:~String" resolve="String" />
        </node>
      </node>
      <node concept="3uibUv" id="2OB0Aer8NMU" role="Sfmx6">
        <ref role="3uigEE" to="25x5:~ParseException" resolve="ParseException" />
      </node>
    </node>
    <node concept="2tJIrI" id="2OB0AerfeNh" role="jymVt" />
    <node concept="2YIFZL" id="2OB0AerfGcM" role="jymVt">
      <property role="TrG5h" value="computeRepartitionNumber" />
      <node concept="3clFbS" id="2OB0AerfGcP" role="3clF47">
        <node concept="3cpWs8" id="2OB0AerfXgb" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AerfXgc" role="3cpWs9">
            <property role="TrG5h" value="unitary" />
            <node concept="3uibUv" id="2OB0AerfXgd" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
              <node concept="3uibUv" id="2OB0AerfYZt" role="11_B2D">
                <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
              </node>
            </node>
            <node concept="2ShNRf" id="2OB0AerfZLt" role="33vP2m">
              <node concept="1pGfFk" id="2OB0Aerg1JC" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
                <node concept="3uibUv" id="2OB0Aerg3dC" role="1pMfVU">
                  <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AerhqTP" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AerhqTS" role="3cpWs9">
            <property role="TrG5h" value="perdayWork" />
            <node concept="10Oyi0" id="2OB0AerhqTN" role="1tU5fm" />
            <node concept="3cmrfG" id="2OB0Aerhs9P" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2OB0AerhaPH" role="3cqZAp">
          <node concept="3cpWsn" id="2OB0AerhaPK" role="3cpWs9">
            <property role="TrG5h" value="sum" />
            <node concept="10Oyi0" id="2OB0AerhaPF" role="1tU5fm" />
            <node concept="3cmrfG" id="2OB0Aerhc21" role="33vP2m">
              <property role="3cmrfH" value="0" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2OB0AerhdJS" role="3cqZAp">
          <node concept="3clFbS" id="2OB0AerhdJU" role="3clFbx">
            <node concept="3clFbF" id="2OB0AerhsHO" role="3cqZAp">
              <node concept="37vLTI" id="2OB0Aerhu1F" role="3clFbG">
                <node concept="FJ1c_" id="2OB0AerhwRo" role="37vLTx">
                  <node concept="37vLTw" id="2OB0Aerhyu0" role="3uHU7w">
                    <ref role="3cqZAo" node="2OB0AerfQ7T" resolve="remainingDays" />
                  </node>
                  <node concept="37vLTw" id="2OB0Aerhv9e" role="3uHU7B">
                    <ref role="3cqZAo" node="2OB0AerfH0p" resolve="spendHours" />
                  </node>
                </node>
                <node concept="37vLTw" id="2OB0AerhsHM" role="37vLTJ">
                  <ref role="3cqZAo" node="2OB0AerhqTS" resolve="perdayWork" />
                </node>
              </node>
            </node>
            <node concept="1Dw8fO" id="2OB0Aerh$bq" role="3cqZAp">
              <node concept="3clFbS" id="2OB0Aerh$bs" role="2LFqv$">
                <node concept="3clFbJ" id="2OB0AerhHAS" role="3cqZAp">
                  <node concept="3clFbS" id="2OB0AerhHAU" role="3clFbx">
                    <node concept="3clFbF" id="2OB0Aeri3uh" role="3cqZAp">
                      <node concept="2OqwBi" id="2OB0Aeri50N" role="3clFbG">
                        <node concept="37vLTw" id="2OB0Aeri3uf" role="2Oq$k0">
                          <ref role="3cqZAo" node="2OB0AerfXgc" resolve="unitary" />
                        </node>
                        <node concept="liA8E" id="2OB0Aeri85u" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="3cpWsd" id="2OB0AeribTU" role="37wK5m">
                            <node concept="37vLTw" id="2OB0Aerict4" role="3uHU7w">
                              <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                            </node>
                            <node concept="37vLTw" id="2OB0Aeria08" role="3uHU7B">
                              <ref role="3cqZAo" node="2OB0AerfH0p" resolve="spendHours" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="2OB0AeridRm" role="3cqZAp">
                      <node concept="37vLTI" id="2OB0Aerig0o" role="3clFbG">
                        <node concept="3cpWs3" id="2OB0AerihSq" role="37vLTx">
                          <node concept="1eOMI4" id="2OB0AeriisW" role="3uHU7w">
                            <node concept="3cpWsd" id="2OB0Aerimk6" role="1eOMHV">
                              <node concept="37vLTw" id="2OB0AerimRg" role="3uHU7w">
                                <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                              </node>
                              <node concept="37vLTw" id="2OB0AerikmZ" role="3uHU7B">
                                <ref role="3cqZAo" node="2OB0AerfH0p" resolve="spendHours" />
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTw" id="2OB0Aerig$s" role="3uHU7B">
                            <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                          </node>
                        </node>
                        <node concept="37vLTw" id="2OB0AeridRk" role="37vLTJ">
                          <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3eOSWO" id="2OB0Aeri0hP" role="3clFbw">
                    <node concept="37vLTw" id="2OB0Aeri1CM" role="3uHU7w">
                      <ref role="3cqZAo" node="2OB0AerfH0p" resolve="spendHours" />
                    </node>
                    <node concept="1eOMI4" id="2OB0AerhKpo" role="3uHU7B">
                      <node concept="3cpWs3" id="2OB0AerhP2O" role="1eOMHV">
                        <node concept="3cmrfG" id="2OB0AerhP_U" role="3uHU7w">
                          <property role="3cmrfH" value="1" />
                        </node>
                        <node concept="3cpWs3" id="2OB0AerhMkI" role="3uHU7B">
                          <node concept="37vLTw" id="2OB0AerhKXi" role="3uHU7B">
                            <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                          </node>
                          <node concept="37vLTw" id="2OB0AerhNsj" role="3uHU7w">
                            <ref role="3cqZAo" node="2OB0AerhqTS" resolve="perdayWork" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="9aQIb" id="2OB0AerioUm" role="9aQIa">
                    <node concept="3clFbS" id="2OB0AerioUn" role="9aQI4">
                      <node concept="3clFbF" id="2OB0Aeriqk6" role="3cqZAp">
                        <node concept="2OqwBi" id="2OB0AerirQd" role="3clFbG">
                          <node concept="37vLTw" id="2OB0Aeriqk5" role="2Oq$k0">
                            <ref role="3cqZAo" node="2OB0AerfXgc" resolve="unitary" />
                          </node>
                          <node concept="liA8E" id="2OB0Aeriwp2" role="2OqNvi">
                            <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                            <node concept="3cpWs3" id="2OB0AerizPo" role="37wK5m">
                              <node concept="3cmrfG" id="2OB0Aeri$oX" role="3uHU7w">
                                <property role="3cmrfH" value="1" />
                              </node>
                              <node concept="37vLTw" id="2OB0AeriylC" role="3uHU7B">
                                <ref role="3cqZAo" node="2OB0AerhqTS" resolve="perdayWork" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbF" id="2OB0Aeri_Cm" role="3cqZAp">
                        <node concept="37vLTI" id="2OB0AeriAVx" role="3clFbG">
                          <node concept="3cpWs3" id="2OB0AeriG1p" role="37vLTx">
                            <node concept="3cmrfG" id="2OB0AeriG$C" role="3uHU7w">
                              <property role="3cmrfH" value="1" />
                            </node>
                            <node concept="3cpWs3" id="2OB0AeriD68" role="3uHU7B">
                              <node concept="37vLTw" id="2OB0AeriBJo" role="3uHU7B">
                                <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                              </node>
                              <node concept="37vLTw" id="2OB0AeriEdi" role="3uHU7w">
                                <ref role="3cqZAo" node="2OB0AerhqTS" resolve="perdayWork" />
                              </node>
                            </node>
                          </node>
                          <node concept="37vLTw" id="2OB0Aeri_Ck" role="37vLTJ">
                            <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWsn" id="2OB0Aerh$bt" role="1Duv9x">
                <property role="TrG5h" value="i" />
                <node concept="10Oyi0" id="2OB0Aerh$nZ" role="1tU5fm" />
                <node concept="3cmrfG" id="2OB0Aerh_95" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
              <node concept="3eOVzh" id="2OB0AerhDM6" role="1Dwp0S">
                <node concept="37vLTw" id="2OB0AerhF0S" role="3uHU7w">
                  <ref role="3cqZAo" node="2OB0AerfQ7T" resolve="remainingDays" />
                </node>
                <node concept="37vLTw" id="2OB0AerhCVY" role="3uHU7B">
                  <ref role="3cqZAo" node="2OB0Aerh$bt" resolve="i" />
                </node>
              </node>
              <node concept="3uNrnE" id="2OB0AerhGWe" role="1Dwrff">
                <node concept="37vLTw" id="2OB0AerhGWg" role="2$L3a6">
                  <ref role="3cqZAo" node="2OB0Aerh$bt" resolve="i" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3fqX7Q" id="2OB0AerheU1" role="3clFbw">
            <node concept="1eOMI4" id="2OB0AerheU3" role="3fr31v">
              <node concept="3clFbC" id="2OB0AerhltD" role="1eOMHV">
                <node concept="3cmrfG" id="2OB0Aerhma7" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2dk9JS" id="2OB0AerhhSa" role="3uHU7B">
                  <node concept="37vLTw" id="2OB0Aerhg1d" role="3uHU7B">
                    <ref role="3cqZAo" node="2OB0AerfH0p" resolve="spendHours" />
                  </node>
                  <node concept="37vLTw" id="2OB0Aerhjuq" role="3uHU7w">
                    <ref role="3cqZAo" node="2OB0AerfQ7T" resolve="remainingDays" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="2OB0AeriIBz" role="9aQIa">
            <node concept="3clFbS" id="2OB0AeriIB$" role="9aQI4">
              <node concept="3clFbF" id="2OB0AeriKl2" role="3cqZAp">
                <node concept="37vLTI" id="2OB0AeriMjK" role="3clFbG">
                  <node concept="FJ1c_" id="2OB0AeriP9z" role="37vLTx">
                    <node concept="37vLTw" id="2OB0AeriQKe" role="3uHU7w">
                      <ref role="3cqZAo" node="2OB0AerfQ7T" resolve="remainingDays" />
                    </node>
                    <node concept="37vLTw" id="2OB0AeriNrm" role="3uHU7B">
                      <ref role="3cqZAo" node="2OB0AerfH0p" resolve="spendHours" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="2OB0AeriKl1" role="37vLTJ">
                    <ref role="3cqZAo" node="2OB0AerhqTS" resolve="perdayWork" />
                  </node>
                </node>
              </node>
              <node concept="1Dw8fO" id="2OB0AeriStF" role="3cqZAp">
                <node concept="3clFbS" id="2OB0AeriStH" role="2LFqv$">
                  <node concept="3clFbF" id="2OB0Aerj0$c" role="3cqZAp">
                    <node concept="2OqwBi" id="2OB0Aerj26m" role="3clFbG">
                      <node concept="37vLTw" id="2OB0Aerj0$a" role="2Oq$k0">
                        <ref role="3cqZAo" node="2OB0AerfXgc" resolve="unitary" />
                      </node>
                      <node concept="liA8E" id="2OB0Aerj6oO" role="2OqNvi">
                        <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                        <node concept="37vLTw" id="2OB0Aerj8nm" role="37wK5m">
                          <ref role="3cqZAo" node="2OB0AerhqTS" resolve="perdayWork" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="2OB0Aerj97Y" role="3cqZAp">
                    <node concept="37vLTI" id="2OB0Aerjavi" role="3clFbG">
                      <node concept="3cpWs3" id="2OB0Aerjcnq" role="37vLTx">
                        <node concept="37vLTw" id="2OB0AerjduB" role="3uHU7w">
                          <ref role="3cqZAo" node="2OB0AerhqTS" resolve="perdayWork" />
                        </node>
                        <node concept="37vLTw" id="2OB0Aerjb2Y" role="3uHU7B">
                          <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="2OB0Aerj97W" role="37vLTJ">
                        <ref role="3cqZAo" node="2OB0AerhaPK" resolve="sum" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWsn" id="2OB0AeriStI" role="1Duv9x">
                  <property role="TrG5h" value="i" />
                  <node concept="10Oyi0" id="2OB0AeriSEg" role="1tU5fm" />
                  <node concept="3cmrfG" id="2OB0AeriTrm" role="33vP2m">
                    <property role="3cmrfH" value="0" />
                  </node>
                </node>
                <node concept="3eOVzh" id="2OB0AeriWk0" role="1Dwp0S">
                  <node concept="37vLTw" id="2OB0AeriXr5" role="3uHU7w">
                    <ref role="3cqZAo" node="2OB0AerfQ7T" resolve="remainingDays" />
                  </node>
                  <node concept="37vLTw" id="2OB0AeriTZg" role="3uHU7B">
                    <ref role="3cqZAo" node="2OB0AeriStI" resolve="i" />
                  </node>
                </node>
                <node concept="3uNrnE" id="2OB0AeriZmu" role="1Dwrff">
                  <node concept="37vLTw" id="2OB0AeriZmw" role="2$L3a6">
                    <ref role="3cqZAo" node="2OB0AeriStI" resolve="i" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="2OB0Aerg3WH" role="3cqZAp">
          <node concept="37vLTw" id="2OB0Aerg53P" role="3cqZAk">
            <ref role="3cqZAo" node="2OB0AerfXgc" resolve="unitary" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="2OB0AerftCH" role="1B3o_S" />
      <node concept="3uibUv" id="2OB0AerfF3e" role="3clF45">
        <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
        <node concept="3uibUv" id="2OB0AerfG2s" role="11_B2D">
          <ref role="3uigEE" to="wyt6:~Integer" resolve="Integer" />
        </node>
      </node>
      <node concept="37vLTG" id="2OB0AerfH0p" role="3clF46">
        <property role="TrG5h" value="spendHours" />
        <node concept="10Oyi0" id="2OB0AerfH0o" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="2OB0AerfQ7T" role="3clF46">
        <property role="TrG5h" value="remainingDays" />
        <node concept="10Oyi0" id="2OB0AerfQkr" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="2lFfnZ_yjA9" role="jymVt" />
    <node concept="3clFb_" id="2lFfnZ_yACB" role="jymVt">
      <property role="TrG5h" value="Difficulty" />
      <node concept="3clFbS" id="2lFfnZ_yACE" role="3clF47">
        <node concept="3cpWs8" id="2lFfnZ_zcU1" role="3cqZAp">
          <node concept="3cpWsn" id="2lFfnZ_zcU4" role="3cpWs9">
            <property role="TrG5h" value="returnString" />
            <node concept="17QB3L" id="2lFfnZ_zcTZ" role="1tU5fm" />
            <node concept="Xl_RD" id="2lFfnZ_zjgG" role="33vP2m">
              <property role="Xl_RC" value="Low" />
            </node>
          </node>
        </node>
        <node concept="3KaCP$" id="2lFfnZ_yOtI" role="3cqZAp">
          <node concept="3KbdKl" id="2lFfnZ_yQlE" role="3KbHQx">
            <node concept="3cmrfG" id="2lFfnZ_yQTY" role="3Kbmr1">
              <property role="3cmrfH" value="1" />
            </node>
            <node concept="3clFbS" id="2lFfnZ_yQlG" role="3Kbo56">
              <node concept="3clFbF" id="2lFfnZ_zoXd" role="3cqZAp">
                <node concept="37vLTI" id="2lFfnZ_zuEF" role="3clFbG">
                  <node concept="Xl_RD" id="2lFfnZ_zwcN" role="37vLTx">
                    <property role="Xl_RC" value="Low" />
                  </node>
                  <node concept="37vLTw" id="2lFfnZ_zoXc" role="37vLTJ">
                    <ref role="3cqZAo" node="2lFfnZ_zcU4" resolve="returnString" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="2lFfnZ_$PEn" role="3cqZAp" />
            </node>
          </node>
          <node concept="3KbdKl" id="2lFfnZ_zweV" role="3KbHQx">
            <node concept="3cmrfG" id="2lFfnZ_z$Xr" role="3Kbmr1">
              <property role="3cmrfH" value="2" />
            </node>
            <node concept="3clFbS" id="2lFfnZ_zweX" role="3Kbo56">
              <node concept="3clFbF" id="2lFfnZ_zB8Z" role="3cqZAp">
                <node concept="37vLTI" id="2lFfnZ_zC3U" role="3clFbG">
                  <node concept="Xl_RD" id="2lFfnZ_zCC9" role="37vLTx">
                    <property role="Xl_RC" value="Medium" />
                  </node>
                  <node concept="37vLTw" id="2lFfnZ_zB8Y" role="37vLTJ">
                    <ref role="3cqZAo" node="2lFfnZ_zcU4" resolve="returnString" />
                  </node>
                </node>
              </node>
              <node concept="3zACq4" id="2lFfnZ_$WJ6" role="3cqZAp" />
            </node>
          </node>
          <node concept="3KbdKl" id="2lFfnZ_$9vi" role="3KbHQx">
            <node concept="3clFbS" id="2lFfnZ_$9vk" role="3Kbo56">
              <node concept="3clFbF" id="2lFfnZ_$gVU" role="3cqZAp">
                <node concept="37vLTI" id="2lFfnZ_$ivT" role="3clFbG">
                  <node concept="Xl_RD" id="2lFfnZ_$j4a" role="37vLTx">
                    <property role="Xl_RC" value="High" />
                  </node>
                  <node concept="37vLTw" id="2lFfnZ_$gVT" role="37vLTJ">
                    <ref role="3cqZAo" node="2lFfnZ_zcU4" resolve="returnString" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cmrfG" id="2lFfnZ_$gny" role="3Kbmr1">
              <property role="3cmrfH" value="3" />
            </node>
          </node>
          <node concept="37vLTw" id="2lFfnZ_yP_l" role="3KbGdf">
            <ref role="3cqZAo" node="2lFfnZ_yGN2" resolve="difficult" />
          </node>
        </node>
        <node concept="3cpWs6" id="2lFfnZ_zT5x" role="3cqZAp">
          <node concept="37vLTw" id="2lFfnZ_$3F1" role="3cqZAk">
            <ref role="3cqZAo" node="2lFfnZ_zcU4" resolve="returnString" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="2lFfnZ_yvHd" role="1B3o_S" />
      <node concept="17QB3L" id="2lFfnZ_y_Qv" role="3clF45" />
      <node concept="37vLTG" id="2lFfnZ_yGN2" role="3clF46">
        <property role="TrG5h" value="difficult" />
        <node concept="10Oyi0" id="2lFfnZ_yGN1" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="2lFfnZ_D9lT" role="jymVt" />
    <node concept="3clFb_" id="2lFfnZ_D$4P" role="jymVt">
      <property role="TrG5h" value="formatTime" />
      <node concept="3clFbS" id="2lFfnZ_D$4S" role="3clF47">
        <node concept="3cpWs8" id="2lFfnZ_Eacm" role="3cqZAp">
          <node concept="3cpWsn" id="2lFfnZ_Eacp" role="3cpWs9">
            <property role="TrG5h" value="retString" />
            <node concept="17QB3L" id="2lFfnZ_Eacl" role="1tU5fm" />
            <node concept="Xl_RD" id="2lFfnZ_EaO5" role="33vP2m">
              <property role="Xl_RC" value="" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2lFfnZ_Eu$S" role="3cqZAp">
          <node concept="37vLTI" id="2lFfnZ_Evvp" role="3clFbG">
            <node concept="3cpWs3" id="2lFfnZ_EHaJ" role="37vLTx">
              <node concept="2YIFZM" id="2lFfnZ_ExJZ" role="3uHU7B">
                <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
                <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                <node concept="Xl_RD" id="2lFfnZ_EzrX" role="37wK5m">
                  <property role="Xl_RC" value="%02d" />
                </node>
                <node concept="2YIFZM" id="3rIOsIrk6D$" role="37wK5m">
                  <ref role="37wK5l" to="wyt6:~Math.abs(int):int" resolve="abs" />
                  <ref role="1Pybhc" to="wyt6:~Math" resolve="Math" />
                  <node concept="37vLTw" id="3rIOsIrk7yn" role="37wK5m">
                    <ref role="3cqZAo" node="2lFfnZ_DEqn" resolve="hour" />
                  </node>
                </node>
              </node>
              <node concept="2YIFZM" id="2lFfnZ_EPkg" role="3uHU7w">
                <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
                <ref role="37wK5l" to="wyt6:~String.format(java.lang.String,java.lang.Object...):java.lang.String" resolve="format" />
                <node concept="Xl_RD" id="2lFfnZ_ERg7" role="37wK5m">
                  <property role="Xl_RC" value="%02d" />
                </node>
                <node concept="37vLTw" id="2lFfnZ_ETeW" role="37wK5m">
                  <ref role="3cqZAo" node="2lFfnZ_DLTZ" resolve="min" />
                </node>
              </node>
            </node>
            <node concept="37vLTw" id="2lFfnZ_Eu$Q" role="37vLTJ">
              <ref role="3cqZAo" node="2lFfnZ_Eacp" resolve="retString" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="2lFfnZ_EUUp" role="3cqZAp">
          <node concept="37vLTw" id="2lFfnZ_F02x" role="3cqZAk">
            <ref role="3cqZAo" node="2lFfnZ_Eacp" resolve="retString" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="2lFfnZ_Dg3W" role="1B3o_S" />
      <node concept="17QB3L" id="2lFfnZ_DzUU" role="3clF45" />
      <node concept="37vLTG" id="2lFfnZ_DEqn" role="3clF46">
        <property role="TrG5h" value="hour" />
        <node concept="10Oyi0" id="2lFfnZ_DEqm" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="2lFfnZ_DLTZ" role="3clF46">
        <property role="TrG5h" value="min" />
        <node concept="10Oyi0" id="2lFfnZ_DRID" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="2lFfnZ__UAM" role="jymVt" />
    <node concept="3clFb_" id="2lFfnZ_qj4z" role="jymVt">
      <property role="TrG5h" value="prepareSchedule" />
      <node concept="3clFbS" id="2lFfnZ_qj4A" role="3clF47">
        <node concept="3cpWs8" id="3rIOsIryxxP" role="3cqZAp">
          <node concept="3cpWsn" id="3rIOsIryxxQ" role="3cpWs9">
            <property role="TrG5h" value="tempArray" />
            <node concept="3uibUv" id="3rIOsIryxxN" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
              <node concept="17QB3L" id="3rIOsIryzCr" role="11_B2D" />
            </node>
            <node concept="2ShNRf" id="3rIOsIryzNI" role="33vP2m">
              <node concept="1pGfFk" id="3rIOsIry$ng" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
                <node concept="17QB3L" id="3rIOsIry_8F" role="1pMfVU" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="2lFfnZ_qDDM" role="3cqZAp">
          <node concept="3cpWsn" id="2lFfnZ_qDDP" role="3cpWs9">
            <property role="TrG5h" value="tempString" />
            <node concept="17QB3L" id="2lFfnZ_qDDK" role="1tU5fm" />
            <node concept="Xl_RD" id="2lFfnZ_qED8" role="33vP2m" />
          </node>
        </node>
        <node concept="3cpWs8" id="2lFfnZ_XiMg" role="3cqZAp">
          <node concept="3cpWsn" id="2lFfnZ_XiMj" role="3cpWs9">
            <property role="TrG5h" value="courseInfo" />
            <node concept="17QB3L" id="2lFfnZ_XiMe" role="1tU5fm" />
            <node concept="Xl_RD" id="2lFfnZ_Xlvd" role="33vP2m" />
          </node>
        </node>
        <node concept="1DcWWT" id="2lFfnZ_vNt4" role="3cqZAp">
          <node concept="3clFbS" id="2lFfnZ_vNt6" role="2LFqv$">
            <node concept="3clFbF" id="2lFfnZ_vRY7" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ_SHeB" role="3clFbG">
                <node concept="37vLTw" id="2lFfnZ_XmfT" role="37vLTJ">
                  <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                </node>
                <node concept="3cpWs3" id="2lFfnZ_SHeE" role="37vLTx">
                  <node concept="Xl_RD" id="2lFfnZ_SHeF" role="3uHU7w">
                    <property role="Xl_RC" value="," />
                  </node>
                  <node concept="3cpWs3" id="2lFfnZ_SHeG" role="3uHU7B">
                    <node concept="Xl_RD" id="2lFfnZ_SHeH" role="3uHU7B">
                      <property role="Xl_RC" value="Course," />
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ_SHeI" role="3uHU7w">
                      <node concept="37vLTw" id="2lFfnZ_SHeJ" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ_vNt7" resolve="myCourse" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ_SHeK" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$MS_o" resolve="name" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ_ydZx" role="3cqZAp">
              <node concept="d57v9" id="2lFfnZ_yeUY" role="3clFbG">
                <node concept="37vLTw" id="2lFfnZ_Xn5b" role="37vLTJ">
                  <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                </node>
                <node concept="1rXfSq" id="2lFfnZ__jKr" role="37vLTx">
                  <ref role="37wK5l" node="2lFfnZ_yACB" resolve="Difficulty" />
                  <node concept="2OqwBi" id="2lFfnZ__mCo" role="37wK5m">
                    <node concept="37vLTw" id="2lFfnZ__lHx" role="2Oq$k0">
                      <ref role="3cqZAo" node="2lFfnZ_vNt7" resolve="myCourse" />
                    </node>
                    <node concept="2OwXpG" id="2lFfnZ__ncY" role="2OqNvi">
                      <ref role="2Oxat5" node="2lFfnZ$MUJq" resolve="levelOfDifficulty" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1DcWWT" id="2lFfnZ_xXgD" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ_xXgF" role="2LFqv$">
                <node concept="3clFbJ" id="2lFfnZ_FR6J" role="3cqZAp">
                  <node concept="3clFbS" id="2lFfnZ_FR6L" role="3clFbx">
                    <node concept="3clFbF" id="2lFfnZ_XAPy" role="3cqZAp">
                      <node concept="37vLTI" id="2lFfnZ_XBUE" role="3clFbG">
                        <node concept="3cpWs3" id="2lFfnZ_YzMD" role="37vLTx">
                          <node concept="1rXfSq" id="2lFfnZ_XTaz" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZ_XU5N" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_XU5O" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_XU5P" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZ_XU5Q" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_XU5R" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_XU5S" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs3" id="2lFfnZ_XQOM" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZ_XMAg" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZ_XJBb" role="3uHU7B">
                                <node concept="3cpWs3" id="2lFfnZ_XG4o" role="3uHU7B">
                                  <node concept="3cpWs3" id="2lFfnZ_XDq3" role="3uHU7B">
                                    <node concept="37vLTw" id="2lFfnZ_XCuY" role="3uHU7B">
                                      <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                                    </node>
                                    <node concept="Xl_RD" id="2lFfnZ_XFlV" role="3uHU7w">
                                      <property role="Xl_RC" value="," />
                                    </node>
                                  </node>
                                  <node concept="2OqwBi" id="2lFfnZ_XH_f" role="3uHU7w">
                                    <node concept="37vLTw" id="2lFfnZ_XGH3" role="2Oq$k0">
                                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                    </node>
                                    <node concept="2OwXpG" id="2lFfnZ_XIg_" role="2OqNvi">
                                      <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZ_XLs3" role="3uHU7w">
                                  <property role="Xl_RC" value=",Monday," />
                                </node>
                              </node>
                              <node concept="1rXfSq" id="2lFfnZ_XNrR" role="3uHU7w">
                                <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                                <node concept="2OqwBi" id="2lFfnZ_XOle" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_XOlf" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_XP88" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="2lFfnZ_XOlh" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_XOli" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_XPEa" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZ_Y_xG" role="3uHU7w">
                              <property role="Xl_RC" value="," />
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="2lFfnZ_XAPw" role="37vLTJ">
                          <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="3rIOsIryPNB" role="3cqZAp">
                      <node concept="2OqwBi" id="3rIOsIryR5V" role="3clFbG">
                        <node concept="37vLTw" id="3rIOsIryPN_" role="2Oq$k0">
                          <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                        </node>
                        <node concept="liA8E" id="3rIOsIryVuL" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="37vLTw" id="3rIOsIryVL5" role="37wK5m">
                            <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2lFfnZ_FTdW" role="3clFbw">
                    <node concept="37vLTw" id="2lFfnZ_FSoV" role="2Oq$k0">
                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                    </node>
                    <node concept="2OwXpG" id="2lFfnZ_FTP0" role="2OqNvi">
                      <ref role="2Oxat5" node="2lFfnZ$FQX5" resolve="mon" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="2lFfnZ_VYmy" role="3cqZAp">
                  <node concept="3clFbS" id="2lFfnZ_VYm$" role="3clFbx">
                    <node concept="3clFbF" id="2lFfnZ_XVQw" role="3cqZAp">
                      <node concept="37vLTI" id="2lFfnZ_XVQx" role="3clFbG">
                        <node concept="3cpWs3" id="2lFfnZ_XVQy" role="37vLTx">
                          <node concept="1rXfSq" id="2lFfnZ_XVQz" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZ_XVQ$" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_XVQ_" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_XVQA" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZ_XVQB" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_XVQC" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_XVQD" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs3" id="2lFfnZ_YA5_" role="3uHU7B">
                            <node concept="Xl_RD" id="2lFfnZ_YACX" role="3uHU7w">
                              <property role="Xl_RC" value="," />
                            </node>
                            <node concept="3cpWs3" id="2lFfnZ_XVQE" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZ_XVQF" role="3uHU7B">
                                <node concept="3cpWs3" id="2lFfnZ_XVQG" role="3uHU7B">
                                  <node concept="3cpWs3" id="2lFfnZ_XVQH" role="3uHU7B">
                                    <node concept="37vLTw" id="2lFfnZ_XVQI" role="3uHU7B">
                                      <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                                    </node>
                                    <node concept="Xl_RD" id="2lFfnZ_XVQJ" role="3uHU7w">
                                      <property role="Xl_RC" value="," />
                                    </node>
                                  </node>
                                  <node concept="2OqwBi" id="2lFfnZ_XVQK" role="3uHU7w">
                                    <node concept="37vLTw" id="2lFfnZ_XVQL" role="2Oq$k0">
                                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                    </node>
                                    <node concept="2OwXpG" id="2lFfnZ_XVQM" role="2OqNvi">
                                      <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZ_XVQN" role="3uHU7w">
                                  <property role="Xl_RC" value=",Tuesday," />
                                </node>
                              </node>
                              <node concept="1rXfSq" id="2lFfnZ_XVQO" role="3uHU7w">
                                <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                                <node concept="2OqwBi" id="2lFfnZ_XVQP" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_XVQQ" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_XVQR" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="2lFfnZ_XVQS" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_XVQT" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_XVQU" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="2lFfnZ_XVQV" role="37vLTJ">
                          <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="2lFfnZ_XVQW" role="3cqZAp">
                      <node concept="2OqwBi" id="2lFfnZ_XVQX" role="3clFbG">
                        <node concept="37vLTw" id="3rIOsIryWnB" role="2Oq$k0">
                          <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                        </node>
                        <node concept="liA8E" id="2lFfnZ_XVQZ" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="37vLTw" id="2lFfnZ_XVR0" role="37wK5m">
                            <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2lFfnZ_W0yy" role="3clFbw">
                    <node concept="37vLTw" id="2lFfnZ_VZHb" role="2Oq$k0">
                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                    </node>
                    <node concept="2OwXpG" id="2lFfnZ_W196" role="2OqNvi">
                      <ref role="2Oxat5" node="2lFfnZ$FVd1" resolve="tue" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="2lFfnZ_W4dL" role="3cqZAp">
                  <node concept="3clFbS" id="2lFfnZ_W4dN" role="3clFbx">
                    <node concept="3clFbF" id="2lFfnZ_XYjj" role="3cqZAp">
                      <node concept="37vLTI" id="2lFfnZ_XYjk" role="3clFbG">
                        <node concept="3cpWs3" id="2lFfnZ_XYjl" role="37vLTx">
                          <node concept="1rXfSq" id="2lFfnZ_XYjm" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZ_XYjn" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_XYjo" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_XYjp" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZ_XYjq" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_XYjr" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_XYjs" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs3" id="2lFfnZ_YChy" role="3uHU7B">
                            <node concept="Xl_RD" id="2lFfnZ_YCOU" role="3uHU7w">
                              <property role="Xl_RC" value="," />
                            </node>
                            <node concept="3cpWs3" id="2lFfnZ_XYjt" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZ_XYju" role="3uHU7B">
                                <node concept="3cpWs3" id="2lFfnZ_XYjv" role="3uHU7B">
                                  <node concept="3cpWs3" id="2lFfnZ_XYjw" role="3uHU7B">
                                    <node concept="37vLTw" id="2lFfnZ_XYjx" role="3uHU7B">
                                      <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                                    </node>
                                    <node concept="Xl_RD" id="2lFfnZ_XYjy" role="3uHU7w">
                                      <property role="Xl_RC" value="," />
                                    </node>
                                  </node>
                                  <node concept="2OqwBi" id="2lFfnZ_XYjz" role="3uHU7w">
                                    <node concept="37vLTw" id="2lFfnZ_XYj$" role="2Oq$k0">
                                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                    </node>
                                    <node concept="2OwXpG" id="2lFfnZ_XYj_" role="2OqNvi">
                                      <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZ_XYjA" role="3uHU7w">
                                  <property role="Xl_RC" value=",Wednesday," />
                                </node>
                              </node>
                              <node concept="1rXfSq" id="2lFfnZ_XYjB" role="3uHU7w">
                                <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                                <node concept="2OqwBi" id="2lFfnZ_XYjC" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_XYjD" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_XYjE" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="2lFfnZ_XYjF" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_XYjG" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_XYjH" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="2lFfnZ_XYjI" role="37vLTJ">
                          <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="3rIOsIryWyY" role="3cqZAp">
                      <node concept="2OqwBi" id="3rIOsIryWyZ" role="3clFbG">
                        <node concept="37vLTw" id="3rIOsIryWz0" role="2Oq$k0">
                          <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                        </node>
                        <node concept="liA8E" id="3rIOsIryWz1" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="37vLTw" id="3rIOsIryWz2" role="37wK5m">
                            <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2lFfnZ_W6vA" role="3clFbw">
                    <node concept="37vLTw" id="2lFfnZ_W5Ef" role="2Oq$k0">
                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                    </node>
                    <node concept="2OwXpG" id="2lFfnZ_W76a" role="2OqNvi">
                      <ref role="2Oxat5" node="2lFfnZ$FZEv" resolve="wed" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="2lFfnZ_Waq0" role="3cqZAp">
                  <node concept="3clFbS" id="2lFfnZ_Waq2" role="3clFbx">
                    <node concept="3clFbF" id="2lFfnZ_Y0jh" role="3cqZAp">
                      <node concept="37vLTI" id="2lFfnZ_Y0ji" role="3clFbG">
                        <node concept="3cpWs3" id="2lFfnZ_Y0jj" role="37vLTx">
                          <node concept="1rXfSq" id="2lFfnZ_Y0jk" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZ_Y0jl" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_Y0jm" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_Y0jn" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZ_Y0jo" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_Y0jp" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_Y0jq" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs3" id="2lFfnZ_YEtA" role="3uHU7B">
                            <node concept="Xl_RD" id="2lFfnZ_YF0Y" role="3uHU7w">
                              <property role="Xl_RC" value="," />
                            </node>
                            <node concept="3cpWs3" id="2lFfnZ_Y0jr" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZ_Y0js" role="3uHU7B">
                                <node concept="3cpWs3" id="2lFfnZ_Y0jt" role="3uHU7B">
                                  <node concept="3cpWs3" id="2lFfnZ_Y0ju" role="3uHU7B">
                                    <node concept="37vLTw" id="2lFfnZ_Y0jv" role="3uHU7B">
                                      <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                                    </node>
                                    <node concept="Xl_RD" id="2lFfnZ_Y0jw" role="3uHU7w">
                                      <property role="Xl_RC" value="," />
                                    </node>
                                  </node>
                                  <node concept="2OqwBi" id="2lFfnZ_Y0jx" role="3uHU7w">
                                    <node concept="37vLTw" id="2lFfnZ_Y0jy" role="2Oq$k0">
                                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                    </node>
                                    <node concept="2OwXpG" id="2lFfnZ_Y0jz" role="2OqNvi">
                                      <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZ_Y0j$" role="3uHU7w">
                                  <property role="Xl_RC" value=",Thursday," />
                                </node>
                              </node>
                              <node concept="1rXfSq" id="2lFfnZ_Y0j_" role="3uHU7w">
                                <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                                <node concept="2OqwBi" id="2lFfnZ_Y0jA" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_Y0jB" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_Y0jC" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="2lFfnZ_Y0jD" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_Y0jE" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_Y0jF" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="2lFfnZ_Y0jG" role="37vLTJ">
                          <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="3rIOsIryWXo" role="3cqZAp">
                      <node concept="2OqwBi" id="3rIOsIryWXp" role="3clFbG">
                        <node concept="37vLTw" id="3rIOsIryWXq" role="2Oq$k0">
                          <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                        </node>
                        <node concept="liA8E" id="3rIOsIryWXr" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="37vLTw" id="3rIOsIryWXs" role="37wK5m">
                            <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2lFfnZ_WcLE" role="3clFbw">
                    <node concept="37vLTw" id="2lFfnZ_WbWj" role="2Oq$k0">
                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                    </node>
                    <node concept="2OwXpG" id="2lFfnZ_Wdoe" role="2OqNvi">
                      <ref role="2Oxat5" node="2lFfnZ$G4Nq" resolve="thu" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="2lFfnZ_WkUD" role="3cqZAp">
                  <node concept="3clFbS" id="2lFfnZ_WkUF" role="3clFbx">
                    <node concept="3clFbF" id="2lFfnZ_Y4l3" role="3cqZAp">
                      <node concept="37vLTI" id="2lFfnZ_Y4l4" role="3clFbG">
                        <node concept="3cpWs3" id="2lFfnZ_Y4l5" role="37vLTx">
                          <node concept="1rXfSq" id="2lFfnZ_Y4l6" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZ_Y4l7" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_Y4l8" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_Y4l9" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZ_Y4la" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_Y4lb" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_Y4lc" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs3" id="2lFfnZ_YGD5" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZ_Y4ld" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZ_Y4le" role="3uHU7B">
                                <node concept="3cpWs3" id="2lFfnZ_Y4lf" role="3uHU7B">
                                  <node concept="3cpWs3" id="2lFfnZ_Y4lg" role="3uHU7B">
                                    <node concept="37vLTw" id="2lFfnZ_Y4lh" role="3uHU7B">
                                      <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                                    </node>
                                    <node concept="Xl_RD" id="2lFfnZ_Y4li" role="3uHU7w">
                                      <property role="Xl_RC" value="," />
                                    </node>
                                  </node>
                                  <node concept="2OqwBi" id="2lFfnZ_Y4lj" role="3uHU7w">
                                    <node concept="37vLTw" id="2lFfnZ_Y4lk" role="2Oq$k0">
                                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                    </node>
                                    <node concept="2OwXpG" id="2lFfnZ_Y4ll" role="2OqNvi">
                                      <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZ_Y4lm" role="3uHU7w">
                                  <property role="Xl_RC" value=",Friday," />
                                </node>
                              </node>
                              <node concept="1rXfSq" id="2lFfnZ_Y4ln" role="3uHU7w">
                                <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                                <node concept="2OqwBi" id="2lFfnZ_Y4lo" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_Y4lp" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_Y4lq" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="2lFfnZ_Y4lr" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_Y4ls" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_Y4lt" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZ_YK5r" role="3uHU7w">
                              <property role="Xl_RC" value="," />
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="2lFfnZ_Y4lu" role="37vLTJ">
                          <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="3rIOsIryXpH" role="3cqZAp">
                      <node concept="2OqwBi" id="3rIOsIryXpI" role="3clFbG">
                        <node concept="37vLTw" id="3rIOsIryXpJ" role="2Oq$k0">
                          <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                        </node>
                        <node concept="liA8E" id="3rIOsIryXpK" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="37vLTw" id="3rIOsIryXpL" role="37wK5m">
                            <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2lFfnZ_Wno8" role="3clFbw">
                    <node concept="37vLTw" id="2lFfnZ_WmyL" role="2Oq$k0">
                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                    </node>
                    <node concept="2OwXpG" id="2lFfnZ_WnYG" role="2OqNvi">
                      <ref role="2Oxat5" node="2lFfnZ$G92r" resolve="fri" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="2lFfnZ_Wrnl" role="3cqZAp">
                  <node concept="3clFbS" id="2lFfnZ_Wrnn" role="3clFbx">
                    <node concept="3clFbF" id="2lFfnZ_Y6yC" role="3cqZAp">
                      <node concept="37vLTI" id="2lFfnZ_Y6yD" role="3clFbG">
                        <node concept="3cpWs3" id="2lFfnZ_Y6yE" role="37vLTx">
                          <node concept="1rXfSq" id="2lFfnZ_Y6yF" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZ_Y6yG" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_Y6yH" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_Y6yI" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZ_Y6yJ" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_Y6yK" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_Y6yL" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs3" id="2lFfnZ_YKtG" role="3uHU7B">
                            <node concept="Xl_RD" id="2lFfnZ_YL14" role="3uHU7w">
                              <property role="Xl_RC" value="," />
                            </node>
                            <node concept="3cpWs3" id="2lFfnZ_Y6yM" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZ_Y6yN" role="3uHU7B">
                                <node concept="3cpWs3" id="2lFfnZ_Y6yO" role="3uHU7B">
                                  <node concept="3cpWs3" id="2lFfnZ_Y6yP" role="3uHU7B">
                                    <node concept="37vLTw" id="2lFfnZ_Y6yQ" role="3uHU7B">
                                      <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                                    </node>
                                    <node concept="Xl_RD" id="2lFfnZ_Y6yR" role="3uHU7w">
                                      <property role="Xl_RC" value="," />
                                    </node>
                                  </node>
                                  <node concept="2OqwBi" id="2lFfnZ_Y6yS" role="3uHU7w">
                                    <node concept="37vLTw" id="2lFfnZ_Y6yT" role="2Oq$k0">
                                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                    </node>
                                    <node concept="2OwXpG" id="2lFfnZ_Y6yU" role="2OqNvi">
                                      <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZ_Y6yV" role="3uHU7w">
                                  <property role="Xl_RC" value=",Saturday," />
                                </node>
                              </node>
                              <node concept="1rXfSq" id="2lFfnZ_Y6yW" role="3uHU7w">
                                <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                                <node concept="2OqwBi" id="2lFfnZ_Y6yX" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_Y6yY" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_Y6yZ" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="2lFfnZ_Y6z0" role="37wK5m">
                                  <node concept="37vLTw" id="2lFfnZ_Y6z1" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_Y6z2" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="2lFfnZ_Y6z3" role="37vLTJ">
                          <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="3rIOsIryXRX" role="3cqZAp">
                      <node concept="2OqwBi" id="3rIOsIryXRY" role="3clFbG">
                        <node concept="37vLTw" id="3rIOsIryXRZ" role="2Oq$k0">
                          <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                        </node>
                        <node concept="liA8E" id="3rIOsIryXS0" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="37vLTw" id="3rIOsIryXS1" role="37wK5m">
                            <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2lFfnZ_Wtnk" role="3clFbw">
                    <node concept="37vLTw" id="2lFfnZ_WsxX" role="2Oq$k0">
                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                    </node>
                    <node concept="2OwXpG" id="2lFfnZ_WtXS" role="2OqNvi">
                      <ref role="2Oxat5" node="2lFfnZ$GdfV" resolve="sat" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="2lFfnZ_Wxnr" role="3cqZAp">
                  <node concept="3clFbS" id="2lFfnZ_Wxnt" role="3clFbx">
                    <node concept="3clFbF" id="2lFfnZ_Y8sE" role="3cqZAp">
                      <node concept="37vLTI" id="2lFfnZ_Y8sF" role="3clFbG">
                        <node concept="3cpWs3" id="2lFfnZ_Y8sG" role="37vLTx">
                          <node concept="1rXfSq" id="2lFfnZ_Y8sH" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZ_Y8sI" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_Y8sJ" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_Y8sK" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZ_Y8sL" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZ_Y8sM" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZ_Y8sN" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs3" id="2lFfnZ_Y8sO" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZ_Y8sP" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZ_Y8sQ" role="3uHU7B">
                                <node concept="3cpWs3" id="2lFfnZ_Y8sR" role="3uHU7B">
                                  <node concept="37vLTw" id="2lFfnZ_Y8sS" role="3uHU7B">
                                    <ref role="3cqZAo" node="2lFfnZ_XiMj" resolve="courseInfo" />
                                  </node>
                                  <node concept="Xl_RD" id="2lFfnZ_Y8sT" role="3uHU7w">
                                    <property role="Xl_RC" value="," />
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="2lFfnZ_Y8sU" role="3uHU7w">
                                  <node concept="37vLTw" id="2lFfnZ_Y8sV" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZ_Y8sW" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                  </node>
                                </node>
                              </node>
                              <node concept="Xl_RD" id="2lFfnZ_Y8sX" role="3uHU7w">
                                <property role="Xl_RC" value=",Sunday," />
                              </node>
                            </node>
                            <node concept="1rXfSq" id="2lFfnZ_Y8sY" role="3uHU7w">
                              <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                              <node concept="2OqwBi" id="2lFfnZ_Y8sZ" role="37wK5m">
                                <node concept="37vLTw" id="2lFfnZ_Y8t0" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZ_Y8t1" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2lFfnZ_Y8t2" role="37wK5m">
                                <node concept="37vLTw" id="2lFfnZ_Y8t3" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZ_Y8t4" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="2lFfnZ_Y8t5" role="37vLTJ">
                          <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="3rIOsIryYo8" role="3cqZAp">
                      <node concept="2OqwBi" id="3rIOsIryYo9" role="3clFbG">
                        <node concept="37vLTw" id="3rIOsIryYoa" role="2Oq$k0">
                          <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                        </node>
                        <node concept="liA8E" id="3rIOsIryYob" role="2OqNvi">
                          <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                          <node concept="37vLTw" id="3rIOsIryYoc" role="37wK5m">
                            <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2lFfnZ_WzsO" role="3clFbw">
                    <node concept="37vLTw" id="2lFfnZ_WyBS" role="2Oq$k0">
                      <ref role="3cqZAo" node="2lFfnZ_xXgG" resolve="myClass" />
                    </node>
                    <node concept="2OwXpG" id="2lFfnZ_W$3N" role="2OqNvi">
                      <ref role="2Oxat5" node="2lFfnZ$GhvA" resolve="sun" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWsn" id="2lFfnZ_xXgG" role="1Duv9x">
                <property role="TrG5h" value="myClass" />
                <node concept="3uibUv" id="2lFfnZ_xXLN" role="1tU5fm">
                  <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
                  <node concept="1KehLL" id="2lFfnZ_xYQs" role="lGtFl">
                    <property role="1K8rM7" value="ReferencePresentation_91bvrs_a0a0" />
                    <property role="1Kfyot" value="right" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3rIOsIr_Zzg" role="1DdaDG">
                <node concept="37vLTw" id="2lFfnZ_xZA8" role="2Oq$k0">
                  <ref role="3cqZAo" node="2lFfnZ_vNt7" resolve="myCourse" />
                </node>
                <node concept="2OwXpG" id="3rIOsIrA0V9" role="2OqNvi">
                  <ref role="2Oxat5" node="3rIOsIrzsnn" resolve="classes" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="2lFfnZ_vNt7" role="1Duv9x">
            <property role="TrG5h" value="myCourse" />
            <node concept="3uibUv" id="2lFfnZ_vNNO" role="1tU5fm">
              <ref role="3uigEE" node="2lFfnZ$MNHC" resolve="Schedule.Course" />
            </node>
          </node>
          <node concept="37vLTw" id="3rIOsIryAMC" role="1DdaDG">
            <ref role="3cqZAo" node="3rIOsIry918" resolve="courses" />
          </node>
        </node>
        <node concept="3clFbH" id="3rIOsIrHLIm" role="3cqZAp" />
        <node concept="1DcWWT" id="2lFfnZ_Zikv" role="3cqZAp">
          <node concept="3clFbS" id="2lFfnZ_Zikx" role="2LFqv$">
            <node concept="3clFbJ" id="2lFfnZ_ZroW" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ_ZroX" role="3clFbx">
                <node concept="3clFbF" id="2lFfnZA1nty" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZA1oBg" role="3clFbG">
                    <node concept="3cpWs3" id="2lFfnZA1NDv" role="37vLTx">
                      <node concept="1rXfSq" id="2lFfnZA1PWf" role="3uHU7w">
                        <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                        <node concept="2OqwBi" id="2lFfnZA1QW0" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA1QW1" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA1QW2" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2lFfnZA1QW3" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA1QW4" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA1QW5" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                          </node>
                        </node>
                      </node>
                      <node concept="3cpWs3" id="2lFfnZA1LaB" role="3uHU7B">
                        <node concept="3cpWs3" id="2lFfnZA1Ct1" role="3uHU7B">
                          <node concept="3cpWs3" id="2lFfnZA1ybg" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZA1uo3" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZA1sel" role="3uHU7B">
                                <node concept="2OqwBi" id="2lFfnZA1q$s" role="3uHU7B">
                                  <node concept="37vLTw" id="2lFfnZA1pIW" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZA1qXR" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZA1tHF" role="3uHU7w">
                                  <property role="Xl_RC" value="," />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2lFfnZA1wDr" role="3uHU7w">
                                <node concept="37vLTw" id="2lFfnZA1vK4" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZA1wRv" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZA1z05" role="3uHU7w">
                              <property role="Xl_RC" value=",NA,NA,Monday," />
                            </node>
                          </node>
                          <node concept="1rXfSq" id="2lFfnZA1DPM" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZA1GTx" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA1GTy" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA1Hu1" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZA1GT$" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA1GT_" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA1HRl" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="2lFfnZA1M27" role="3uHU7w">
                          <property role="Xl_RC" value="," />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="2lFfnZA1ntw" role="37vLTJ">
                      <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3rIOsIryZl0" role="3cqZAp">
                  <node concept="2OqwBi" id="3rIOsIryZl1" role="3clFbG">
                    <node concept="37vLTw" id="3rIOsIryZl2" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                    </node>
                    <node concept="liA8E" id="3rIOsIryZl3" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="3rIOsIryZl4" role="37wK5m">
                        <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="2lFfnZ_Zrpx" role="3clFbw">
                <node concept="37vLTw" id="2lFfnZ_ZDI0" role="2Oq$k0">
                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                </node>
                <node concept="2OwXpG" id="2lFfnZ_Zrpz" role="2OqNvi">
                  <ref role="2Oxat5" node="2lFfnZ$FQX5" resolve="mon" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="2lFfnZ_Zrp$" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ_Zrp_" role="3clFbx">
                <node concept="3clFbF" id="2lFfnZA1SOT" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZA1SOU" role="3clFbG">
                    <node concept="3cpWs3" id="2lFfnZA1SOV" role="37vLTx">
                      <node concept="1rXfSq" id="2lFfnZA1SOW" role="3uHU7w">
                        <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                        <node concept="2OqwBi" id="2lFfnZA1SOX" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA1SOY" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA1SOZ" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2lFfnZA1SP0" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA1SP1" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA1SP2" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                          </node>
                        </node>
                      </node>
                      <node concept="3cpWs3" id="2lFfnZA1SP3" role="3uHU7B">
                        <node concept="3cpWs3" id="2lFfnZA1SP4" role="3uHU7B">
                          <node concept="3cpWs3" id="2lFfnZA1SP5" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZA1SP6" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZA1SP7" role="3uHU7B">
                                <node concept="2OqwBi" id="2lFfnZA1SP8" role="3uHU7B">
                                  <node concept="37vLTw" id="2lFfnZA1SP9" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZA1SPa" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZA1SPb" role="3uHU7w">
                                  <property role="Xl_RC" value="," />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2lFfnZA1SPc" role="3uHU7w">
                                <node concept="37vLTw" id="2lFfnZA1SPd" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZA1SPe" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZA1SPf" role="3uHU7w">
                              <property role="Xl_RC" value=",NA,NA,Tuesday," />
                            </node>
                          </node>
                          <node concept="1rXfSq" id="2lFfnZA1SPg" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZA1SPh" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA1SPi" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA1SPj" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZA1SPk" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA1SPl" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA1SPm" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="2lFfnZA1SPn" role="3uHU7w">
                          <property role="Xl_RC" value="," />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="2lFfnZA1SPo" role="37vLTJ">
                      <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3rIOsIryZSJ" role="3cqZAp">
                  <node concept="2OqwBi" id="3rIOsIryZSK" role="3clFbG">
                    <node concept="37vLTw" id="3rIOsIryZSL" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                    </node>
                    <node concept="liA8E" id="3rIOsIryZSM" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="3rIOsIryZSN" role="37wK5m">
                        <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="2lFfnZ_Zrq9" role="3clFbw">
                <node concept="37vLTw" id="2lFfnZA25C3" role="2Oq$k0">
                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                </node>
                <node concept="2OwXpG" id="2lFfnZ_Zrqb" role="2OqNvi">
                  <ref role="2Oxat5" node="2lFfnZ$FVd1" resolve="tue" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="2lFfnZ_Zrqc" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ_Zrqd" role="3clFbx">
                <node concept="3clFbF" id="2lFfnZA1ViR" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZA1ViS" role="3clFbG">
                    <node concept="3cpWs3" id="2lFfnZA1ViT" role="37vLTx">
                      <node concept="1rXfSq" id="2lFfnZA1ViU" role="3uHU7w">
                        <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                        <node concept="2OqwBi" id="2lFfnZA1ViV" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA1ViW" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA1ViX" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2lFfnZA1ViY" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA1ViZ" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA1Vj0" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                          </node>
                        </node>
                      </node>
                      <node concept="3cpWs3" id="2lFfnZA1Vj1" role="3uHU7B">
                        <node concept="3cpWs3" id="2lFfnZA1Vj2" role="3uHU7B">
                          <node concept="3cpWs3" id="2lFfnZA1Vj3" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZA1Vj4" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZA1Vj5" role="3uHU7B">
                                <node concept="2OqwBi" id="2lFfnZA1Vj6" role="3uHU7B">
                                  <node concept="37vLTw" id="2lFfnZA1Vj7" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZA1Vj8" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZA1Vj9" role="3uHU7w">
                                  <property role="Xl_RC" value="," />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2lFfnZA1Vja" role="3uHU7w">
                                <node concept="37vLTw" id="2lFfnZA1Vjb" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZA1Vjc" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZA1Vjd" role="3uHU7w">
                              <property role="Xl_RC" value=",NA,NA,Wednesday," />
                            </node>
                          </node>
                          <node concept="1rXfSq" id="2lFfnZA1Vje" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZA1Vjf" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA1Vjg" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA1Vjh" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZA1Vji" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA1Vjj" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA1Vjk" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="2lFfnZA1Vjl" role="3uHU7w">
                          <property role="Xl_RC" value="," />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="2lFfnZA1Vjm" role="37vLTJ">
                      <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3rIOsIrz0up" role="3cqZAp">
                  <node concept="2OqwBi" id="3rIOsIrz0uq" role="3clFbG">
                    <node concept="37vLTw" id="3rIOsIrz0ur" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                    </node>
                    <node concept="liA8E" id="3rIOsIrz0us" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="3rIOsIrz0ut" role="37wK5m">
                        <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="2lFfnZ_ZrqL" role="3clFbw">
                <node concept="37vLTw" id="2lFfnZA26yf" role="2Oq$k0">
                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                </node>
                <node concept="2OwXpG" id="2lFfnZ_ZrqN" role="2OqNvi">
                  <ref role="2Oxat5" node="2lFfnZ$FZEv" resolve="wed" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="2lFfnZ_ZrqO" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ_ZrqP" role="3clFbx">
                <node concept="3clFbF" id="2lFfnZA1Y3u" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZA1Y3v" role="3clFbG">
                    <node concept="3cpWs3" id="2lFfnZA1Y3w" role="37vLTx">
                      <node concept="1rXfSq" id="2lFfnZA1Y3x" role="3uHU7w">
                        <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                        <node concept="2OqwBi" id="2lFfnZA1Y3y" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA1Y3z" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA1Y3$" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2lFfnZA1Y3_" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA1Y3A" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA1Y3B" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                          </node>
                        </node>
                      </node>
                      <node concept="3cpWs3" id="2lFfnZA1Y3C" role="3uHU7B">
                        <node concept="3cpWs3" id="2lFfnZA1Y3D" role="3uHU7B">
                          <node concept="3cpWs3" id="2lFfnZA1Y3E" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZA1Y3F" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZA1Y3G" role="3uHU7B">
                                <node concept="2OqwBi" id="2lFfnZA1Y3H" role="3uHU7B">
                                  <node concept="37vLTw" id="2lFfnZA1Y3I" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZA1Y3J" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZA1Y3K" role="3uHU7w">
                                  <property role="Xl_RC" value="," />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2lFfnZA1Y3L" role="3uHU7w">
                                <node concept="37vLTw" id="2lFfnZA1Y3M" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZA1Y3N" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZA1Y3O" role="3uHU7w">
                              <property role="Xl_RC" value=",NA,NA,Thursday," />
                            </node>
                          </node>
                          <node concept="1rXfSq" id="2lFfnZA1Y3P" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZA1Y3Q" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA1Y3R" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA1Y3S" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZA1Y3T" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA1Y3U" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA1Y3V" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="2lFfnZA1Y3W" role="3uHU7w">
                          <property role="Xl_RC" value="," />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="2lFfnZA1Y3X" role="37vLTJ">
                      <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3rIOsIrz160" role="3cqZAp">
                  <node concept="2OqwBi" id="3rIOsIrz161" role="3clFbG">
                    <node concept="37vLTw" id="3rIOsIrz162" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                    </node>
                    <node concept="liA8E" id="3rIOsIrz163" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="3rIOsIrz164" role="37wK5m">
                        <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="2lFfnZ_Zrrp" role="3clFbw">
                <node concept="37vLTw" id="2lFfnZA27sr" role="2Oq$k0">
                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                </node>
                <node concept="2OwXpG" id="2lFfnZ_Zrrr" role="2OqNvi">
                  <ref role="2Oxat5" node="2lFfnZ$G4Nq" resolve="thu" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="2lFfnZ_Zrrs" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ_Zrrt" role="3clFbx">
                <node concept="3clFbF" id="2lFfnZA2a_j" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZA2a_k" role="3clFbG">
                    <node concept="3cpWs3" id="2lFfnZA2a_l" role="37vLTx">
                      <node concept="1rXfSq" id="2lFfnZA2a_m" role="3uHU7w">
                        <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                        <node concept="2OqwBi" id="2lFfnZA2a_n" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA2a_o" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA2a_p" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2lFfnZA2a_q" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA2a_r" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA2a_s" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                          </node>
                        </node>
                      </node>
                      <node concept="3cpWs3" id="2lFfnZA2a_t" role="3uHU7B">
                        <node concept="3cpWs3" id="2lFfnZA2a_u" role="3uHU7B">
                          <node concept="3cpWs3" id="2lFfnZA2a_v" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZA2a_w" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZA2a_x" role="3uHU7B">
                                <node concept="2OqwBi" id="2lFfnZA2a_y" role="3uHU7B">
                                  <node concept="37vLTw" id="2lFfnZA2a_z" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZA2a_$" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZA2a__" role="3uHU7w">
                                  <property role="Xl_RC" value="," />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2lFfnZA2a_A" role="3uHU7w">
                                <node concept="37vLTw" id="2lFfnZA2a_B" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZA2a_C" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZA2a_D" role="3uHU7w">
                              <property role="Xl_RC" value=",NA,NA,Friday," />
                            </node>
                          </node>
                          <node concept="1rXfSq" id="2lFfnZA2a_E" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZA2a_F" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA2a_G" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA2a_H" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZA2a_I" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA2a_J" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA2a_K" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="2lFfnZA2a_L" role="3uHU7w">
                          <property role="Xl_RC" value="," />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="2lFfnZA2a_M" role="37vLTJ">
                      <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3rIOsIrz1Jw" role="3cqZAp">
                  <node concept="2OqwBi" id="3rIOsIrz1Jx" role="3clFbG">
                    <node concept="37vLTw" id="3rIOsIrz1Jy" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                    </node>
                    <node concept="liA8E" id="3rIOsIrz1Jz" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="3rIOsIrz1J$" role="37wK5m">
                        <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="2lFfnZ_Zrs1" role="3clFbw">
                <node concept="37vLTw" id="2lFfnZA28mB" role="2Oq$k0">
                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                </node>
                <node concept="2OwXpG" id="2lFfnZ_Zrs3" role="2OqNvi">
                  <ref role="2Oxat5" node="2lFfnZ$G92r" resolve="fri" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="2lFfnZ_Zrs4" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ_Zrs5" role="3clFbx">
                <node concept="3clFbF" id="2lFfnZA2cBp" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZA2cBq" role="3clFbG">
                    <node concept="3cpWs3" id="2lFfnZA2cBr" role="37vLTx">
                      <node concept="1rXfSq" id="2lFfnZA2cBs" role="3uHU7w">
                        <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                        <node concept="2OqwBi" id="2lFfnZA2cBt" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA2cBu" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA2cBv" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2lFfnZA2cBw" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA2cBx" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA2cBy" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                          </node>
                        </node>
                      </node>
                      <node concept="3cpWs3" id="2lFfnZA2cBz" role="3uHU7B">
                        <node concept="3cpWs3" id="2lFfnZA2cB$" role="3uHU7B">
                          <node concept="3cpWs3" id="2lFfnZA2cB_" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZA2cBA" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZA2cBB" role="3uHU7B">
                                <node concept="2OqwBi" id="2lFfnZA2cBC" role="3uHU7B">
                                  <node concept="37vLTw" id="2lFfnZA2cBD" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZA2cBE" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZA2cBF" role="3uHU7w">
                                  <property role="Xl_RC" value="," />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2lFfnZA2cBG" role="3uHU7w">
                                <node concept="37vLTw" id="2lFfnZA2cBH" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZA2cBI" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZA2cBJ" role="3uHU7w">
                              <property role="Xl_RC" value=",NA,NA,Saturday," />
                            </node>
                          </node>
                          <node concept="1rXfSq" id="2lFfnZA2cBK" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZA2cBL" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA2cBM" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA2cBN" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZA2cBO" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA2cBP" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA2cBQ" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="2lFfnZA2cBR" role="3uHU7w">
                          <property role="Xl_RC" value="," />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="2lFfnZA2cBS" role="37vLTJ">
                      <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3rIOsIrz2qV" role="3cqZAp">
                  <node concept="2OqwBi" id="3rIOsIrz2qW" role="3clFbG">
                    <node concept="37vLTw" id="3rIOsIrz2qX" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                    </node>
                    <node concept="liA8E" id="3rIOsIrz2qY" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="3rIOsIrz2qZ" role="37wK5m">
                        <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="2lFfnZ_ZrsD" role="3clFbw">
                <node concept="37vLTw" id="2lFfnZA29gN" role="2Oq$k0">
                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                </node>
                <node concept="2OwXpG" id="2lFfnZ_ZrsF" role="2OqNvi">
                  <ref role="2Oxat5" node="2lFfnZ$GdfV" resolve="sat" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="2lFfnZ_ZrsG" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ_ZrsH" role="3clFbx">
                <node concept="3clFbF" id="2lFfnZA2eDv" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZA2eDw" role="3clFbG">
                    <node concept="3cpWs3" id="2lFfnZA2eDx" role="37vLTx">
                      <node concept="1rXfSq" id="2lFfnZA2eDy" role="3uHU7w">
                        <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                        <node concept="2OqwBi" id="2lFfnZA2eDz" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA2eD$" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA2eD_" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2lFfnZA2eDA" role="37wK5m">
                          <node concept="37vLTw" id="2lFfnZA2eDB" role="2Oq$k0">
                            <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                          </node>
                          <node concept="2OwXpG" id="2lFfnZA2eDC" role="2OqNvi">
                            <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                          </node>
                        </node>
                      </node>
                      <node concept="3cpWs3" id="2lFfnZA2eDD" role="3uHU7B">
                        <node concept="3cpWs3" id="2lFfnZA2eDE" role="3uHU7B">
                          <node concept="3cpWs3" id="2lFfnZA2eDF" role="3uHU7B">
                            <node concept="3cpWs3" id="2lFfnZA2eDG" role="3uHU7B">
                              <node concept="3cpWs3" id="2lFfnZA2eDH" role="3uHU7B">
                                <node concept="2OqwBi" id="2lFfnZA2eDI" role="3uHU7B">
                                  <node concept="37vLTw" id="2lFfnZA2eDJ" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                  </node>
                                  <node concept="2OwXpG" id="2lFfnZA2eDK" role="2OqNvi">
                                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                                  </node>
                                </node>
                                <node concept="Xl_RD" id="2lFfnZA2eDL" role="3uHU7w">
                                  <property role="Xl_RC" value="," />
                                </node>
                              </node>
                              <node concept="2OqwBi" id="2lFfnZA2eDM" role="3uHU7w">
                                <node concept="37vLTw" id="2lFfnZA2eDN" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                                </node>
                                <node concept="2OwXpG" id="2lFfnZA2eDO" role="2OqNvi">
                                  <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                                </node>
                              </node>
                            </node>
                            <node concept="Xl_RD" id="2lFfnZA2eDP" role="3uHU7w">
                              <property role="Xl_RC" value=",NA,NA,Sunday," />
                            </node>
                          </node>
                          <node concept="1rXfSq" id="2lFfnZA2eDQ" role="3uHU7w">
                            <ref role="37wK5l" node="2lFfnZ_D$4P" resolve="formatTime" />
                            <node concept="2OqwBi" id="2lFfnZA2eDR" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA2eDS" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA2eDT" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                              </node>
                            </node>
                            <node concept="2OqwBi" id="2lFfnZA2eDU" role="37wK5m">
                              <node concept="37vLTw" id="2lFfnZA2eDV" role="2Oq$k0">
                                <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                              </node>
                              <node concept="2OwXpG" id="2lFfnZA2eDW" role="2OqNvi">
                                <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="Xl_RD" id="2lFfnZA2eDX" role="3uHU7w">
                          <property role="Xl_RC" value="," />
                        </node>
                      </node>
                    </node>
                    <node concept="37vLTw" id="2lFfnZA2eDY" role="37vLTJ">
                      <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3rIOsIrz38h" role="3cqZAp">
                  <node concept="2OqwBi" id="3rIOsIrz38i" role="3clFbG">
                    <node concept="37vLTw" id="3rIOsIrz38j" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                    </node>
                    <node concept="liA8E" id="3rIOsIrz38k" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="3rIOsIrz38l" role="37wK5m">
                        <ref role="3cqZAo" node="2lFfnZ_qDDP" resolve="tempString" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="2lFfnZ_Zrtf" role="3clFbw">
                <node concept="37vLTw" id="2lFfnZA2aaZ" role="2Oq$k0">
                  <ref role="3cqZAo" node="2lFfnZ_Ziky" resolve="activity" />
                </node>
                <node concept="2OwXpG" id="2lFfnZ_Zrth" role="2OqNvi">
                  <ref role="2Oxat5" node="2lFfnZ$GhvA" resolve="sun" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="2lFfnZ_Ziky" role="1Duv9x">
            <property role="TrG5h" value="activity" />
            <node concept="3uibUv" id="2lFfnZ_ZjDi" role="1tU5fm">
              <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
            </node>
          </node>
          <node concept="2OqwBi" id="2lFfnZ_ZmZ$" role="1DdaDG">
            <node concept="Xjq3P" id="2lFfnZ_Zly1" role="2Oq$k0" />
            <node concept="2OwXpG" id="3rIOsIrzd_E" role="2OqNvi">
              <ref role="2Oxat5" node="3rIOsIrxzmG" resolve="activities" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="3rIOsIrHOxc" role="3cqZAp" />
        <node concept="3cpWs8" id="2lFfnZA3PtN" role="3cqZAp">
          <node concept="3cpWsn" id="2lFfnZA3PtQ" role="3cpWs9">
            <property role="TrG5h" value="size" />
            <node concept="10Oyi0" id="2lFfnZA3PtL" role="1tU5fm" />
            <node concept="2OqwBi" id="2lFfnZA3VNv" role="33vP2m">
              <node concept="37vLTw" id="3rIOsIrz3Wy" role="2Oq$k0">
                <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
              </node>
              <node concept="liA8E" id="2lFfnZA40cI" role="2OqNvi">
                <ref role="37wK5l" to="33ny:~ArrayList.size():int" resolve="size" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="3rIOsIrB2ui" role="3cqZAp">
          <node concept="3cpWsn" id="3rIOsIrB2ul" role="3cpWs9">
            <property role="TrG5h" value="retString" />
            <node concept="17QB3L" id="3rIOsIrB2ug" role="1tU5fm" />
            <node concept="Xl_RD" id="3rIOsIrB73W" role="33vP2m" />
          </node>
        </node>
        <node concept="3clFbF" id="2lFfnZA59jv" role="3cqZAp">
          <node concept="37vLTI" id="2lFfnZA5gav" role="3clFbG">
            <node concept="2ShNRf" id="2lFfnZA5ieg" role="37vLTx">
              <node concept="3$_iS1" id="2lFfnZA5lFV" role="2ShVmc">
                <node concept="3$GHV9" id="2lFfnZA5lFX" role="3$GQph">
                  <node concept="2OqwBi" id="2lFfnZA6L9_" role="3$I4v7">
                    <node concept="37vLTw" id="3rIOsIrz49J" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
                    </node>
                    <node concept="liA8E" id="2lFfnZA6L9B" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.size():int" resolve="size" />
                    </node>
                  </node>
                </node>
                <node concept="17QB3L" id="2lFfnZA5lhG" role="3$_nBY" />
              </node>
            </node>
            <node concept="37vLTw" id="2lFfnZA59jt" role="37vLTJ">
              <ref role="3cqZAo" node="2lFfnZ_klFn" resolve="scheduleArray" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2lFfnZA70Zj" role="3cqZAp">
          <node concept="37vLTI" id="2lFfnZA78Xd" role="3clFbG">
            <node concept="2OqwBi" id="2lFfnZA7e8_" role="37vLTx">
              <node concept="37vLTw" id="3rIOsIrz4j4" role="2Oq$k0">
                <ref role="3cqZAo" node="3rIOsIryxxQ" resolve="tempArray" />
              </node>
              <node concept="liA8E" id="2lFfnZA7iBl" role="2OqNvi">
                <ref role="37wK5l" to="33ny:~ArrayList.toArray(java.lang.Object[]):java.lang.Object[]" resolve="toArray" />
                <node concept="37vLTw" id="2lFfnZA7nim" role="37wK5m">
                  <ref role="3cqZAo" node="2lFfnZ_klFn" resolve="scheduleArray" />
                </node>
              </node>
            </node>
            <node concept="37vLTw" id="2lFfnZA70Zh" role="37vLTJ">
              <ref role="3cqZAo" node="2lFfnZ_klFn" resolve="scheduleArray" />
            </node>
          </node>
        </node>
        <node concept="1DcWWT" id="5t_d$dJuzNy" role="3cqZAp">
          <node concept="3clFbS" id="5t_d$dJuzN$" role="2LFqv$">
            <node concept="3clFbJ" id="5t_d$dJFTol" role="3cqZAp">
              <node concept="3clFbS" id="5t_d$dJFTon" role="3clFbx">
                <node concept="3clFbF" id="5t_d$dJuHrx" role="3cqZAp">
                  <node concept="2OqwBi" id="5t_d$dJuHru" role="3clFbG">
                    <node concept="10M0yZ" id="5t_d$dJuHrv" role="2Oq$k0">
                      <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                      <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
                    </node>
                    <node concept="liA8E" id="5t_d$dJuHrw" role="2OqNvi">
                      <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                      <node concept="3cpWs3" id="5t_d$dJuMQx" role="37wK5m">
                        <node concept="Xl_RD" id="5t_d$dJuN_n" role="3uHU7w">
                          <property role="Xl_RC" value="\&quot;," />
                        </node>
                        <node concept="3cpWs3" id="5t_d$dJuKUR" role="3uHU7B">
                          <node concept="Xl_RD" id="5t_d$dJuJ_s" role="3uHU7B">
                            <property role="Xl_RC" value="\&quot;" />
                          </node>
                          <node concept="37vLTw" id="5t_d$dJuLwF" role="3uHU7w">
                            <ref role="3cqZAo" node="5t_d$dJuzN_" resolve="temp" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOSWO" id="5t_d$dJFYRx" role="3clFbw">
                <node concept="3cmrfG" id="5t_d$dJGa4q" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="5t_d$dJGyaT" role="3uHU7B">
                  <node concept="2OqwBi" id="5t_d$dJGwQQ" role="2Oq$k0">
                    <node concept="37vLTw" id="5t_d$dJFUGY" role="2Oq$k0">
                      <ref role="3cqZAo" node="5t_d$dJuzN_" resolve="temp" />
                    </node>
                    <node concept="liA8E" id="5t_d$dJGxAk" role="2OqNvi">
                      <ref role="37wK5l" to="wyt6:~String.trim():java.lang.String" resolve="trim" />
                    </node>
                  </node>
                  <node concept="liA8E" id="5t_d$dJGzqV" role="2OqNvi">
                    <ref role="37wK5l" to="wyt6:~String.length():int" resolve="length" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="5t_d$dJuzN_" role="1Duv9x">
            <property role="TrG5h" value="temp" />
            <node concept="17QB3L" id="5t_d$dJuBRo" role="1tU5fm" />
          </node>
          <node concept="37vLTw" id="5t_d$dJuECv" role="1DdaDG">
            <ref role="3cqZAo" node="2lFfnZ_klFn" resolve="scheduleArray" />
          </node>
        </node>
        <node concept="3clFbF" id="5t_d$dJzaxe" role="3cqZAp">
          <node concept="2OqwBi" id="5t_d$dJzaxb" role="3clFbG">
            <node concept="10M0yZ" id="5t_d$dJzaxc" role="2Oq$k0">
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
            </node>
            <node concept="liA8E" id="5t_d$dJzaxd" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="Xl_RD" id="5t_d$dJzbHn" role="37wK5m">
                <property role="Xl_RC" value="\n" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5t_d$dJFsZq" role="3cqZAp">
          <node concept="2OqwBi" id="5t_d$dJFsZn" role="3clFbG">
            <node concept="10M0yZ" id="5t_d$dJFsZo" role="2Oq$k0">
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
            </node>
            <node concept="liA8E" id="5t_d$dJFsZp" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="Xl_RD" id="5t_d$dJFyy3" role="37wK5m">
                <property role="Xl_RC" value="Boon" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5t_d$dJvlan" role="3cqZAp" />
      </node>
      <node concept="3Tm6S6" id="2lFfnZ_qejH" role="1B3o_S" />
      <node concept="3cqZAl" id="2lFfnZ_qimB" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2lFfnZ$MGgV" role="jymVt" />
    <node concept="312cEu" id="2lFfnZ$MNHC" role="jymVt">
      <property role="2bfB8j" value="true" />
      <property role="TrG5h" value="Course" />
      <node concept="312cEg" id="2lFfnZ$MS_o" role="jymVt">
        <property role="TrG5h" value="name" />
        <node concept="3Tm1VV" id="2lFfnZ$MRMD" role="1B3o_S" />
        <node concept="17QB3L" id="2lFfnZ$MSmH" role="1tU5fm" />
      </node>
      <node concept="312cEg" id="2lFfnZ$MUJq" role="jymVt">
        <property role="TrG5h" value="levelOfDifficulty" />
        <node concept="3Tm1VV" id="2lFfnZ$MTW_" role="1B3o_S" />
        <node concept="10Oyi0" id="2lFfnZ$MUIR" role="1tU5fm" />
      </node>
      <node concept="312cEg" id="3rIOsIrzsnn" role="jymVt">
        <property role="TrG5h" value="classes" />
        <node concept="3Tm1VV" id="3rIOsIrznZU" role="1B3o_S" />
        <node concept="3uibUv" id="3rIOsIrzqKz" role="1tU5fm">
          <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
          <node concept="3uibUv" id="3rIOsIrzs88" role="11_B2D">
            <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
          </node>
        </node>
        <node concept="2ShNRf" id="3rIOsIrzvaN" role="33vP2m">
          <node concept="1pGfFk" id="3rIOsIrzxiY" role="2ShVmc">
            <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
            <node concept="3uibUv" id="3rIOsIrzy9q" role="1pMfVU">
              <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="2lFfnZ$MLFt" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="2lFfnZ$BVuZ" role="jymVt" />
    <node concept="312cEu" id="2lFfnZ$Bbpm" role="jymVt">
      <property role="2bfB8j" value="true" />
      <property role="TrG5h" value="ActivityTime" />
      <node concept="312cEg" id="2lFfnZ_ZNpp" role="jymVt">
        <property role="TrG5h" value="type" />
        <node concept="3Tm1VV" id="2lFfnZ_ZJjx" role="1B3o_S" />
        <node concept="17QB3L" id="2lFfnZ_ZNoW" role="1tU5fm" />
        <node concept="Xl_RD" id="2lFfnZ_ZRTG" role="33vP2m" />
      </node>
      <node concept="312cEg" id="2lFfnZ$LWXk" role="jymVt">
        <property role="TrG5h" value="name" />
        <node concept="3Tm1VV" id="2lFfnZ$LVc5" role="1B3o_S" />
        <node concept="17QB3L" id="2lFfnZ$LWWL" role="1tU5fm" />
        <node concept="Xl_RD" id="2lFfnZ$LZlr" role="33vP2m" />
      </node>
      <node concept="312cEg" id="2lFfnZ$BjjR" role="jymVt">
        <property role="TrG5h" value="startHour" />
        <node concept="3cmrfG" id="2lFfnZ$EGc2" role="33vP2m">
          <property role="3cmrfH" value="0" />
        </node>
        <node concept="10Oyi0" id="2lFfnZ$GqLi" role="1tU5fm" />
        <node concept="3Tm1VV" id="2lFfnZ$Gy9p" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$EiZx" role="jymVt">
        <property role="TrG5h" value="startMin" />
        <node concept="10Oyi0" id="2lFfnZ$Ei$L" role="1tU5fm" />
        <node concept="3cmrfG" id="2lFfnZ$EGXQ" role="33vP2m">
          <property role="3cmrfH" value="0" />
        </node>
        <node concept="3Tm1VV" id="2lFfnZ$Gye0" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$BlOI" role="jymVt">
        <property role="TrG5h" value="endHour" />
        <node concept="10Oyi0" id="2lFfnZ$Bln9" role="1tU5fm" />
        <node concept="3cmrfG" id="2lFfnZ$EHJE" role="33vP2m">
          <property role="3cmrfH" value="0" />
        </node>
        <node concept="3Tm1VV" id="2lFfnZ$GyiB" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$EldF" role="jymVt">
        <property role="TrG5h" value="endMin" />
        <node concept="10Oyi0" id="2lFfnZ$Ektp" role="1tU5fm" />
        <node concept="3cmrfG" id="2lFfnZ$EIx3" role="33vP2m">
          <property role="3cmrfH" value="0" />
        </node>
        <node concept="3Tm1VV" id="2lFfnZ$Gyne" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$FQX5" role="jymVt">
        <property role="TrG5h" value="mon" />
        <node concept="10P_77" id="2lFfnZ$FPNS" role="1tU5fm" />
        <node concept="3clFbT" id="2lFfnZ$FRBe" role="33vP2m" />
        <node concept="3Tm1VV" id="2lFfnZ$GyrP" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$FVd1" role="jymVt">
        <property role="TrG5h" value="tue" />
        <node concept="10P_77" id="2lFfnZ$FU3H" role="1tU5fm" />
        <node concept="3clFbT" id="2lFfnZ$FVS6" role="33vP2m" />
        <node concept="3Tm1VV" id="2lFfnZ$Gysq" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$FZEv" role="jymVt">
        <property role="TrG5h" value="wed" />
        <node concept="10P_77" id="2lFfnZ$FYyQ" role="1tU5fm" />
        <node concept="3clFbT" id="2lFfnZ$G0kO" role="33vP2m" />
        <node concept="3Tm1VV" id="2lFfnZ$GysZ" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$G4Nq" role="jymVt">
        <property role="TrG5h" value="thu" />
        <node concept="10P_77" id="2lFfnZ$G3FF" role="1tU5fm" />
        <node concept="3clFbT" id="2lFfnZ$G5uF" role="33vP2m" />
        <node concept="3Tm1VV" id="2lFfnZ$Gyt$" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$G92r" role="jymVt">
        <property role="TrG5h" value="fri" />
        <node concept="10P_77" id="2lFfnZ$G7SQ" role="1tU5fm" />
        <node concept="3clFbT" id="2lFfnZ$G9HM" role="33vP2m" />
        <node concept="3Tm1VV" id="2lFfnZ$Gyu9" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$GdfV" role="jymVt">
        <property role="TrG5h" value="sat" />
        <node concept="10P_77" id="2lFfnZ$Gc81" role="1tU5fm" />
        <node concept="3clFbT" id="2lFfnZ$Gf1L" role="33vP2m" />
        <node concept="3Tm1VV" id="2lFfnZ$GyuI" role="1B3o_S" />
      </node>
      <node concept="312cEg" id="2lFfnZ$GhvA" role="jymVt">
        <property role="TrG5h" value="sun" />
        <node concept="10P_77" id="2lFfnZ$GgnA" role="1tU5fm" />
        <node concept="3clFbT" id="2lFfnZ$GjfL" role="33vP2m" />
        <node concept="3Tm1VV" id="2lFfnZ$Gyvj" role="1B3o_S" />
      </node>
      <node concept="3Tm6S6" id="2lFfnZ$B9SB" role="1B3o_S" />
    </node>
    <node concept="2tJIrI" id="2lFfnZ$MYjS" role="jymVt" />
    <node concept="3clFb_" id="2lFfnZ$N66X" role="jymVt">
      <property role="TrG5h" value="addCourses" />
      <node concept="3clFbS" id="2lFfnZ$N670" role="3clF47">
        <node concept="9aQIb" id="2lFfnZ$Nr$M" role="3cqZAp">
          <node concept="3clFbS" id="2lFfnZ$Nr$N" role="9aQI4">
            <node concept="3cpWs8" id="2lFfnZ$OiP1" role="3cqZAp">
              <node concept="3cpWsn" id="2lFfnZ$OiP2" role="3cpWs9">
                <property role="TrG5h" value="myCourse" />
                <node concept="3uibUv" id="2lFfnZ$OiP3" role="1tU5fm">
                  <ref role="3uigEE" node="2lFfnZ$MNHC" resolve="Schedule.Course" />
                </node>
                <node concept="2ShNRf" id="2lFfnZ$Ok6A" role="33vP2m">
                  <node concept="HV5vD" id="2lFfnZ$OlRl" role="2ShVmc">
                    <ref role="HV5vE" node="2lFfnZ$MNHC" resolve="Schedule.Course" />
                  </node>
                </node>
                <node concept="17Uvod" id="2lFfnZ$XeI_" role="lGtFl">
                  <property role="2qtEX9" value="name" />
                  <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                  <node concept="3zFVjK" id="2lFfnZ$XeIA" role="3zH0cK">
                    <node concept="3clFbS" id="2lFfnZ$XeIB" role="2VODD2">
                      <node concept="3clFbF" id="2lFfnZ$Xjfp" role="3cqZAp">
                        <node concept="2OqwBi" id="2lFfnZ$XkzH" role="3clFbG">
                          <node concept="1iwH7S" id="2lFfnZ$Xjfo" role="2Oq$k0" />
                          <node concept="2piZGk" id="2lFfnZ$Xl5$" role="2OqNvi">
                            <node concept="Xl_RD" id="2lFfnZ$XlLR" role="2piZGb">
                              <property role="Xl_RC" value="mycourse" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$OnA5" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$Oq90" role="3clFbG">
                <node concept="Xl_RD" id="2lFfnZ$OqI8" role="37vLTx">
                  <property role="Xl_RC" value="swen424" />
                  <node concept="17Uvod" id="2lFfnZ$OxTa" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                    <node concept="3zFVjK" id="2lFfnZ$OxTb" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$OxTc" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$OzYW" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$O$dp" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$OzYV" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$OGEc" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$OokA" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$OnA3" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$OiP2" resolve="myCourse" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$OoR$" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$MS_o" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$Os1a" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$OxcG" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$OxOE" role="37vLTx">
                  <property role="3cmrfH" value="1" />
                  <node concept="17Uvod" id="2lFfnZ$OH5O" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$OH5P" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$OH5Q" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$OIjI" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$OIAA" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$OIjH" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$ORHs" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXyVp" resolve="difficulty" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$OsE$" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$Os18" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$OiP2" resolve="myCourse" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$Otdy" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$MUJq" resolve="levelOfDifficulty" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="9aQIb" id="2lFfnZ$OWqw" role="3cqZAp">
              <node concept="3clFbS" id="2lFfnZ$OWqy" role="9aQI4">
                <node concept="3cpWs8" id="2lFfnZ$OYBL" role="3cqZAp">
                  <node concept="3cpWsn" id="2lFfnZ$OYBM" role="3cpWs9">
                    <property role="TrG5h" value="activityObj" />
                    <node concept="3uibUv" id="2lFfnZ$OYBN" role="1tU5fm">
                      <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
                    </node>
                    <node concept="2ShNRf" id="2lFfnZ$OZQp" role="33vP2m">
                      <node concept="HV5vD" id="2lFfnZ$P1B8" role="2ShVmc">
                        <ref role="HV5vE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
                      </node>
                    </node>
                    <node concept="17Uvod" id="2lFfnZ$WxtS" role="lGtFl">
                      <property role="2qtEX9" value="name" />
                      <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                      <node concept="3zFVjK" id="2lFfnZ$WxtT" role="3zH0cK">
                        <node concept="3clFbS" id="2lFfnZ$WxtU" role="2VODD2">
                          <node concept="3clFbF" id="2lFfnZ$W$uc" role="3cqZAp">
                            <node concept="2OqwBi" id="2lFfnZ$W_il" role="3clFbG">
                              <node concept="1iwH7S" id="2lFfnZ$W$ub" role="2Oq$k0" />
                              <node concept="2piZGk" id="2lFfnZ$W_Kz" role="2OqNvi">
                                <node concept="Xl_RD" id="2lFfnZ$WAjX" role="2piZGb">
                                  <property role="Xl_RC" value="obj" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZA0Md6" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZA1bgV" role="3clFbG">
                    <node concept="Xl_RD" id="2lFfnZA1bQ5" role="37vLTx">
                      <property role="Xl_RC" value="class" />
                    </node>
                    <node concept="2OqwBi" id="2lFfnZA0NSv" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZA0Md4" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZA0XJd" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ$P2Nd" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ$P5h5" role="3clFbG">
                    <node concept="Xl_RD" id="2lFfnZ$P5Qt" role="37vLTx">
                      <property role="Xl_RC" value="lecture" />
                      <node concept="17Uvod" id="2lFfnZ$P95f" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                        <node concept="3zFVjK" id="2lFfnZ$P95g" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ$P95h" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ$PaVm" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ$Pb9N" role="3clFbG">
                                <node concept="30H73N" id="2lFfnZ$PaVl" role="2Oq$k0" />
                                <node concept="3TrcHB" id="2lFfnZ$PJXs" role="2OqNvi">
                                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ$P3s0" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ$P2Nb" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ$P3Zp" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ$XGj7" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ$Y6mp" role="3clFbG">
                    <node concept="3cmrfG" id="2lFfnZ$Y6Z2" role="37vLTx">
                      <property role="3cmrfH" value="10" />
                      <node concept="17Uvod" id="2lFfnZ_I_LN" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                        <node concept="3zFVjK" id="2lFfnZ_I_LO" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_I_LP" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_IFeU" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_II2G" role="3clFbG">
                                <node concept="30H73N" id="2lFfnZ_IHJO" role="2Oq$k0" />
                                <node concept="3TrcHB" id="2lFfnZ_IIPW" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXyaT" resolve="startHour" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ$XH8s" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ$XGj5" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ$XR4k" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ$Y8uV" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ$YyzF" role="3clFbG">
                    <node concept="3cmrfG" id="2lFfnZ$Yzbh" role="37vLTx">
                      <property role="3cmrfH" value="10" />
                      <node concept="17Uvod" id="2lFfnZ_IKjp" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                        <node concept="3zFVjK" id="2lFfnZ_IKjq" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_IKjr" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_ILx_" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_ILOt" role="3clFbG">
                                <node concept="30H73N" id="2lFfnZ_ILx$" role="2Oq$k0" />
                                <node concept="3TrcHB" id="2lFfnZ_IYgZ" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXyaX" resolve="startMin" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ$Y9ks" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ$Y8uT" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ$Yjgk" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ$Y$Gp" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ$YZZZ" role="3clFbG">
                    <node concept="3cmrfG" id="2lFfnZ$Z0Cd" role="37vLTx">
                      <property role="3cmrfH" value="10" />
                      <node concept="17Uvod" id="2lFfnZ_IZ2H" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                        <node concept="3zFVjK" id="2lFfnZ_IZ2I" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_IZ2J" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_J0pB" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_J0Gv" role="3clFbG">
                                <node concept="30H73N" id="2lFfnZ_J0pA" role="2Oq$k0" />
                                <node concept="3TrcHB" id="2lFfnZ_Jd0q" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCOj" resolve="endHour" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ$YA5m" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ$Y_sJ" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ$YK1D" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ$Z2d3" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ$ZsQ9" role="3clFbG">
                    <node concept="3cmrfG" id="2lFfnZ$Zttk" role="37vLTx">
                      <property role="3cmrfH" value="10" />
                      <node concept="17Uvod" id="2lFfnZ_JdBg" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                        <node concept="3zFVjK" id="2lFfnZ_JdBh" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_JdBi" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_JeRE" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_Jfay" role="3clFbG">
                                <node concept="30H73N" id="2lFfnZ_JeRD" role="2Oq$k0" />
                                <node concept="3TrcHB" id="2lFfnZ_Jrut" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCOq" resolve="endMin" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ$Z3BT" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ$Z2X_" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ$ZdzL" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ$ZuYO" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ$ZSho" role="3clFbG">
                    <node concept="3clFbT" id="2lFfnZ$ZSQK" role="37vLTx">
                      <node concept="17Uvod" id="2lFfnZ_Js5j" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                        <node concept="3zFVjK" id="2lFfnZ_Js5k" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_Js5l" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_Jt4j" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_JPdF" role="3clFbG">
                                <node concept="2OqwBi" id="2lFfnZ_Jti7" role="2Oq$k0">
                                  <node concept="30H73N" id="2lFfnZ_Jt4i" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="2lFfnZ_JDoK" role="2OqNvi">
                                    <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                                  </node>
                                </node>
                                <node concept="3TrcHB" id="2lFfnZ_K16D" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCR5" resolve="Monday" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ$ZwNg" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ$ZuYM" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ$ZEJ8" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$FQX5" resolve="mon" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ$ZTDh" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ_0hD1" role="3clFbG">
                    <node concept="3clFbT" id="2lFfnZ_0idm" role="37vLTx">
                      <node concept="17Uvod" id="2lFfnZ_K1rj" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                        <node concept="3zFVjK" id="2lFfnZ_K1rk" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_K1rl" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_K2jd" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_KqpX" role="3clFbG">
                                <node concept="2OqwBi" id="2lFfnZ_K2x1" role="2Oq$k0">
                                  <node concept="30H73N" id="2lFfnZ_K2jc" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="2lFfnZ_Ke_K" role="2OqNvi">
                                    <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                                  </node>
                                </node>
                                <node concept="3TrcHB" id="2lFfnZ_KAi8" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCR8" resolve="Tuesday" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ$ZU$P" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ$ZTDf" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ_04rd" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$FVd1" resolve="tue" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ_0j0Y" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ_0EZC" role="3clFbG">
                    <node concept="3clFbT" id="2lFfnZ_0FzX" role="37vLTx">
                      <node concept="17Uvod" id="2lFfnZ_KAAM" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                        <node concept="3zFVjK" id="2lFfnZ_KAAN" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_KAAO" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_KBr2" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_KZyw" role="3clFbG">
                                <node concept="2OqwBi" id="2lFfnZ_KBCQ" role="2Oq$k0">
                                  <node concept="30H73N" id="2lFfnZ_KBr1" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="2lFfnZ_KNHW" role="2OqNvi">
                                    <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                                  </node>
                                </node>
                                <node concept="3TrcHB" id="2lFfnZ_LbqF" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCRd" resolve="Wednesday" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ_0jWI" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ_0j0W" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ_0tN6" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$FZEv" resolve="wed" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ_0GnL" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ_14mc" role="3clFbG">
                    <node concept="3clFbT" id="2lFfnZ_14UW" role="37vLTx">
                      <node concept="17Uvod" id="2lFfnZ_LbR9" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                        <node concept="3zFVjK" id="2lFfnZ_LbRa" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_LbRb" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_Lczn" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_L$KZ" role="3clFbG">
                                <node concept="2OqwBi" id="2lFfnZ_LcLb" role="2Oq$k0">
                                  <node concept="30H73N" id="2lFfnZ_Lczl" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="2lFfnZ_LoQY" role="2OqNvi">
                                    <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                                  </node>
                                </node>
                                <node concept="3TrcHB" id="2lFfnZ_LKDa" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCRk" resolve="Thursday" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ_0HjH" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ_0GnJ" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ_0Ra5" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$G4Nq" resolve="thu" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ_15IW" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ_1tCF" role="3clFbG">
                    <node concept="3clFbT" id="2lFfnZ_1ud0" role="37vLTx">
                      <node concept="17Uvod" id="2lFfnZ_LL5C" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                        <node concept="3zFVjK" id="2lFfnZ_LL5D" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_LL5E" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_LLLQ" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_Ma0n" role="3clFbG">
                                <node concept="2OqwBi" id="2lFfnZ_LLZE" role="2Oq$k0">
                                  <node concept="30H73N" id="2lFfnZ_LLLO" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="2lFfnZ_LY5O" role="2OqNvi">
                                    <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                                  </node>
                                </node>
                                <node concept="3TrcHB" id="2lFfnZ_MlSy" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCRt" resolve="Friday" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ_16_m" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ_15IU" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ_1gs9" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$G92r" resolve="fri" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ_1v1c" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ_24MU" role="3clFbG">
                    <node concept="3clFbT" id="2lFfnZ_25nf" role="37vLTx">
                      <node concept="17Uvod" id="2lFfnZ_Mml0" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                        <node concept="3zFVjK" id="2lFfnZ_Mml1" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_Mml2" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_Mn9g" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_MJiO" role="3clFbG">
                                <node concept="2OqwBi" id="2lFfnZ_Mnn4" role="2Oq$k0">
                                  <node concept="30H73N" id="2lFfnZ_Mn9f" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="2lFfnZ_MzrN" role="2OqNvi">
                                    <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                                  </node>
                                </node>
                                <node concept="3TrcHB" id="2lFfnZ_MVgN" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCRC" resolve="Saturday" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ_1vRM" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ_1v1a" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ_1DJR" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$GdfV" resolve="sat" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="2lFfnZ_26bB" role="3cqZAp">
                  <node concept="37vLTI" id="2lFfnZ_2uak" role="3clFbG">
                    <node concept="3clFbT" id="2lFfnZ_2uID" role="37vLTx">
                      <node concept="17Uvod" id="2lFfnZ_MV_t" role="lGtFl">
                        <property role="2qtEX9" value="value" />
                        <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                        <node concept="3zFVjK" id="2lFfnZ_MV_u" role="3zH0cK">
                          <node concept="3clFbS" id="2lFfnZ_MV_v" role="2VODD2">
                            <node concept="3clFbF" id="2lFfnZ_MWtn" role="3cqZAp">
                              <node concept="2OqwBi" id="2lFfnZ_NkBc" role="3clFbG">
                                <node concept="2OqwBi" id="2lFfnZ_MWFb" role="2Oq$k0">
                                  <node concept="30H73N" id="2lFfnZ_MWtm" role="2Oq$k0" />
                                  <node concept="3TrEf2" id="2lFfnZ_N8JU" role="2OqNvi">
                                    <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                                  </node>
                                </node>
                                <node concept="3TrcHB" id="2lFfnZ_Nwvn" role="2OqNvi">
                                  <ref role="3TsBF5" to="ky5v:6miNywCXCRP" resolve="Sunday" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2lFfnZ_272p" role="37vLTJ">
                      <node concept="37vLTw" id="2lFfnZ_26b_" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                      <node concept="2OwXpG" id="2lFfnZ_2gTc" role="2OqNvi">
                        <ref role="2Oxat5" node="2lFfnZ$GhvA" resolve="sun" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="3rIOsIr$57N" role="3cqZAp">
                  <node concept="2OqwBi" id="3rIOsIr$r20" role="3clFbG">
                    <node concept="2OqwBi" id="3rIOsIr$6aV" role="2Oq$k0">
                      <node concept="37vLTw" id="3rIOsIr$57L" role="2Oq$k0">
                        <ref role="3cqZAo" node="2lFfnZ$OiP2" resolve="myCourse" />
                      </node>
                      <node concept="2OwXpG" id="3rIOsIr$g6v" role="2OqNvi">
                        <ref role="2Oxat5" node="3rIOsIrzsnn" resolve="classes" />
                      </node>
                    </node>
                    <node concept="liA8E" id="3rIOsIr$WY4" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                      <node concept="37vLTw" id="3rIOsIr$XHI" role="37wK5m">
                        <ref role="3cqZAo" node="2lFfnZ$OYBM" resolve="activityObj" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="1WS0z7" id="2lFfnZ$PoU7" role="lGtFl">
                <node concept="3JmXsc" id="2lFfnZ$PoU8" role="3Jn$fo">
                  <node concept="3clFbS" id="2lFfnZ$PoU9" role="2VODD2">
                    <node concept="3clFbF" id="2lFfnZ$PquL" role="3cqZAp">
                      <node concept="2OqwBi" id="2lFfnZ$Prny" role="3clFbG">
                        <node concept="30H73N" id="2lFfnZ$PquK" role="2Oq$k0" />
                        <node concept="3Tsc0h" id="2lFfnZ$P_S9" role="2OqNvi">
                          <ref role="3TtcxE" to="ky5v:6miNywCXCSw" resolve="classes" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="3rIOsIr_0yq" role="3cqZAp">
              <node concept="2OqwBi" id="3rIOsIr_3qA" role="3clFbG">
                <node concept="2OqwBi" id="3rIOsIr_1mc" role="2Oq$k0">
                  <node concept="Xjq3P" id="3rIOsIr_0yo" role="2Oq$k0" />
                  <node concept="2OwXpG" id="3rIOsIr_27b" role="2OqNvi">
                    <ref role="2Oxat5" node="3rIOsIry918" resolve="courses" />
                  </node>
                </node>
                <node concept="liA8E" id="3rIOsIr_rHa" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                  <node concept="37vLTw" id="3rIOsIr_sHM" role="37wK5m">
                    <ref role="3cqZAo" node="2lFfnZ$OiP2" resolve="myCourse" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="2lFfnZ$Ogq$" role="lGtFl">
            <node concept="3JmXsc" id="2lFfnZ$OgqB" role="3Jn$fo">
              <node concept="3clFbS" id="2lFfnZ$OgqC" role="2VODD2">
                <node concept="3clFbF" id="2lFfnZ$OgqI" role="3cqZAp">
                  <node concept="2OqwBi" id="2lFfnZ$OgqD" role="3clFbG">
                    <node concept="3Tsc0h" id="2lFfnZ$OgqG" role="2OqNvi">
                      <ref role="3TtcxE" to="ky5v:2t50$NbHYD5" resolve="courses" />
                    </node>
                    <node concept="30H73N" id="2lFfnZ$OgqH" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="2lFfnZ$N2IU" role="1B3o_S" />
      <node concept="3cqZAl" id="2lFfnZ$N5yb" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2lFfnZ$BWFz" role="jymVt" />
    <node concept="3clFb_" id="2lFfnZ$AHa$" role="jymVt">
      <property role="TrG5h" value="addMealTime" />
      <node concept="3clFbS" id="2lFfnZ$AHaB" role="3clF47">
        <node concept="9aQIb" id="2lFfnZ$D5jU" role="3cqZAp">
          <node concept="3clFbS" id="2lFfnZ$D5jW" role="9aQI4">
            <node concept="3cpWs8" id="2lFfnZ$D6XF" role="3cqZAp">
              <node concept="3cpWsn" id="2lFfnZ$D6XG" role="3cpWs9">
                <property role="TrG5h" value="meal" />
                <node concept="3uibUv" id="2lFfnZ$D6XH" role="1tU5fm">
                  <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
                </node>
                <node concept="2ShNRf" id="2lFfnZ$D83c" role="33vP2m">
                  <node concept="HV5vD" id="2lFfnZ$GCu8" role="2ShVmc">
                    <ref role="HV5vE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
                  </node>
                </node>
                <node concept="17Uvod" id="2lFfnZ$WnsH" role="lGtFl">
                  <property role="2qtEX9" value="name" />
                  <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                  <node concept="3zFVjK" id="2lFfnZ$WnsI" role="3zH0cK">
                    <node concept="3clFbS" id="2lFfnZ$WnsJ" role="2VODD2">
                      <node concept="3clFbF" id="2lFfnZ$WpPl" role="3cqZAp">
                        <node concept="2OqwBi" id="2lFfnZ$Wr_M" role="3clFbG">
                          <node concept="1iwH7S" id="2lFfnZ$WpPk" role="2Oq$k0" />
                          <node concept="2piZGk" id="2lFfnZ$Wsp5" role="2OqNvi">
                            <node concept="Xl_RD" id="2lFfnZ$Wt5o" role="2piZGb">
                              <property role="Xl_RC" value="obj" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ_ZVcX" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZA0kdf" role="3clFbG">
                <node concept="Xl_RD" id="2lFfnZA0l09" role="37vLTx">
                  <property role="Xl_RC" value="Meal" />
                </node>
                <node concept="2OqwBi" id="2lFfnZ_ZWSL" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ_ZVcV" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZA06WD" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$M0OU" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$M45F" role="3clFbG">
                <node concept="Xl_RD" id="2lFfnZ$M4EN" role="37vLTx">
                  <property role="Xl_RC" value="activityName" />
                  <node concept="17Uvod" id="2lFfnZ$M53D" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                    <node concept="3zFVjK" id="2lFfnZ$M53E" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$M53F" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$M6PT" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$M74m" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$M6PS" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$M8pr" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$M2mL" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$M0OS" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$M2Of" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$GDMV" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$GIqI" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$HuoS" role="37vLTx">
                  <property role="3cmrfH" value="10" />
                  <node concept="17Uvod" id="2lFfnZ$HwrZ" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$Hws0" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$Hws1" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$HzKd" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$H$35" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$HzKc" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$HH98" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXyaT" resolve="startHour" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$GEzq" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$GDMT" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$GF6o" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$GJQx" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$GOoP" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$Hv4j" role="37vLTx">
                  <property role="3cmrfH" value="10" />
                  <node concept="17Uvod" id="2lFfnZ$HygF" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$HygG" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$HygH" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$HIrr" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$HIIj" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$HIrq" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$HROm" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXyaX" resolve="startMin" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$GKAL" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$GJQv" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$GL4E" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$GPIr" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$GVoW" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$HvJI" role="37vLTx">
                  <property role="3cmrfH" value="10" />
                  <node concept="17Uvod" id="2lFfnZ$HSA5" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$HSA6" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$HSA7" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$HTWH" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$HUf_" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$HTWG" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$I3dz" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCOj" resolve="endHour" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$GQvh" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$GPIp" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$GRpK" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$GWGz" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$H1gB" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$Hx_g" role="37vLTx">
                  <property role="3cmrfH" value="10" />
                  <node concept="17Uvod" id="2lFfnZ$I4kM" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$I4kN" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$I4kO" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$I5yG" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$I5P$" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$I5yF" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$IeMJ" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCOq" resolve="endMin" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$GXuW" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$GWGx" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$GXWP" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$H2$q" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$H54N" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$H5DV" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$If$u" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$If$v" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$If$w" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$Igs6" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$IxZU" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$IgDU" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$Igs5" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$IprH" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$IyuW" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCR5" resolve="Monday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$H3lD" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$H2$o" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$H3N7" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$FQX5" resolve="mon" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$H6pm" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$H8VF" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$H9w0" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$IyVr" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$IyVs" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$IyVt" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$IzJp" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$IP6f" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$IzXd" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$IzJo" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$IGFc" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$IXGE" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCR8" resolve="Tuesday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$H7cx" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$H6pk" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$H7DZ" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$FVd1" resolve="tue" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$Haex" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$HcPD" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$HdpY" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$IY1l" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$IY1m" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$IY1n" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$IYPj" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$JggH" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$IZ37" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$IYPi" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$J7LT" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$JoRV" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRd" resolve="Wednesday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$Hb1T" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$Haev" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$Hbvn" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$FZEv" resolve="wed" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$He8F" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$HgEX" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$Hhgw" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$JpKF" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$JpKG" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$JpKH" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$Jq$D" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$JG1j" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$JqMt" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$Jq$C" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$Jzws" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$JOHy" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRk" resolve="Thursday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$HeWe" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$He8D" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$HfpG" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$G4Nq" resolve="thu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$Hi0v" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$Hkxt" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$Hl6_" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$JPa1" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$JPa2" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$JPa3" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$JPXZ" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$K7wy" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$JQbN" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$JPXY" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$JYTM" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$Kg7K" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRt" resolve="Friday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$HiL_" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$Hi0t" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$HjfL" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$G92r" resolve="fri" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$HlP0" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$HolU" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$HoVt" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$Kgsr" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$Kgss" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$Kgst" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$Khoh" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$KyJN" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$KhA5" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$Khog" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$Kqkl" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$KFs2" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRC" resolve="Saturday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$HmAK" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$HlOY" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$Hn4D" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$GdfV" resolve="sat" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$HpFO" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$Hsfl" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$HsOt" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$KFKH" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$KFKI" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$KFKJ" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$KG$F" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$KY1l" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$KGMv" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$KG$E" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$KPwu" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$L6BK" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRP" resolve="Sunday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$Hqvw" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$HpFM" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$HqXp" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$GhvA" resolve="sun" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="3rIOsIr_wfJ" role="3cqZAp">
              <node concept="2OqwBi" id="3rIOsIr_yYv" role="3clFbG">
                <node concept="2OqwBi" id="3rIOsIr_xoz" role="2Oq$k0">
                  <node concept="Xjq3P" id="3rIOsIr_xdU" role="2Oq$k0" />
                  <node concept="2OwXpG" id="3rIOsIr_xEW" role="2OqNvi">
                    <ref role="2Oxat5" node="3rIOsIrxzmG" resolve="activities" />
                  </node>
                </node>
                <node concept="liA8E" id="3rIOsIr_VfH" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                  <node concept="37vLTw" id="3rIOsIr_Wd5" role="37wK5m">
                    <ref role="3cqZAo" node="2lFfnZ$D6XG" resolve="meal" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="2lFfnZ$E2q1" role="lGtFl">
            <node concept="3JmXsc" id="2lFfnZ$E2q4" role="3Jn$fo">
              <node concept="3clFbS" id="2lFfnZ$E2q5" role="2VODD2">
                <node concept="3clFbF" id="2lFfnZ$E2qb" role="3cqZAp">
                  <node concept="2OqwBi" id="2lFfnZ$EgPb" role="3clFbG">
                    <node concept="30H73N" id="2lFfnZ$E2qa" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="2lFfnZ$Eh7g" role="2OqNvi">
                      <ref role="3TtcxE" to="ky5v:6miNywCXCSr" resolve="MealTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="2lFfnZ$AFJk" role="1B3o_S" />
      <node concept="3cqZAl" id="2lFfnZ$AGUW" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2lFfnZ$LB3S" role="jymVt" />
    <node concept="3clFb_" id="2lFfnZ$LCHI" role="jymVt">
      <property role="TrG5h" value="addRestTime" />
      <node concept="3clFbS" id="2lFfnZ$LCHJ" role="3clF47">
        <node concept="9aQIb" id="2lFfnZ$LCHK" role="3cqZAp">
          <node concept="3clFbS" id="2lFfnZ$LCHL" role="9aQI4">
            <node concept="3cpWs8" id="2lFfnZ$LCHM" role="3cqZAp">
              <node concept="3cpWsn" id="2lFfnZ$LCHN" role="3cpWs9">
                <property role="TrG5h" value="rest" />
                <node concept="3uibUv" id="2lFfnZ$LCHO" role="1tU5fm">
                  <ref role="3uigEE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
                </node>
                <node concept="2ShNRf" id="2lFfnZ$LCHP" role="33vP2m">
                  <node concept="HV5vD" id="2lFfnZ$LCHQ" role="2ShVmc">
                    <ref role="HV5vE" node="2lFfnZ$Bbpm" resolve="Schedule.ActivityTime" />
                  </node>
                </node>
                <node concept="17Uvod" id="2lFfnZ$WhSc" role="lGtFl">
                  <property role="2qtEX9" value="name" />
                  <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
                  <node concept="3zFVjK" id="2lFfnZ$WhSd" role="3zH0cK">
                    <node concept="3clFbS" id="2lFfnZ$WhSe" role="2VODD2">
                      <node concept="3clFbF" id="2lFfnZ$Wk2z" role="3cqZAp">
                        <node concept="2OqwBi" id="2lFfnZ$WlVS" role="3clFbG">
                          <node concept="1iwH7S" id="2lFfnZ$Wk2y" role="2Oq$k0" />
                          <node concept="2piZGk" id="2lFfnZ$Wmog" role="2OqNvi">
                            <node concept="Xl_RD" id="2lFfnZ$Wn4z" role="2piZGb">
                              <property role="Xl_RC" value="obj" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZA0mEf" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZA0JJz" role="3clFbG">
                <node concept="Xl_RD" id="2lFfnZA0Ky_" role="37vLTx">
                  <property role="Xl_RC" value="Rest" />
                </node>
                <node concept="2OqwBi" id="2lFfnZA0olC" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZA0mEd" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZA0ypV" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ_ZNpp" resolve="type" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$Mae1" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$Mdv8" role="3clFbG">
                <node concept="Xl_RD" id="2lFfnZ$Me4o" role="37vLTx">
                  <property role="Xl_RC" value="activityName" />
                  <node concept="17Uvod" id="2lFfnZ$Mef0" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1070475926800/1070475926801" />
                    <node concept="3zFVjK" id="2lFfnZ$Mef1" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$Mef2" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$MfzL" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$MfMe" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$MfzK" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$MoUB" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$MbF$" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$MadZ" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$Mc9t" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$LWXk" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCHR" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCHS" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$LCHT" role="37vLTx">
                  <property role="3cmrfH" value="10" />
                  <node concept="17Uvod" id="2lFfnZ$LCHU" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$LCHV" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCHW" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCHX" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCHY" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$LCHZ" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$LCI0" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXyaT" resolve="startHour" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCI1" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCI2" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCI3" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$BjjR" resolve="startHour" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCI4" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCI5" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$LCI6" role="37vLTx">
                  <property role="3cmrfH" value="10" />
                  <node concept="17Uvod" id="2lFfnZ$LCI7" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$LCI8" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCI9" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCIa" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCIb" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$LCIc" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$LCId" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXyaX" resolve="startMin" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCIe" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCIf" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCIg" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$EiZx" resolve="startMin" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCIh" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCIi" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$LCIj" role="37vLTx">
                  <property role="3cmrfH" value="10" />
                  <node concept="17Uvod" id="2lFfnZ$LCIk" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$LCIl" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCIm" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCIn" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCIo" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$LCIp" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$LCIq" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCOj" resolve="endHour" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCIr" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCIs" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCIt" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$BlOI" resolve="endHour" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCIu" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCIv" role="3clFbG">
                <node concept="3cmrfG" id="2lFfnZ$LCIw" role="37vLTx">
                  <property role="3cmrfH" value="10" />
                  <node concept="17Uvod" id="2lFfnZ$LCIx" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580320020/1068580320021" />
                    <node concept="3zFVjK" id="2lFfnZ$LCIy" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCIz" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCI$" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCI_" role="3clFbG">
                            <node concept="30H73N" id="2lFfnZ$LCIA" role="2Oq$k0" />
                            <node concept="3TrcHB" id="2lFfnZ$LCIB" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCOq" resolve="endMin" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCIC" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCID" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCIE" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$EldF" resolve="endMin" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCIF" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCIG" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$LCIH" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$LCII" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$LCIJ" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCIK" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCIL" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCIM" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$LCIN" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$LCIO" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$LCIP" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$LCIQ" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCR5" resolve="Monday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCIR" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCIS" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCIT" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$FQX5" resolve="mon" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCIU" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCIV" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$LCIW" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$LCIX" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$LCIY" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCIZ" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCJ0" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCJ1" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$LCJ2" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$LCJ3" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$LCJ4" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$LCJ5" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCR8" resolve="Tuesday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCJ6" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCJ7" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCJ8" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$FVd1" resolve="tue" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCJ9" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCJa" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$LCJb" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$LCJc" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$LCJd" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCJe" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCJf" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCJg" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$LCJh" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$LCJi" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$LCJj" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$LCJk" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRd" resolve="Wednesday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCJl" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCJm" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCJn" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$FZEv" resolve="wed" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCJo" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCJp" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$LCJq" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$LCJr" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$LCJs" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCJt" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCJu" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCJv" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$LCJw" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$LCJx" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$LCJy" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$LCJz" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRk" resolve="Thursday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCJ$" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCJ_" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCJA" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$G4Nq" resolve="thu" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCJB" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCJC" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$LCJD" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$LCJE" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$LCJF" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCJG" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCJH" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCJI" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$LCJJ" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$LCJK" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$LCJL" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$LCJM" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRt" resolve="Friday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCJN" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCJO" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCJP" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$G92r" resolve="fri" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCJQ" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCJR" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$LCJS" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$LCJT" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$LCJU" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCJV" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCJW" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCJX" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$LCJY" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$LCJZ" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$LCK0" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$LCK1" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRC" resolve="Saturday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCK2" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCK3" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCK4" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$GdfV" resolve="sat" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2lFfnZ$LCK5" role="3cqZAp">
              <node concept="37vLTI" id="2lFfnZ$LCK6" role="3clFbG">
                <node concept="3clFbT" id="2lFfnZ$LCK7" role="37vLTx">
                  <node concept="17Uvod" id="2lFfnZ$LCK8" role="lGtFl">
                    <property role="2qtEX9" value="value" />
                    <property role="P4ACc" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123137/1068580123138" />
                    <node concept="3zFVjK" id="2lFfnZ$LCK9" role="3zH0cK">
                      <node concept="3clFbS" id="2lFfnZ$LCKa" role="2VODD2">
                        <node concept="3clFbF" id="2lFfnZ$LCKb" role="3cqZAp">
                          <node concept="2OqwBi" id="2lFfnZ$LCKc" role="3clFbG">
                            <node concept="2OqwBi" id="2lFfnZ$LCKd" role="2Oq$k0">
                              <node concept="30H73N" id="2lFfnZ$LCKe" role="2Oq$k0" />
                              <node concept="3TrEf2" id="2lFfnZ$LCKf" role="2OqNvi">
                                <ref role="3Tt5mk" to="ky5v:6miNywCXCSc" resolve="dayOfWeek" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="2lFfnZ$LCKg" role="2OqNvi">
                              <ref role="3TsBF5" to="ky5v:6miNywCXCRP" resolve="Sunday" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2lFfnZ$LCKh" role="37vLTJ">
                  <node concept="37vLTw" id="2lFfnZ$LCKi" role="2Oq$k0">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                  <node concept="2OwXpG" id="2lFfnZ$LCKj" role="2OqNvi">
                    <ref role="2Oxat5" node="2lFfnZ$GhvA" resolve="sun" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="3rIOsIrzAq9" role="3cqZAp">
              <node concept="2OqwBi" id="3rIOsIrzD34" role="3clFbG">
                <node concept="2OqwBi" id="3rIOsIrzBtg" role="2Oq$k0">
                  <node concept="Xjq3P" id="3rIOsIrzAq7" role="2Oq$k0" />
                  <node concept="2OwXpG" id="3rIOsIrzBJe" role="2OqNvi">
                    <ref role="2Oxat5" node="3rIOsIrxzmG" resolve="activities" />
                  </node>
                </node>
                <node concept="liA8E" id="3rIOsIr$1jJ" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object):boolean" resolve="add" />
                  <node concept="37vLTw" id="3rIOsIr$2bR" role="37wK5m">
                    <ref role="3cqZAo" node="2lFfnZ$LCHN" resolve="rest" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="2lFfnZ$LCKp" role="lGtFl">
            <node concept="3JmXsc" id="2lFfnZ$LCKq" role="3Jn$fo">
              <node concept="3clFbS" id="2lFfnZ$LCKr" role="2VODD2">
                <node concept="3clFbF" id="2lFfnZ$LCKs" role="3cqZAp">
                  <node concept="2OqwBi" id="2lFfnZ$LCKt" role="3clFbG">
                    <node concept="30H73N" id="2lFfnZ$LCKu" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="2lFfnZ$LRXC" role="2OqNvi">
                      <ref role="3TtcxE" to="ky5v:6miNywCXCSz" resolve="RestTime" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="2lFfnZ$LCKw" role="1B3o_S" />
      <node concept="3cqZAl" id="2lFfnZ$LCKx" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="2lFfnZ$LBea" role="jymVt" />
    <node concept="2tJIrI" id="2lFfnZ$_hTa" role="jymVt" />
    <node concept="2YIFZL" id="2lFfnZ$$V1U" role="jymVt">
      <property role="TrG5h" value="main" />
      <node concept="37vLTG" id="2lFfnZ$$V1V" role="3clF46">
        <property role="TrG5h" value="args" />
        <node concept="10Q1$e" id="2lFfnZ$$V1W" role="1tU5fm">
          <node concept="17QB3L" id="2lFfnZ$$V1X" role="10Q1$1" />
        </node>
      </node>
      <node concept="3cqZAl" id="2lFfnZ$$V1Y" role="3clF45" />
      <node concept="3Tm1VV" id="2lFfnZ$$V1Z" role="1B3o_S" />
      <node concept="3clFbS" id="2lFfnZ$$V20" role="3clF47">
        <node concept="3clFbF" id="7bbHeTWfy0f" role="3cqZAp">
          <node concept="2YIFZM" id="7bbHeTWfy1f" role="3clFbG">
            <ref role="37wK5l" to="dxuu:~SwingUtilities.invokeLater(java.lang.Runnable):void" resolve="invokeLater" />
            <ref role="1Pybhc" to="dxuu:~SwingUtilities" resolve="SwingUtilities" />
            <node concept="2ShNRf" id="7bbHeTWfy_5" role="37wK5m">
              <node concept="YeOm9" id="7bbHeTWfBgW" role="2ShVmc">
                <node concept="1Y3b0j" id="7bbHeTWfBgZ" role="YeSDq">
                  <property role="2bfB8j" value="true" />
                  <ref role="1Y3XeK" to="wyt6:~Runnable" resolve="Runnable" />
                  <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                  <node concept="3Tm1VV" id="7bbHeTWfBh0" role="1B3o_S" />
                  <node concept="3clFb_" id="7bbHeTWfBh2" role="jymVt">
                    <property role="TrG5h" value="run" />
                    <node concept="3Tm1VV" id="7bbHeTWfBh3" role="1B3o_S" />
                    <node concept="3cqZAl" id="7bbHeTWfBh5" role="3clF45" />
                    <node concept="3clFbS" id="7bbHeTWfBh6" role="3clF47">
                      <node concept="3clFbF" id="7bbHeTWfWKK" role="3cqZAp">
                        <node concept="2OqwBi" id="7bbHeTWg0aW" role="3clFbG">
                          <node concept="2ShNRf" id="7bbHeTWfWKI" role="2Oq$k0">
                            <node concept="1pGfFk" id="7bbHeTWfZ8a" role="2ShVmc">
                              <ref role="37wK5l" node="2lFfnZ_gNmR" resolve="Schedule" />
                            </node>
                          </node>
                          <node concept="liA8E" id="7bbHeTWg2kq" role="2OqNvi">
                            <ref role="37wK5l" to="z60i:~Window.setVisible(boolean):void" resolve="setVisible" />
                            <node concept="3clFbT" id="7bbHeTWg3Xi" role="37wK5m">
                              <property role="3clFbU" value="true" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="2OB0AerCfAG" role="jymVt" />
    <node concept="312cEu" id="2OB0AerCHzQ" role="jymVt">
      <property role="2bfB8j" value="true" />
      <property role="TrG5h" value="TaskCreatePanel" />
      <node concept="312cEg" id="2OB0AerDjil" role="jymVt">
        <property role="TrG5h" value="categoryLabel" />
        <node concept="3Tm6S6" id="2OB0AerDfKN" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerDjcS" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JLabel" resolve="JLabel" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerDo1d" role="jymVt">
        <property role="TrG5h" value="categoryField" />
        <node concept="3Tm6S6" id="2OB0AerDkUc" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerDnVK" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerDsCu" role="jymVt">
        <property role="TrG5h" value="comboForType" />
        <node concept="3Tm6S6" id="2OB0AerDpi2" role="1B3o_S" />
        <node concept="3uibUv" id="5t_d$dJppov" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerDwSI" role="jymVt">
        <property role="TrG5h" value="nameLabel" />
        <node concept="3Tm6S6" id="2OB0AerDtRB" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerDwNh" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JLabel" resolve="JLabel" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerD_7N" role="jymVt">
        <property role="TrG5h" value="nameField" />
        <node concept="3Tm6S6" id="2OB0AerDy2d" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerD$yf" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerDEvG" role="jymVt">
        <property role="TrG5h" value="comboFordifficulty" />
        <node concept="3Tm6S6" id="2OB0AerDARv" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerDDMv" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JComboBox" resolve="JComboBox" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerDKJ_" role="jymVt">
        <property role="TrG5h" value="hoursLabel" />
        <node concept="3Tm6S6" id="2OB0AerDFJU" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerDKE8" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JLabel" resolve="JLabel" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerDPYa" role="jymVt">
        <property role="TrG5h" value="hoursField" />
        <node concept="3Tm6S6" id="2OB0AerDMYm" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerDPI6" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerDT$1" role="jymVt">
        <property role="TrG5h" value="dueLabel" />
        <node concept="3Tm6S6" id="2OB0AerDQSE" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerDTty" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JLabel" resolve="JLabel" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerDX_I" role="jymVt">
        <property role="TrG5h" value="dueField" />
        <node concept="3Tm6S6" id="2OB0AerDU_S" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerDXg6" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerE1fl" role="jymVt">
        <property role="TrG5h" value="courseLabel" />
        <node concept="3Tm6S6" id="2OB0AerDYCS" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerE0Dc" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JLabel" resolve="JLabel" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerE5uu" role="jymVt">
        <property role="TrG5h" value="courseField" />
        <node concept="3Tm6S6" id="2OB0AerE2ou" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerE5p1" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JTextField" resolve="JTextField" />
        </node>
      </node>
      <node concept="312cEg" id="2OB0AerEa4l" role="jymVt">
        <property role="TrG5h" value="scheduleButton" />
        <node concept="3Tm6S6" id="2OB0AerE7eQ" role="1B3o_S" />
        <node concept="3uibUv" id="2OB0AerE9Iv" role="1tU5fm">
          <ref role="3uigEE" to="dxuu:~JButton" resolve="JButton" />
        </node>
      </node>
      <node concept="2tJIrI" id="2OB0AerE6db" role="jymVt" />
      <node concept="3clFbW" id="2OB0AerD3_v" role="jymVt">
        <node concept="3cqZAl" id="2OB0AerD3_w" role="3clF45" />
        <node concept="3clFbS" id="2OB0AerD3_y" role="3clF47">
          <node concept="3cpWs8" id="2OB0AerD5cL" role="3cqZAp">
            <node concept="3cpWsn" id="2OB0AerD5cM" role="3cpWs9">
              <property role="TrG5h" value="typeStrings" />
              <node concept="10Q1$e" id="2OB0AerD5cN" role="1tU5fm">
                <node concept="3uibUv" id="2OB0AerD5cO" role="10Q1$1">
                  <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                </node>
              </node>
              <node concept="2BsdOp" id="2OB0AerDaeh" role="33vP2m">
                <node concept="Xl_RD" id="2OB0AerDb8s" role="2BsfMF">
                  <property role="Xl_RC" value="Project" />
                </node>
                <node concept="Xl_RD" id="2OB0AerDc5Z" role="2BsfMF">
                  <property role="Xl_RC" value="Assignment" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2OB0AerEbGu" role="3cqZAp">
            <node concept="37vLTI" id="2OB0AerEgR8" role="3clFbG">
              <node concept="2ShNRf" id="2OB0AerEiQh" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJc6_B" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;(int)" resolve="JTextField" />
                  <node concept="3cmrfG" id="5t_d$dJpAiJ" role="37wK5m">
                    <property role="3cmrfH" value="20" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="2OB0AerEbGs" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerDsCu" resolve="comboForType" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="2OB0AerESZe" role="3cqZAp">
            <node concept="37vLTI" id="2OB0AerEXOs" role="3clFbG">
              <node concept="2ShNRf" id="2OB0AerEZMe" role="37vLTx">
                <node concept="1pGfFk" id="2OB0AerEYSQ" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                  <node concept="Xl_RD" id="2OB0AerF1K9" role="37wK5m">
                    <property role="Xl_RC" value="Project/Assignment Name:" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="2OB0AerESZc" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerDwSI" resolve="nameLabel" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJb_o6" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJbGky" role="3clFbG">
              <node concept="2ShNRf" id="5t_d$dJbIhf" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJpWs2" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;(int)" resolve="JTextField" />
                  <node concept="3cmrfG" id="5t_d$dJpY9e" role="37wK5m">
                    <property role="3cmrfH" value="20" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5t_d$dJb_o4" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerD_7N" resolve="nameField" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJcb4L" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJcfhi" role="3clFbG">
              <node concept="2ShNRf" id="5t_d$dJchY0" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJch4C" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JButton.&lt;init&gt;(java.lang.String)" resolve="JButton" />
                  <node concept="Xl_RD" id="5t_d$dJciWl" role="37wK5m">
                    <property role="Xl_RC" value="Schedule" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5t_d$dJcb4J" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerEa4l" resolve="scheduleButton" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJclWE" role="3cqZAp">
            <node concept="2OqwBi" id="5t_d$dJcoYm" role="3clFbG">
              <node concept="37vLTw" id="5t_d$dJclWC" role="2Oq$k0">
                <ref role="3cqZAo" node="2OB0AerEa4l" resolve="scheduleButton" />
              </node>
              <node concept="liA8E" id="5t_d$dJcAMQ" role="2OqNvi">
                <ref role="37wK5l" to="dxuu:~AbstractButton.addActionListener(java.awt.event.ActionListener):void" resolve="addActionListener" />
                <node concept="2ShNRf" id="5t_d$dJcFrS" role="37wK5m">
                  <node concept="YeOm9" id="5t_d$dJcIRA" role="2ShVmc">
                    <node concept="1Y3b0j" id="5t_d$dJcIRD" role="YeSDq">
                      <property role="2bfB8j" value="true" />
                      <ref role="1Y3XeK" to="hyam:~ActionListener" resolve="ActionListener" />
                      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
                      <node concept="3Tm1VV" id="5t_d$dJcIRE" role="1B3o_S" />
                      <node concept="3clFb_" id="5t_d$dJcIRG" role="jymVt">
                        <property role="TrG5h" value="actionPerformed" />
                        <node concept="3Tm1VV" id="5t_d$dJcIRH" role="1B3o_S" />
                        <node concept="3cqZAl" id="5t_d$dJcIRJ" role="3clF45" />
                        <node concept="37vLTG" id="5t_d$dJcIRK" role="3clF46">
                          <property role="TrG5h" value="p0" />
                          <node concept="3uibUv" id="5t_d$dJcIRL" role="1tU5fm">
                            <ref role="3uigEE" to="hyam:~ActionEvent" resolve="ActionEvent" />
                          </node>
                        </node>
                        <node concept="3clFbS" id="5t_d$dJcIRM" role="3clF47">
                          <node concept="3cpWs8" id="5t_d$dJcVLm" role="3cqZAp">
                            <node concept="3cpWsn" id="5t_d$dJcVLn" role="3cpWs9">
                              <property role="TrG5h" value="categoryV" />
                              <node concept="3uibUv" id="5t_d$dJcVLo" role="1tU5fm">
                                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                              </node>
                              <node concept="2OqwBi" id="5t_d$dJdpRM" role="33vP2m">
                                <node concept="2OqwBi" id="5t_d$dJd4Tb" role="2Oq$k0">
                                  <node concept="37vLTw" id="5t_d$dJd2T4" role="2Oq$k0">
                                    <ref role="3cqZAo" node="2OB0AerDsCu" resolve="comboForType" />
                                  </node>
                                  <node concept="liA8E" id="5t_d$dJdoGa" role="2OqNvi">
                                    <ref role="37wK5l" to="r791:~JTextComponent.getText():java.lang.String" resolve="getText" />
                                  </node>
                                </node>
                                <node concept="liA8E" id="5t_d$dJdCD3" role="2OqNvi">
                                  <ref role="37wK5l" to="wyt6:~String.toString():java.lang.String" resolve="toString" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs8" id="5t_d$dJdZgR" role="3cqZAp">
                            <node concept="3cpWsn" id="5t_d$dJdZgS" role="3cpWs9">
                              <property role="TrG5h" value="difficultyV" />
                              <node concept="3uibUv" id="5t_d$dJdZgT" role="1tU5fm">
                                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                              </node>
                              <node concept="2OqwBi" id="5t_d$dJe8qB" role="33vP2m">
                                <node concept="37vLTw" id="5t_d$dJe6CC" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2OB0AerD_7N" resolve="nameField" />
                                </node>
                                <node concept="liA8E" id="5t_d$dJelKA" role="2OqNvi">
                                  <ref role="37wK5l" to="r791:~JTextComponent.getText():java.lang.String" resolve="getText" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs8" id="5t_d$dJeo3T" role="3cqZAp">
                            <node concept="3cpWsn" id="5t_d$dJeo3U" role="3cpWs9">
                              <property role="TrG5h" value="hoursV" />
                              <node concept="3uibUv" id="5t_d$dJeo3V" role="1tU5fm">
                                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                              </node>
                              <node concept="2OqwBi" id="5t_d$dJex_P" role="33vP2m">
                                <node concept="37vLTw" id="5t_d$dJrkYJ" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2OB0AerDPYa" resolve="hoursField" />
                                </node>
                                <node concept="liA8E" id="5t_d$dJeIVV" role="2OqNvi">
                                  <ref role="37wK5l" to="r791:~JTextComponent.getText():java.lang.String" resolve="getText" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs8" id="5t_d$dJeM_g" role="3cqZAp">
                            <node concept="3cpWsn" id="5t_d$dJeM_h" role="3cpWs9">
                              <property role="TrG5h" value="dueDateV" />
                              <node concept="3uibUv" id="5t_d$dJeM_i" role="1tU5fm">
                                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                              </node>
                              <node concept="2OqwBi" id="5t_d$dJeW8G" role="33vP2m">
                                <node concept="37vLTw" id="5t_d$dJeUmU" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2OB0AerDX_I" resolve="dueField" />
                                </node>
                                <node concept="liA8E" id="5t_d$dJf9vk" role="2OqNvi">
                                  <ref role="37wK5l" to="r791:~JTextComponent.getText():java.lang.String" resolve="getText" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3cpWs8" id="5t_d$dJfdsT" role="3cqZAp">
                            <node concept="3cpWsn" id="5t_d$dJfdsU" role="3cpWs9">
                              <property role="TrG5h" value="courseV" />
                              <node concept="3uibUv" id="5t_d$dJfdsV" role="1tU5fm">
                                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                              </node>
                              <node concept="2OqwBi" id="5t_d$dJflKU" role="33vP2m">
                                <node concept="37vLTw" id="5t_d$dJfjEP" role="2Oq$k0">
                                  <ref role="3cqZAo" node="2OB0AerE5uu" resolve="courseField" />
                                </node>
                                <node concept="liA8E" id="5t_d$dJf$2q" role="2OqNvi">
                                  <ref role="37wK5l" to="r791:~JTextComponent.getText():java.lang.String" resolve="getText" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbH" id="5t_d$dJf$ZS" role="3cqZAp" />
                          <node concept="3cpWs8" id="5t_d$dJfC$_" role="3cqZAp">
                            <node concept="3cpWsn" id="5t_d$dJfC$A" role="3cpWs9">
                              <property role="TrG5h" value="details" />
                              <node concept="3uibUv" id="5t_d$dJfC$B" role="1tU5fm">
                                <ref role="3uigEE" to="wyt6:~String" resolve="String" />
                              </node>
                              <node concept="3cpWs3" id="5t_d$dJgn23" role="33vP2m">
                                <node concept="37vLTw" id="5t_d$dJgoMj" role="3uHU7w">
                                  <ref role="3cqZAo" node="5t_d$dJfdsU" resolve="courseV" />
                                </node>
                                <node concept="3cpWs3" id="5t_d$dJgkmh" role="3uHU7B">
                                  <node concept="3cpWs3" id="5t_d$dJgdBu" role="3uHU7B">
                                    <node concept="3cpWs3" id="5t_d$dJgblA" role="3uHU7B">
                                      <node concept="3cpWs3" id="5t_d$dJg5ND" role="3uHU7B">
                                        <node concept="3cpWs3" id="5t_d$dJg3AE" role="3uHU7B">
                                          <node concept="3cpWs3" id="5t_d$dJfXb3" role="3uHU7B">
                                            <node concept="3cpWs3" id="5t_d$dJfVdc" role="3uHU7B">
                                              <node concept="Xl_RD" id="5t_d$dJrI9I" role="3uHU7B">
                                                <property role="Xl_RC" value="Project/Assignment" />
                                              </node>
                                              <node concept="Xl_RD" id="5t_d$dJfWgT" role="3uHU7w">
                                                <property role="Xl_RC" value="," />
                                              </node>
                                            </node>
                                            <node concept="37vLTw" id="5t_d$dJfZxV" role="3uHU7w">
                                              <ref role="3cqZAo" node="5t_d$dJdZgS" resolve="difficultyV" />
                                            </node>
                                          </node>
                                          <node concept="Xl_RD" id="5t_d$dJg4JF" role="3uHU7w">
                                            <property role="Xl_RC" value="," />
                                          </node>
                                        </node>
                                        <node concept="37vLTw" id="5t_d$dJg7gF" role="3uHU7w">
                                          <ref role="3cqZAo" node="5t_d$dJeo3U" resolve="hoursV" />
                                        </node>
                                      </node>
                                      <node concept="Xl_RD" id="5t_d$dJgcW1" role="3uHU7w">
                                        <property role="Xl_RC" value="," />
                                      </node>
                                    </node>
                                    <node concept="37vLTw" id="5t_d$dJggha" role="3uHU7w">
                                      <ref role="3cqZAo" node="5t_d$dJeM_h" resolve="dueDateV" />
                                    </node>
                                  </node>
                                  <node concept="Xl_RD" id="5t_d$dJglqf" role="3uHU7w">
                                    <property role="Xl_RC" value="," />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="SfApY" id="5t_d$dJgscu" role="3cqZAp">
                            <node concept="3clFbS" id="5t_d$dJgscw" role="SfCbr">
                              <node concept="3clFbF" id="5t_d$dJgwwp" role="3cqZAp">
                                <node concept="2YIFZM" id="5t_d$dJgA2L" role="3clFbG">
                                  <ref role="37wK5l" node="2OB0Aer8wzk" resolve="scheduleApp" />
                                  <ref role="1Pybhc" node="2lFfnZ$$V1C" resolve="Schedule" />
                                  <node concept="37vLTw" id="5t_d$dJgGem" role="37wK5m">
                                    <ref role="3cqZAo" node="5t_d$dJfC$A" resolve="details" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="TDmWw" id="5t_d$dJgscx" role="TEbGg">
                              <node concept="3cpWsn" id="5t_d$dJgscz" role="TDEfY">
                                <property role="TrG5h" value="ex" />
                                <node concept="3uibUv" id="5t_d$dJgtW2" role="1tU5fm">
                                  <ref role="3uigEE" to="25x5:~ParseException" resolve="ParseException" />
                                </node>
                              </node>
                              <node concept="3clFbS" id="5t_d$dJgscB" role="TDEfX" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1X3_iC" id="5t_d$dJtF8B" role="lGtFl">
            <property role="3V$3am" value="statement" />
            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
            <node concept="3clFbF" id="5t_d$dJh3iR" role="8Wnug">
              <node concept="37vLTI" id="5t_d$dJha5s" role="3clFbG">
                <node concept="2ShNRf" id="5t_d$dJhh37" role="37vLTx">
                  <node concept="1pGfFk" id="5t_d$dJhg9J" role="2ShVmc">
                    <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                    <node concept="Xl_RD" id="5t_d$dJhi3c" role="37wK5m">
                      <property role="Xl_RC" value="Category:" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="5t_d$dJh3iP" role="37vLTJ">
                  <ref role="3cqZAo" node="2OB0AerDjil" resolve="categoryLabel" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1X3_iC" id="5t_d$dJtHXk" role="lGtFl">
            <property role="3V$3am" value="statement" />
            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
            <node concept="3clFbF" id="5t_d$dJhlxC" role="8Wnug">
              <node concept="37vLTI" id="5t_d$dJhxUw" role="3clFbG">
                <node concept="2ShNRf" id="5t_d$dJh_44" role="37vLTx">
                  <node concept="1pGfFk" id="5t_d$dJh$aG" role="2ShVmc">
                    <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;(int)" resolve="JTextField" />
                    <node concept="3cmrfG" id="5t_d$dJhABh" role="37wK5m">
                      <property role="3cmrfH" value="20" />
                    </node>
                  </node>
                </node>
                <node concept="37vLTw" id="5t_d$dJhlxA" role="37vLTJ">
                  <ref role="3cqZAo" node="2OB0AerDo1d" resolve="categoryField" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJhG8I" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJhMxv" role="3clFbG">
              <node concept="2ShNRf" id="5t_d$dJhORe" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJhNXQ" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                  <node concept="Xl_RD" id="5t_d$dJhQJK" role="37wK5m">
                    <property role="Xl_RC" value="Estimated Hours:" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5t_d$dJhG8G" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerDKJ_" resolve="hoursLabel" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJhVmD" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJi0G_" role="3clFbG">
              <node concept="2ShNRf" id="5t_d$dJi2Xd" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJi23P" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;(int)" resolve="JTextField" />
                  <node concept="3cmrfG" id="5t_d$dJi6FT" role="37wK5m">
                    <property role="3cmrfH" value="20" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5t_d$dJhVmB" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerDPYa" resolve="hoursField" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJicfJ" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJiibc" role="3clFbG">
              <node concept="2ShNRf" id="5t_d$dJilpR" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJikwv" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                  <node concept="Xl_RD" id="5t_d$dJimpT" role="37wK5m">
                    <property role="Xl_RC" value="Due Date" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5t_d$dJicfH" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerDT$1" resolve="dueLabel" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJipO0" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJiwtj" role="3clFbG">
              <node concept="2ShNRf" id="5t_d$dJizAN" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJiyHr" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;(int)" resolve="JTextField" />
                  <node concept="3cmrfG" id="5t_d$dJi_ap" role="37wK5m">
                    <property role="3cmrfH" value="20" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5t_d$dJipNY" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerDX_I" resolve="dueField" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJiEIO" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJiKvL" role="3clFbG">
              <node concept="2ShNRf" id="5t_d$dJiNj8" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJiMpK" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JLabel.&lt;init&gt;(java.lang.String)" resolve="JLabel" />
                  <node concept="Xl_RD" id="5t_d$dJiOIX" role="37wK5m">
                    <property role="Xl_RC" value="Course Name:" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5t_d$dJiEIM" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerE1fl" resolve="courseLabel" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJiSGF" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJiY22" role="3clFbG">
              <node concept="2ShNRf" id="5t_d$dJj1dh" role="37vLTx">
                <node concept="1pGfFk" id="5t_d$dJj0jT" role="2ShVmc">
                  <ref role="37wK5l" to="dxuu:~JTextField.&lt;init&gt;(int)" resolve="JTextField" />
                  <node concept="3cmrfG" id="5t_d$dJj2KZ" role="37wK5m">
                    <property role="3cmrfH" value="20" />
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="5t_d$dJiSGD" role="37vLTJ">
                <ref role="3cqZAo" node="2OB0AerE5uu" resolve="courseField" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJjeBM" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJjeBK" role="3clFbG">
              <ref role="37wK5l" node="5t_d$dJjbxJ" resolve="setComponents" />
            </node>
          </node>
        </node>
        <node concept="3Tm1VV" id="2OB0AerD2BV" role="1B3o_S" />
      </node>
      <node concept="2tJIrI" id="5t_d$dJj6IT" role="jymVt" />
      <node concept="3clFb_" id="5t_d$dJjbxJ" role="jymVt">
        <property role="TrG5h" value="setComponents" />
        <node concept="3clFbS" id="5t_d$dJjbxM" role="3clF47">
          <node concept="3clFbF" id="5t_d$dJkyhv" role="3cqZAp">
            <node concept="2OqwBi" id="5t_d$dJkzEX" role="3clFbG">
              <node concept="Xjq3P" id="5t_d$dJkyht" role="2Oq$k0" />
              <node concept="liA8E" id="5t_d$dJk$Vv" role="2OqNvi">
                <ref role="37wK5l" to="z60i:~Container.setLayout(java.awt.LayoutManager):void" resolve="setLayout" />
                <node concept="2ShNRf" id="5t_d$dJkD9M" role="37wK5m">
                  <node concept="HV5vD" id="5t_d$dJkH7z" role="2ShVmc">
                    <ref role="HV5vE" to="z60i:~GridBagLayout" resolve="GridBagLayout" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="5t_d$dJjzTP" role="3cqZAp">
            <node concept="3cpWsn" id="5t_d$dJjzTQ" role="3cpWs9">
              <property role="TrG5h" value="gbc" />
              <node concept="3uibUv" id="5t_d$dJjzTR" role="1tU5fm">
                <ref role="3uigEE" to="z60i:~GridBagConstraints" resolve="GridBagConstraints" />
              </node>
              <node concept="2ShNRf" id="5t_d$dJj$TQ" role="33vP2m">
                <node concept="1pGfFk" id="5t_d$dJjBPd" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~GridBagConstraints.&lt;init&gt;()" resolve="GridBagConstraints" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="5t_d$dJjDH0" role="3cqZAp">
            <node concept="3cpWsn" id="5t_d$dJjDH1" role="3cpWs9">
              <property role="TrG5h" value="basic" />
              <node concept="3uibUv" id="5t_d$dJjDH2" role="1tU5fm">
                <ref role="3uigEE" to="z60i:~Insets" resolve="Insets" />
              </node>
              <node concept="2ShNRf" id="5t_d$dJjH51" role="33vP2m">
                <node concept="1pGfFk" id="5t_d$dJjK0o" role="2ShVmc">
                  <ref role="37wK5l" to="z60i:~Insets.&lt;init&gt;(int,int,int,int)" resolve="Insets" />
                  <node concept="3cmrfG" id="5t_d$dJjKUz" role="37wK5m">
                    <property role="3cmrfH" value="5" />
                  </node>
                  <node concept="3cmrfG" id="5t_d$dJjLO3" role="37wK5m">
                    <property role="3cmrfH" value="2" />
                  </node>
                  <node concept="3cmrfG" id="5t_d$dJjMJt" role="37wK5m">
                    <property role="3cmrfH" value="5" />
                  </node>
                  <node concept="3cmrfG" id="5t_d$dJjND1" role="37wK5m">
                    <property role="3cmrfH" value="5" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJjOCc" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJjRO3" role="3clFbG">
              <node concept="37vLTw" id="5t_d$dJjTGl" role="37vLTx">
                <ref role="3cqZAo" node="5t_d$dJjDH1" resolve="basic" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJjPKk" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJjOCa" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJjQxK" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.insets" resolve="insets" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJjUFE" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJjZHL" role="3clFbG">
              <node concept="10M0yZ" id="5t_d$dJk2IO" role="37vLTx">
                <ref role="3cqZAo" to="z60i:~GridBagConstraints.CENTER" resolve="CENTER" />
                <ref role="1PxDUh" to="z60i:~GridBagConstraints" resolve="GridBagConstraints" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJjVNS" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJjUFC" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJk5FH" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.anchor" resolve="anchor" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJk3Le" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJk9ei" role="3clFbG">
              <node concept="10M0yZ" id="5t_d$dJkcfK" role="37vLTx">
                <ref role="3cqZAo" to="z60i:~GridBagConstraints.BOTH" resolve="BOTH" />
                <ref role="1PxDUh" to="z60i:~GridBagConstraints" resolve="GridBagConstraints" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJk4Ty" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJk3Lc" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJk5Kk" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.fill" resolve="fill" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJkdig" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJkiWC" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJkHgG" role="37vLTx">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJkenM" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJkdie" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJkeYe" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridx" resolve="gridx" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJkkWp" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJkqcZ" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJkHlb" role="37vLTx">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJkm21" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJkkWn" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJkmCt" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJkIyS" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJkOFI" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJkPJh" role="37vLTx">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJkJI4" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJkIyQ" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJkKyx" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridx" resolve="gridx" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1X3_iC" id="5t_d$dJtRZX" role="lGtFl">
            <property role="3V$3am" value="statement" />
            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
            <node concept="3clFbF" id="5t_d$dJkQXv" role="8Wnug">
              <node concept="37vLTI" id="5t_d$dJkWcM" role="3clFbG">
                <node concept="2OqwBi" id="5t_d$dJkS5U" role="37vLTJ">
                  <node concept="37vLTw" id="5t_d$dJkQXt" role="2Oq$k0">
                    <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                  </node>
                  <node concept="2OwXpG" id="5t_d$dJkSIW" role="2OqNvi">
                    <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                  </node>
                </node>
                <node concept="3cmrfG" id="5t_d$dJkYq0" role="37vLTx">
                  <property role="3cmrfH" value="2" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1X3_iC" id="5t_d$dJtU_4" role="lGtFl">
            <property role="3V$3am" value="statement" />
            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
            <node concept="3clFbF" id="5t_d$dJkZnS" role="8Wnug">
              <node concept="1rXfSq" id="5t_d$dJkZnQ" role="3clFbG">
                <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
                <node concept="37vLTw" id="5t_d$dJl4wQ" role="37wK5m">
                  <ref role="3cqZAo" node="2OB0AerDsCu" resolve="comboForType" />
                </node>
                <node concept="37vLTw" id="5t_d$dJl65X" role="37wK5m">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJlaPQ" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJlhmG" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJli_y" role="37vLTx">
                <property role="3cmrfH" value="3" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJlccO" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJlaPO" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJldnZ" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJljZO" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJljZM" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJlnOE" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerD_7N" resolve="nameField" />
              </node>
              <node concept="37vLTw" id="5t_d$dJlq9b" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJltZ6" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJlAc6" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJlBA6" role="37vLTx">
                <property role="3cmrfH" value="4" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJlvWz" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJltZ4" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJlySd" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJlDda" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJlDd8" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJlHvU" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerDPYa" resolve="hoursField" />
              </node>
              <node concept="37vLTw" id="5t_d$dJlJGT" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJlPci" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJlVXI" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJlXu5" role="37vLTx">
                <property role="3cmrfH" value="5" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJlQIW" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJlPcg" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJlSEg" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJlZ17" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJlZ15" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJm4Bn" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerDX_I" resolve="dueField" />
              </node>
              <node concept="37vLTw" id="5t_d$dJm65B" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJmaLi" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJmaLj" role="3clFbG">
              <node concept="2OqwBi" id="5t_d$dJmaLl" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJmaLm" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJmaLn" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
              <node concept="3cmrfG" id="5t_d$dJmeQi" role="37vLTx">
                <property role="3cmrfH" value="6" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJmaLo" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJmaLp" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJmhXr" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerE5uu" resolve="courseField" />
              </node>
              <node concept="37vLTw" id="5t_d$dJmaLr" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJmimN" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJmimO" role="3clFbG">
              <node concept="2OqwBi" id="5t_d$dJmimQ" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJmimR" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJmimS" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
              <node concept="3cmrfG" id="5t_d$dJmlRb" role="37vLTx">
                <property role="3cmrfH" value="7" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJmimT" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJmimU" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJmyrL" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerEa4l" resolve="scheduleButton" />
              </node>
              <node concept="37vLTw" id="5t_d$dJm$Cx" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJmFTs" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJmFTt" role="3clFbG">
              <node concept="2OqwBi" id="5t_d$dJmFTv" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJmFTw" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJmFTx" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
              <node concept="3cmrfG" id="5t_d$dJn_E1" role="37vLTx">
                <property role="3cmrfH" value="6" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJmLRi" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJmSoE" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJmU8c" role="37vLTx">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJmNAD" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJmLRg" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJmOUO" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridx" resolve="gridx" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJmFTy" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJmFTz" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJmZlB" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerE1fl" resolve="courseLabel" />
              </node>
              <node concept="37vLTw" id="5t_d$dJn0Pi" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJn6ZB" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJnDCl" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJnFoS" role="37vLTx">
                <property role="3cmrfH" value="5" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJn92d" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJn6Z_" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJnAkP" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJnIw3" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJnIw1" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJnOvF" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerDT$1" resolve="dueLabel" />
              </node>
              <node concept="37vLTw" id="5t_d$dJnPZW" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJnWc2" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJo34X" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJo4Zd" role="37vLTx">
                <property role="3cmrfH" value="4" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJnYeW" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJnWc0" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJnZIf" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJo6Wo" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJo6Wm" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJoaHK" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerDKJ_" resolve="hoursLabel" />
              </node>
              <node concept="37vLTw" id="5t_d$dJodFO" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJokJ7" role="3cqZAp">
            <node concept="37vLTI" id="5t_d$dJos7Y" role="3clFbG">
              <node concept="3cmrfG" id="5t_d$dJou3t" role="37vLTx">
                <property role="3cmrfH" value="3" />
              </node>
              <node concept="2OqwBi" id="5t_d$dJomD$" role="37vLTJ">
                <node concept="37vLTw" id="5t_d$dJokJ5" role="2Oq$k0">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
                <node concept="2OwXpG" id="5t_d$dJoo9G" role="2OqNvi">
                  <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5t_d$dJow9W" role="3cqZAp">
            <node concept="1rXfSq" id="5t_d$dJow9U" role="3clFbG">
              <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
              <node concept="37vLTw" id="5t_d$dJoz32" role="37wK5m">
                <ref role="3cqZAo" node="2OB0AerDwSI" resolve="nameLabel" />
              </node>
              <node concept="37vLTw" id="5t_d$dJo_6m" role="37wK5m">
                <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
              </node>
            </node>
          </node>
          <node concept="1X3_iC" id="5t_d$dJtKuA" role="lGtFl">
            <property role="3V$3am" value="statement" />
            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
            <node concept="3clFbF" id="5t_d$dJoEC7" role="8Wnug">
              <node concept="37vLTI" id="5t_d$dJoOve" role="3clFbG">
                <node concept="3cmrfG" id="5t_d$dJoQqF" role="37vLTx">
                  <property role="3cmrfH" value="2" />
                </node>
                <node concept="2OqwBi" id="5t_d$dJoGz4" role="37vLTJ">
                  <node concept="37vLTw" id="5t_d$dJoEC5" role="2Oq$k0">
                    <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                  </node>
                  <node concept="2OwXpG" id="5t_d$dJoKcW" role="2OqNvi">
                    <ref role="2Oxat5" to="z60i:~GridBagConstraints.gridy" resolve="gridy" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="1X3_iC" id="5t_d$dJtN4v" role="lGtFl">
            <property role="3V$3am" value="statement" />
            <property role="3V$3ak" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068580123136/1068581517665" />
            <node concept="3clFbF" id="5t_d$dJoSpZ" role="8Wnug">
              <node concept="1rXfSq" id="5t_d$dJoSpX" role="3clFbG">
                <ref role="37wK5l" to="z60i:~Container.add(java.awt.Component,java.lang.Object):void" resolve="add" />
                <node concept="37vLTw" id="5t_d$dJoVk0" role="37wK5m">
                  <ref role="3cqZAo" node="2OB0AerDjil" resolve="categoryLabel" />
                </node>
                <node concept="37vLTw" id="5t_d$dJoXSs" role="37wK5m">
                  <ref role="3cqZAo" node="5t_d$dJjzTQ" resolve="gbc" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3Tm6S6" id="5t_d$dJj8Qw" role="1B3o_S" />
        <node concept="3cqZAl" id="5t_d$dJja5o" role="3clF45" />
      </node>
      <node concept="3Tm1VV" id="2OB0AerCyyj" role="1B3o_S" />
      <node concept="3uibUv" id="2OB0AerCX7W" role="1zkMxy">
        <ref role="3uigEE" to="dxuu:~JPanel" resolve="JPanel" />
      </node>
    </node>
    <node concept="3Tm1VV" id="2lFfnZ$$V1D" role="1B3o_S" />
    <node concept="n94m4" id="2lFfnZ$$V1E" role="lGtFl">
      <ref role="n9lRv" to="ky5v:2t50$NbHYzI" resolve="Schedule" />
    </node>
    <node concept="3uibUv" id="7bbHeTWfniy" role="1zkMxy">
      <ref role="3uigEE" to="dxuu:~JFrame" resolve="JFrame" />
    </node>
  </node>
</model>

